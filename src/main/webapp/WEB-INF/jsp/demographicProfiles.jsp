<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<body>
	<h2>Here is a Big Data Demographic Profile Management.</h2>

		<form action="demographicProfiles" method="post">
			<input type="hidden" name="id">
			<label for="age">Age</label>
			<input type="text" id="age" name="age"/>
			<input type="submit" value="Submit"/>
		</form>

	<table border="1">
		<c:forEach var="profile" items="${demographicProfileList}">
			<tr>
				<td>${profile.age}</td><td><input type="button" value="delete" onclick="window.location='adprefprofile/?${profile.id}'"/></td>
			</tr>
		</c:forEach>
	</table>	
</body>
</html>