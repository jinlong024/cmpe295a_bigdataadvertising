<%--
  Created by IntelliJ IDEA.
  User: macbookpro
  Date: 12/18/13
  Time: 3:13 AM
  To change this template use File | Settings | File Templates.
--%>
<%--
  Created by IntelliJ IDEA.
  User: macbookpro
  Date: 12/9/13
  Time: 12:00 PM
  To change this template use File | Settings | File Templates.
--%>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Collection Input</title>

    <%--<link rel="stylesheet" type="text/css" href="css/bootstrap-select.min.css">--%>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    <style>
        .mid {
            width: auto;
            display: table;
            margin-left: auto;
            margin-right: auto;
            margin-top: 280px;
            margin-bottom: auto;
        }
    </style>
</head>

<body>
<div class="navbar  navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container">
            <button data-target=".nav-collapse" data-toggle="collapse" class="btn btn-navbar" type="button">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="#" class="brand" style="font-family: Flat-UI-Icons; font-size: 30px;color: #0e90d2">AD Big Data</a>

            <div class="nav-collapse collapse">
                <ul class="nav">
                    <li class="active">
                        <a href="adPlanning">Home</a>
                    </li>
                    <li>
                        <a href="adPlanning">AD Planning</a>
                    </li>
                    <li>
                        <a href="/BigDataAdvertising/advHomepage">Recommend</a>
                    </li>
                    <li>
                        <a href="adEvaluation">Evaluate</a>
                    </li>
                    <li>
                        <a href="/BigDataAdvertising/TwitterDataCollection">Data Collection</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="mid">
        <div class="row">
            <form action='collect' method='POST' class="form-horizontal">

                <div class="control-group">
                    <label style="font-size: 20px" class="control-label" for="category">Product Category</label>

                    <div class="controls">
                        <select id="category" name="category">
                            <option value="">Please Choose...</option>
                            <option>Beauty</option>
                            <option>Clothing</option>
                            <option>Electronics</option>
                            <option>Food</option>
                            <option>Home</option>
                        </select>
                    </div>
                </div>


                <div class="control-group">
                    <label style="font-size: 20px" class="control-label" for="name">Product Name</label>

                    <div class="controls">
                        <input type="text" id="name" name="name" placeholder="Product Name">
                    </div>
                </div>


                <div class="control-group">
                    <label style="font-size: 20px" class="control-label" for="model">Product Model</label>

                    <div class="controls">
                        <input type="text" id="model" name="model" placeholder="Product Model">
                    </div>
                </div>

                <div class="control-group">
                    <label style="font-size: 20px" class="control-label" for="manufacturer">Product Manufacturer</label>

                    <div class="controls">
                        <input type="text" id="manufacturer" name="manufacturer" placeholder="Product Manufacturer">
                    </div>
                </div>

                <div>&nbsp;</div>
                <div>&nbsp;</div>


                <%--            <a  class="btn btn-small" href="/BigDataAdvertising/map?id=${adPrefProfile.id}">Recommend </a>--%>
                <center>
                    <button class="btn btn-success btn-large btn-block" type="submit">Submit</button>
                </center>
                <div>&nbsp;</div>
                <a class="btn btn-large btn-block" type="button" href="/BigDataAdvertising/advHomepage">Back</a>

            </form>
        </div>
    </div>
</div>
</body>

<script src="scripts/bootstrap-select.js"></script>
<script src="scripts/bootstrap.min.js"></script>
<script src="scripts/jquery-1.9.1.js"></script>
<script src="scripts/jquery.backstretch.js"></script>
<script>$.backstretch("images/light.jpeg")</script>


</html>