<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
    <title>playground</title>
    <style type="text/css">
        body {
            padding-top: 60px;
            padding-bottom: 40px;
        }

        .slide-text {
            text-align: justify;
            padding: 0;
            margin: 0 -10px;
        }

        .slide-text > span {
            display: inline-block;
            margin: auto;
            padding: 0;
            overflow: hidden;
            white-space: nowrap;
        }

        .slide-text > .fin {
            display: inline-block;
            width: 100%;
        }

        .ui-slide {
            width: 100px;
            height: 12px;
        }
    </style>
    <link rel="stylesheet"
          href="https://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/css/bootstrap-combined.min.css">
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css"/>
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>

</head>

<body>
<div class="navbar navbar-inverse navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container">
            <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span></span>
                <span></span>
                <span></span>
            </button>
            <a class="brand" href="#">Project name</a>

            <div class="nav-collapse collapse">
                <ul class="nav">
                    <li class="active">
                        <a href="#">User Home</a>
                    </li>
                    <li>
                        <a href="">Ad Preference</a>
                    </li>
                    <li>
                        <a href="">Record</a>
                    </li>
                    <li>
                        <a href="">Data Collection</a>
                    </li>
                </ul>
                <!-- <form class="navbar-form pull-right">
                     <input class="span2" type="text" placeholder="Email">
                     <input class="span2" type="password" placeholder="Password">
                     <button type="submit" class="btn">Sign in</button>
                 </form>-->
                <form class="navbar-form pull-right">

                    <input class="span2" type="text" placeholder="Email"/>


                    <input class="span2" type="password" placeholder="Password"/>

                    <button type="submit" class="btn">Sign in</button>
                </form>
            </div>
            <!--/.nav-collapse -->
        </div>
    </div>
</div>
<div class="container">

<form class="form-horizontal" action="adPrefProfiles" method="post">
<fieldset>
    <div id="legend" class="">
        <h2>Please Input your Product Information </h2>

        <h3>--- General Information</h3>
    </div>
    <div class="control-group">
        <div>
            <!-- Text input-->
            <label class="control-label" for="adId" style="font-size: 23px">Product Name:</label>
        </div>
        <div class="controls">
            <input type="text" id="adId" name="adId"
                   required="required" placeholder="" class="input-xlarge"
                   style="width: 270px; height: 28px; color: #555555;  font-size: 18px;">

            <p class="help-block"></p>
        </div>


        <label class="control-label" for="adId" style="font-size: 23px">Model Name:</label>

        <div class="controls">
            <input type="text" id="mldid" name="mldid"
                   required="required" placeholder="" class="input-xlarge"
                   style="width: 270px; height: 28px; color: #555555;  font-size: 18px;">

            <p class="help-block"></p>
        </div>

        <label class="control-label" for="adId" style="font-size: 23px">Manufacturer:</label>

        <div class="controls">
            <input type="text" id="productManufacturer" name="brdId"
                   required="required" placeholder="" class="input-xlarge"
                   style="width: 270px; height: 28px; color: #555555;  font-size: 18px;">


        </div>
</div>

        <div class="row">
<div class="span2">
    <label class="control-label" for="adId" style="font-size: 23px">Category :</label>
</div>
            <div class="span2">
                <select id="cate" name="cate">
                    <option value="">Please Choose...</option>
                    <option >electronic</option>
                    <option>home</option>
                    <option>food</option>
                    <option >apperence</option>
                    <option>other</option>
                </select>
            </div>
        </div>

      <%--<div class="control-group">

        <label class="control-label" for="category">Category:</label>
          <div class="control">
        <select class="input-xlarge" id="category" name="category" required="required">
            <option value="" selected="selected">Please Choose...</option>
            <option value="makeup">Makeup</option>
            <option>Car</option>
           <option>Clothes</option>
            </select>
        </div>

    </div>--%>

    <%--<div class="control-group">--%>

    <%--&lt;!&ndash; Select Basic &ndash;&gt;--%>
    <%--<label class="control-label">Location:</label>--%>

    <%--<div class="controls">--%>
        <%--<label>--%>
            <%--<select class="input-xlarge">--%>
                <%--<option value="" selected="selected">Please Choose...</option>--%>
                <%--<option>San Jose</option>--%>
                <%--<option>Fremont</option>--%>
                <%--<option>Mountview</option>--%>
                <%--<option>Oakland</option>--%>
                <%--</select>--%>
            <%--</label>--%>
        <%--</div>--%>

    <%--</div>--%>






<h3>--- AD Preference Profile</h3>

<p>
<h4>< please select the preference and configure its parameter > </h4>

<table class="table table-condensed">

<tbody>
<!--Age number 1-->

<tr>
    <div>
        <td width="100px" style="font-size:20px">Age</td>
    </div>

    <div>
        <td width="5%">
            <input type="checkbox" id="chk1" size="5">
        </td>
    </div>

    <div>
        <td width="5%" style="font-size:20px">type:</td>
    </div>

    <div>
        <td width="10%">
            <select disabled id="age" name="age" class="input-xlarge">
                <option value="">Please Choose...</option>
                <option>Senior (65 or over)</option>
                <option>Adult (18-64)</option>
                <option>Youth (12-17)</option>
                <option>Child (2-11)</option>
                <option>Infant (Under 2)</option>
            </select>
        </td>
    </div>
    <td width="20%" style="font-size:20px">&nbsp;&nbsp;&nbsp;weight:
        <input type="text" id="s1" name="ageWeight" readonly="readonly"
               style="width: 80px; height: 20px; color: #f6931f; font-weight: bold; font-size: 20px;">
    </td>
    <td width="40%" class="long age">
        <div class="slider-range-max" id="s1Slider">
        </div>
        <div class="slide-text">
            <span>Minimum: 0.1</span>
            <!--<span>0.2</span>-->
            <!--<span>0.3</span>-->
            <!--<span>0.4</span>-->
            <!--<span>0.5</span>-->
            <!--<span>0.6</span>-->
            <!--<span>0.7</span>-->
            <!--<span>0.8</span>-->
            <!--<span>0.9</span>-->
            <span>Maximum: 1.0</span>
            <span class="fin"></span>
        </div>
    </td>

</tr>


<tr>
    <td width="5%" style="font-size:20px">Education</td>
    <td width="5%">
        <label class="checkbox">
            <input type="checkbox" id="chk2" size="5">
        </label>
    </td>
    <div>
        <td width="5%" style="font-size:20px">type:</td>
        <td width="10%">
            <select disabled id="education" name="education" class="input-xlarge"/>
            <option value="" selected="selected">Please Choose...</option>
            <option>Bachelor's degree or higher</option>
            <option>Some college</option>
            <option>High school graduate</option>
            <option>Less than high school</option>
            </select>
        </td>
        <td width="20%" style="font-size:20px">&nbsp;&nbsp;&nbsp;weight:
            <input type="text" id="s2" name="educationWeight" readonly="readonly"
                   style="width: 80px; height: 20px; color: #f6931f; font-weight: bold; font-size: 20px;"
                   disabled="disabled"/>
        </td>
        <td width="40%">
            <div class="slider-range-max" id="s2Slider">
            </div>
            <div class="slide-text">
                <span>0.1</span>
                <span>0.2</span>
                <span>0.3</span>
                <span>0.4</span>
                <span>0.5</span>
                <span>0.6</span>
                <span>0.7</span>
                <span>0.8</span>
                <span>0.9</span>
                <span>1.0</span>
                <span class="fin"></span>
            </div>
        </td>
    </div>
</tr>


<tr>
    <td width="5%" style="font-size:20px">Gender</td>
    <td width="5%">
        <label class="checkbox">
            <input id="chk3" type="checkbox" value="" size="5">
        </label>
    </td>
    <td width="5%" style="font-size:20px">type:</td>
    <td width="10%">
        <select disabled id="sex" name="sex" class="input-xlarge">
            <option value="" selected="selected">Please Choose...</option>
            <option>Male</option>
            <option>Female</option>
        </select>
    </td>
    <td width="20%" style="font-size:20px">&nbsp;&nbsp;&nbsp;weight:
        <input type="text" id="s3" name="sexWeight" readonly="readonly"
               style="width: 80px; height: 20px; color: #f6931f; font-weight: bold; font-size: 20px;">
    </td>
    <td width="45%">
        <div class="slider-range-max" id="s3Slider">
        </div>
        <div class="slide-text">
            <span>0.1</span>
            <span>0.2</span>
            <span>0.3</span>
            <span>0.4</span>
            <span>0.5</span>
            <span>0.6</span>
            <span>0.7</span>
            <span>0.8</span>
            <span>0.9</span>
            <span>1.0</span>
            <span class="fin"></span>
        </div>
    </td>
</tr>
<!--Income number4 -->
<tr>
    <td width="5%" style="font-size:20px">Income</td>
    <td width="5%">
        <label class="checkbox">
            <input id="chk4" type="checkbox" value="" size="5">
        </label>
    </td>
    <td width="5%" style="font-size:20px">type:</td>
    <td width="10%">
        <select disabled id="income" name="income" class="input-xlarge">
            <option value="" selected="selected">Please Choose...</option>
            <option>$200,000 or more</option>
            <option>$100,000 to $199,999</option>
            <option>$50,000 to $99,999</option>
            <option>$10,000 to $49,999</option>
            <option>Less than $10,000</option>
        </select>
    </td>
    <td width="20%" style="font-size:20px">&nbsp;&nbsp;&nbsp;weight:
        <input type="text" id="s4" name="incomeWeight" readonly="readonly"
               style="width: 80px; height: 20px; color: #f6931f; font-weight: bold; font-size: 20px;">
    </td>
    <td width="45%">
        <div class="slider-range-max" id="s4Slider">
        </div>
        <div class="slide-text">
            <span>0.1</span>
            <span>0.2</span>
            <span>0.3</span>
            <span>0.4</span>
            <span>0.5</span>
            <span>0.6</span>
            <span>0.7</span>
            <span>0.8</span>
            <span>0.9</span>
            <span>1.0</span>
            <span class="fin"></span>
        </div>
    </td>
</tr>
<!--weather #5  -->
<tr>
    <td width="5%" style="font-size:20px">Weather</td>
    <td width="5%">
        <label class="checkbox">
            <input id="chk5" type="checkbox" value="" size="5">
        </label>
    </td>
    <td width="5%" style="font-size:20px">type:</td>
    <td width="10%">
        <select disabled id="weather" name="weather" class="input-xlarge">
            <option value="" selected="selected">Please Choose...</option>
            <option>Coastal Weather</option>
            <option>Hot Weather</option>
            <option>Cold Weather</option>
            <option>Mountain Weather</option>
        </select>
    </td>
    <td width="20%" style="font-size:20px">&nbsp;&nbsp;&nbsp;weight:
        <input type="text" id="s5" name="weatherWeight" readonly="readonly"
               style="width: 80px; height: 20px; color: #f6931f; font-weight: bold; font-size: 20px;">
    </td>
    <td width="45%">
        <div class="slider-range-max" id="s5Slider">
        </div>
        <div class="slide-text">
            <span>0.1</span>
            <span>0.2</span>
            <span>0.3</span>
            <span>0.4</span>
            <span>0.5</span>
            <span>0.6</span>
            <span>0.7</span>
            <span>0.8</span>
            <span>0.9</span>
            <span>1.0</span>
            <span class="fin"></span>
        </div>
    </td>
</tr>
<%--<!--Temperature #6  -->
<tr>
    <td width="5%" style="font-size:20px">Temperature</td>
    <td width="5%">
        <label class="checkbox">
            <input id="chk6" type="checkbox" value="" size="5">
        </label>
    </td>
    <td width="5%" style="font-size:20px">type:</td>
    <td width="10%">
        <select disabled id="temperature" name="temperature" class="input-xlarge">
            <option value="" selected="selected">Please Choose...</option>
            <option value="3">45F~67F</option>
            <option value="2">60F~90F</option>
            <option value="1">50F~72F</option>
            <option value="0">30F~50F</option>
        </select>
    </td>
    <td width="20%" style="font-size:20px">&nbsp;&nbsp;&nbsp;weight:
        <input type="text" id="s6" name="temperatureWeight" readonly="readonly"
               style="width: 80px; height: 20px; color: #f6931f; font-weight: bold; font-size: 20px;">
    </td>
    <td width="45%">
        <div class="slider-range-max" id="s6Slider">
        </div>
        <div class="slide-text">
            <span>0.1</span>
            <span>0.2</span>
            <span>0.3</span>
            <span>0.4</span>
            <span>0.5</span>
            <span>0.6</span>
            <span>0.7</span>
            <span>0.8</span>
            <span>0.9</span>
            <span>1.0</span>
            <span class="fin"></span>
        </div>
    </td>
</tr>--%>
<!--Ethnicity #7  -->
<tr>
    <td width="5%" style="font-size:20px">Ethnicity</td>
    <td width="5%">
        <label class="checkbox">
            <input id="chk7" type="checkbox" value="" size="5">
        </label>
    </td>
    <td width="5%" style="font-size:20px">type:</td>
    <td width="10%">
        <select disabled id="ethnicity" name="ethnicity" class="input-xlarge">
            <option value="" selected="selected">Please Choose...</option>
            <option>Hispanic</option>
            <option>Non-hispanic</option>
        </select>
    </td>
    <td width="20%" style="font-size:20px">&nbsp;&nbsp;&nbsp;weight:
        <input type="text" id="s7" name="ethnicityWeight" readonly="readonly"
               style="width: 80px; height: 20px; color: #f6931f; font-weight: bold; font-size: 20px;">
    </td>
    <td width="45%">
        <div class="slider-range-max" id="s7Slider">
        </div>
        <div class="slide-text">
            <span>0.1</span>
            <span>0.2</span>
            <span>0.3</span>
            <span>0.4</span>
            <span>0.5</span>
            <span>0.6</span>
            <span>0.7</span>
            <span>0.8</span>
            <span>0.9</span>
            <span>1.0</span>
            <span class="fin"></span>
        </div>
    </td>
</tr>

<!--Race #8  -->
<tr>
    <td width="5%" style="font-size:20px">Race</td>
    <td width="5%">
        <label class="checkbox">
            <input id="chk8" type="checkbox" value="" size="5">
        </label>
    </td>
    <td width="5%" style="font-size:20px">type:</td>
    <td width="10%">
        <select disabled id="race" name="race" class="input-xlarge">
            <option value="" selected="selected">Please Choose...</option>
            <option>White</option>
            <option>Black or African American</option>
            <option>American Indian and Alaska Native</option>
            <option>Asian</option>
            <option>Native Hawaiian and Other Pacific Islander</option>
            <option>Some Other Race</option>
        </select>
    </td>
    <td width="20%" style="font-size:20px">&nbsp;&nbsp;&nbsp;weight:
        <input type="text" id="s8" name="raceWeight" readonly="readonly"
               style="width: 80px; height: 20px; color: #f6931f; font-weight: bold; font-size: 20px;">
    </td>
    <td width="45%">
        <div class="slider-range-max" id="s8Slider">
        </div>
        <div class="slide-text">
            <span>0.1</span>
            <span>0.2</span>
            <span>0.3</span>
            <span>0.4</span>
            <span>0.5</span>
            <span>0.6</span>
            <span>0.7</span>
            <span>0.8</span>
            <span>0.9</span>
            <span>1.0</span>
            <span class="fin"></span>
        </div>
    </td>
</tr>
<!--Population #9  -->
<tr>
    <td width="5%" style="font-size:20px">Population</td>
    <td width="5%">
        <label class="checkbox">
            <input id="chk9" type="checkbox" value="" size="5">
        </label>
    </td>
    <td width="5%" style="font-size:20px">type:</td>
    <td width="10%">
        <select disabled id="population" name="population" class="input-xlarge">
            <option value="" selected="selected">Not decided yet</option>
            <option>Less than 10,000</option>
            <option>10,000 to 99,999</option>
            <option>109,000 to 499,999</option>
            <option>500,000 to 999,999</option>
            <option>1,000,000 or more</option>
        </select>
    </td>
    <td width="20%" style="font-size:20px">&nbsp;&nbsp;&nbsp;weight:
        <input type="text" id="s9" name="populationWeight" readonly="readonly"
               style="width: 80px; height: 20px; color: #f6931f; font-weight: bold; font-size: 20px;">
    </td>
    <td width="45%">
        <div class="slider-range-max" id="s9Slider">
        </div>
        <div class="slide-text">
            <span>0.1</span>
            <span>0.2</span>
            <span>0.3</span>
            <span>0.4</span>
            <span>0.5</span>
            <span>0.6</span>
            <span>0.7</span>
            <span>0.8</span>
            <span>0.9</span>
            <span>1.0</span>
            <span class="fin"></span>
        </div>
    </td>
</tr>
<!--House #10  -->
<tr>
    <td width="5%" style="font-size:20px">House</td>
    <td width="5%">
        <label class="checkbox">
            <input id="chk10" type="checkbox" value="" size="5">
        </label>
    </td>
    <td width="5%" style="font-size:20px">type:</td>
    <td width="10%">
        <select disabled id="house" name="house" class="input-xlarge">
            <option value="" selected="selected">Please Choose....</option>
            <option>Less than 10,000 </option>
            <option>10,000 to 99,999</option>
            <option>109,000 to 499,999</option>
            <option>500,000 to 999,999</option>
            <option>1,000,000 or more</option>
            <!--            <option value="">3</option>
                        <option value="">4</option>
                        <option value="">5</option>-->
        </select>
    </td>
    <td width="20%" style="font-size:20px">&nbsp;&nbsp;&nbsp;weight:
        <input type="text" id="s10" name="houseWeight" readonly="readonly"
               style="width: 80px; height: 20px; color: #f6931f; font-weight: bold; font-size: 20px;">
    </td>
    <td width="45%">
        <div class="slider-range-max" id="s10Slider">
        </div>
        <div class="slide-text">
            <span>0.1</span>
            <span>0.2</span>
            <span>0.3</span>
            <span>0.4</span>
            <span>0.5</span>
            <span>0.6</span>
            <span>0.7</span>
            <span>0.8</span>
            <span>0.9</span>
            <span>1.0</span>
            <span class="fin"></span>
        </div>
    </td>
</tr>


</tbody>
</table>


<input type="submit" value="Submit"/>
</form>

</fieldset>

<hr>
<footer>
    <p>� Company 2013</p>
</footer>
</div>

<div id="slider-range-max"></div>
<!-- /container -->
</body>

<!--slider script -->


<script>

    $(".slider-range-max").each(function (idx, elm) {
        var name = elm.id.replace('Slider', '');
        $('#' + elm.id).slider({
            range: "max",
            min: 0.1,
            max: 1.0,
            value: 0.1,
            step: 0.1,
            disabled: true,
            slide: function (event, ui) {
                $('#' + name).val(ui.value);
            }
        });
    });
    ////       /* $(".amount").val($(".slider-range-max").slider("value"));
    //        var div = event.target.parentNode;
    //        $(div, '.amount').val($(event.target).slider("value"));
    //    });*/


</script>

<script type="text/javascript">


    $("#chk1").click(function () {
        if ($(this).is(':checked')) {
            $("#s1Slider").slider("enable");
            $("#age").prop("disabled", false)
        }
        else if ($(this).not(':checked')) {
            $("#s1Slider").slider("disable");
            $("#age").prop("disabled", true);
            $("#age").val('');
            $("#s1").val('');
        }
    });

    $("#chk2").click(function () {
        if ($(this).is(':checked')) {
            $("#s2Slider").slider("enable");
            $("#education").prop("disabled", false)
        }
        else if ($(this).not(':checked')) {
            $("#s2Slider").slider("disable");
            $("#education").prop("disabled", true);
            $("#education").val('');
            $("#s2").val('');
        }
    });

    $("#chk3").click(function () {
        if ($(this).is(':checked')) {
            $("#s3Slider").slider("enable");
            $("#sex").prop("disabled", false)
        }
        else if ($(this).not(':checked')) {
            $("#s3Slider").slider("disable");
            $("#sex").prop("disabled", true);
            $("#sex").val('');
            $("#s3").val('');
        }
    });

    $("#chk4").click(function () {
        if ($(this).is(':checked')) {
            $("#s4Slider").slider("enable");
            $("#income").prop("disabled", false)
        }
        else if ($(this).not(':checked')) {
            $("#s4Slider").slider("disable");
            $("#income").prop("disabled", true);
            $("#income").val('');
            $("#s4").val('');
        }
    });

    $("#chk5").click(function () {
        if ($(this).is(':checked')) {
            $("#s5Slider").slider("enable");
            $("#weather").prop("disabled", false)
        }
        else if ($(this).not(':checked')) {
            $("#s5Slider").slider("disable");
            $("#weather").prop("disabled", true);
            $("#weather").val('');
            $("#s5").val('');
        }
    });

    $("#chk6").click(function () {
        if ($(this).is(':checked')) {
            $("#s6Slider").slider("enable");
            $("#temperature").prop("disabled", false)
        }
        else if ($(this).not(':checked')) {
            $("#s6Slider").slider("disable");
            $("#temperature").prop("disabled", true);
            $("#temperature").val('');
            $("#s6").val('');
        }
    });

    $("#chk7").click(function () {
        if ($(this).is(':checked')) {
            $("#s7Slider").slider("enable");
            $("#ethnicity").prop("disabled", false)
        }
        else if ($(this).not(':checked')) {
            $("#s7Slider").slider("disable");
            $("#ethnicity").prop("disabled", true);
            $("#ethnicity").val('');
            $("#s7").val('');
        }
    });

    $("#chk8").click(function () {
        if ($(this).is(':checked')) {
            $("#s8Slider").slider("enable");
            $("#race").prop("disabled", false)
        }
        else if ($(this).not(':checked')) {
            $("#s8Slider").slider("disable");
            $("#race").prop("disabled", true);
            $("#race").val('');
            $("#s8").val('');
        }
    });

    $("#chk9").click(function () {
        if ($(this).is(':checked')) {
            $("#s9Slider").slider("enable");
            $("#population").prop("disabled", false)
        }
        else if ($(this).not(':checked')) {
            $("#s9Slider").slider("disable");
            $("#population").prop("disabled", true);
            $("#population").val('');
            $("#s9").val('');
        }
    });

    $("#chk10").click(function () {
        if ($(this).is(':checked')) {
            $("#s10Slider").slider("enable");
            $("#house").prop("disabled", false)
        }
        else if ($(this).not(':checked')) {
            $("#s10Slider").slider("disable");
            $("#house").prop("disabled", true);
            $("#house").val('');
            $("#s10").val('');
        }
    });


</script>


<table border="1">
    <c:forEach var="profile" items="${demographicProfileList}">
        <tr>
            <td>${profile.age}</td>
            <td><input type="button" value="delete" onclick="window.location='adprefprofile/?${profile.id}'"/></td>
        </tr>
    </c:forEach>
</table>

</html>