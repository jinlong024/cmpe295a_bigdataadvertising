<%--
  Created by IntelliJ IDEA.
  User: macbookpro
  Date: 10/3/13
  Time: 9:29 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title>Accordion Wizard for Bootstrap</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <style type="text/css">.slide-text {
        text-align: justify;
        padding: 0;
        margin: 0 -10px;
    }

    .slide-text > span {
        display: inline-block;
        margin: auto;
        padding: 0;
        overflow: hidden;
        white-space: nowrap;
    }

    .slide-text > .fin {
        display: inline-block;
        width: 100%;
    }

    .slider-range-max {
        width: 100%;
        height: 12px;
    }

    .body {
        background: lightskyblue;
    }
    </style>


    <%--<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css">--%>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    <link href="css/acc-wizard.min.css" rel="stylesheet">
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css"/>
    <link rel="stylesheet" href="css/bootstrap-switch.css" rel="stylesheet">
    <link rel="stylesheet" href="css/flat-ui-fonts.css" rel="stylesheet">

    <%--<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" />--%>


    <%--<link rel="stylesheet" href="http://getbootstrap.com/2.3.2/assets/js/google-code-prettify/prettify.css"/>--%>

    <%--<link rel="stylesheet" href="http://getbootstrap.com/2.3.2/assets/css/docs.css"/>--%>

    <%--<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css">--%>


    <style>[touch-action="none"] {
        -ms-touch-action: none;
        touch-action: none;
    }

    [touch-action="pan-x"] {
        -ms-touch-action: pan-x;
        touch-action: pan-x;
    }

    [touch-action="pan-y"] {
        -ms-touch-action: pan-y;
        touch-action: pan-y;
    }

    [touch-action="scroll"], [touch-action="pan-x pan-y"], [touch-action="pan-y pan-x"] {
        -ms-touch-action: pan-x pan-y;
        touch-action: pan-x pan-y;
    }</style>

<%--
    <style type="text/css">
        .navbar-inner
        {
            background-color: deepskyblue;
            /* remove the gradient */
            background-image: none;
            /* set font color to white */
            color: #ffffff;
            border-radius: 0;

        }

            /* menu items */

            /* set the background of the menu items to pink and default color to white */

        .navbar .nav > li > a {
            /*background-color: pink;*/
            color: #ffffff;
            padding-left: 20px;
            padding-right: 20px;


        }


            /* set hover and focus to lightblue */
        .navbar .nav > li > a:focus,
        .navbar .nav > li > a:hover {
            background-color: #0083fe;
            color: #f0f0f0;
            text-decoration: none;

        }
            /* set active item to darkgreen */
        .navbar .nav > .active > a,
        .navbar .nav > .active > a:hover,
        .navbar .nav > .active > a:focus {

            background-color: transparent;
            color: #ffffff;
        }

            /* set font color and background of the project name (brand) */

        .navbar .brand
        {
            color: #ffffff;
        }
        .navbar .btn btn-navbar {
            background-color: #eeeeee;
        }

    </style>
--%>

</head>


<body>
<div class="navbar  navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container">
            <button data-target=".nav-collapse" data-toggle="collapse" class="btn btn-navbar" type="button">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="#" class="brand" style="font-family: Flat-UI-Icons; font-size: 30px;color: #0e90d2">AD Big Data</a>

            <div class="nav-collapse collapse">
                <ul class="nav">
                    <li class="active">
                        <a href="adPlanning">Home</a>
                    </li>
                    <li>
                        <a href="adPlanning">AD Planning</a>
                    </li>
                    <li>
                        <a href="/BigDataAdvertising/advHomepage">Recommend</a>
                    </li>
                    <li>
                        <a href="adEvaluation">Evaluate</a>
                    </li>
                    <li>
                        <a href="/BigDataAdvertising/TwitterDataCollection">Data Collection</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
</div>

<div class="container">
<div class="row-fluid">
    <p style="font-size: 25px">
        Please follow this wizard to plan your advertisement
    </p>
</div>
<div class="row-fluid">
    <p style="font-size: 25px">
        &nbsp;
    </p>
</div>
<div class="row-fluid">
    <p style="font-size: 25px">
        Please follow this wizard to plan your advertisement
    </p>
</div>

<div class="row-fluid acc-wizard">
<div class="span2" style="padding-left: 2em;">

    <ol class="acc-wizard-sidebar">
        <li class="acc-wizard-todo acc-wizard-active"><a href="#step1">Product Info</a>
        </li>
        <li class="acc-wizard-todo"><a href="#step2">Product Profile</a>
        </li>
        <li class="acc-wizard-todo"><a href="#step3">Confirm</a>
        </li>
        <%--class="accordion-toggle" data-toggle="collapse"--%>

        <%--            back up for more option
                        <li class="acc-wizard-complete"><a href="http://sathomas.me/acc-wizard/#viewpage">Release</a></li>
                        --%>
    </ol>
</div>
<div class="span9">

<form action="adPlanning" method="post">

<div class="accordion" id="whole">
<div class="accordion-group">
    <div class="accordion-heading">
        <a class="accordion-toggle" style="font-size: 22px" data-toggle="collapse" data-parent="#whole"
           href="#step1">
            Step 1: Input Your Product Information
        </a>
    </div>
    <div id="step1" class="accordion-body in collapse" style="height: auto;">

        <%--step 1 Form content begin--%>

        <div class="accordion-inner">
            <%--<form id="form-prerequisites">--%>
            <h3> General Information</h3>

            <div class="control-group">
                <div>
                    <!-- Text input-->
                    <label class="control-label" for="productName" style="font-size: 23px">Product
                        Name:</label>
                </div>
                <div class="controls">
                    <input type="text" id="productName" name="productName"
                           placeholder="" class="input-xlarge"
                           style="width: 270px; height: 28px; color: #555555;  font-size: 18px;">

                    <p class="help-block"></p>
                </div>
                <label class="control-label" for="productModel" style="font-size: 23px">Model Name:</label>

                <div class="controls">
                    <input type="text" id="productModel" name="productModel"
                           placeholder="" class="input-xlarge"
                           style="width: 270px; height: 28px; color: #555555;  font-size: 18px;">

                    <p class="help-block"></p>
                </div>

                <label class="control-label" for="productManufacturer" style="font-size: 23px">Manufacturer:</label>

                <div class="controls">
                    <input type="text" id="productManufacturer" name="productManufacturer"
                           placeholder="" class="input-xlarge"
                           style="width: 270px; height: 28px; color: #555555;  font-size: 18px;">
                </div>
            </div>

            <div>
                <div>
                    <label class="control-label" for="productCategory" style="font-size: 23px">Product Category
                        :</label>
                </div>
                <p class="help-block"></p>

                <div>
                    <select id="productCategory" name="productCategory">
                        <option value="">Please Choose...</option>
                        <option>Beauty</option>
                        <option>Clothing</option>
                        <option>Electronics</option>
                        <option>Food</option>
                        <option>Home</option>
                    </select>
                </div>
                <div class="acc-wizard-step">
                    <%--<button class="btn btn-primary" type="submit">Next Step</button>--%>
                    <a class="btn btn-primary" class="accordion-toggle" data-toggle="collapse" data-parent="#whole"
                       href="#step2">Next
                        Step</a>

                </div>
            </div>
            <%--</form>--%>
        </div>
        <!--/.accordion-inner -->

        <%--end of step 1 Form--%>

    </div>
    <!-- /#prerequisites -->
</div>
<!-- /.accordion-group -->

<div class="accordion-group">
<div class="accordion-heading">
    <a class="accordion-toggle" style="font-size: 22px" data-toggle="collapse" data-parent="#whole" href="#step2">
        Step 2: Set up your AD Preference Profile
    </a>
</div>
<div id="step2" class="accordion-body collapse" style="height: 0px;">
<div class="row-fluid">
<%--<form id="form-step2">--%>

<table class="table table-condensed">
<tbody>
<!--Age number 1-->

<tr>

    <td class="span1 offset 1" style="font-size:20px; text-align:center">
        Age
    </td>

    <td class="span2" style="font-size:20px">
        <select id="age" name="age" class="input">
            <option value="">Please Choose...</option>
            <option>Infant (Under 2)</option>
            <option>Child (2-11)</option>
            <option>Youth (12-17)</option>
            <option>Adult (18-64)</option>
            <option>Senior (65 or over)</option>
        </select>
    </td>

    <td class="span3" style="font-size:20px"> weight: &nbsp;
        <div class="make-switch" id="chk1">
            <input type="checkbox" id="c1">
        </div>
    </td>

    <td class="span1">
        <input type="text" id="s1" name="ageWeight" readonly="readonly"
               style="width: 60px; height: 20px; color: #f6931f;     font-weight: bold; font-size: 20px;">
    </td>

    <td class="span3 offset 3">
        <div class="slider-range-max" id="s1Slider">
        </div>
        <div class="slide-text">
            <span> Min 0.1</span>
            <span>Max 1.0&nbsp;&nbsp;</span>

            <span class="fin"></span>
        </div>
    </td>

</tr>

<%--education #2--%>

<tr>

    <td class="span1" style="font-size:20px; text-align: center">Education
    </td>

    <td class="span2" style="font-size:20px">
        <select id="education" name="education" class="input">
            <option value="" selected="selected">Please Choose...</option>
            <option>Less than high school</option>
            <option>High school graduate</option>
            <option>Some college</option>
            <option>Bachelor's degree or higher</option>
        </select>
    </td>

    <td class="span3" style="font-size:20px"> weight: &nbsp;
        <div class="make-switch" id="chk2">
            <input type="checkbox" id="c2">
        </div>
    </td>

    <td class="span1">
        <input type="text" id="s2" name="educationWeight" readonly="readonly"
               style="width: 60px; height: 20px; color: #f6931f;     font-weight: bold; font-size: 20px;">
    </td>

    <td class="span3 offset3">
        <div class="slider-range-max" id="s2Slider">
        </div>
        <div class="slide-text">
            <span>Min 0.1</span>
            <span>Max 1.0&nbsp;&nbsp;</span>
            <span class="fin"></span>
        </div>
    </td>

</tr>


<%--gender #3--%>

<tr>
    <td class="span1 offset1" style="font-size:20px; text-align: center">
        Gender
    </td>

    <td class="span2" style="font-size:20px">
        <select id="sex" name="sex" class="input">
            <option value="" selected="selected">Please Choose...</option>
            <option>Male</option>
            <option>Female</option>
        </select>
    </td>

    <td class="span3" style="font-size:20px"> weight: &nbsp;
        <div class="make-switch" id="chk3">
            <input type="checkbox" id="c3">
        </div>
    </td>

    <td class="span1">
        <input type="text" id="s3" name="sexWeight" readonly="readonly"
               style="width: 60px; height: 20px; color: #f6931f;     font-weight: bold; font-size: 20px;">
    </td>

    <td class="span3 offset3">
        <div class="slider-range-max" id="s3Slider">
        </div>
        <div class="slide-text">
            <span> Min 0.1</span>
            <span>Max 1.0&nbsp;&nbsp;</span>
            <span class="fin"></span>
        </div>
    </td>
</tr>
<!--Income number4 -->
<tr>
    <td class="span1" style="font-size:20px; text-align: center">
        Income
    </td>

    <td class="span3" style="font-size:20px">
        <select id="income" name="income" class="input">
            <option value="" selected="selected">Please Choose...</option>
            <option>Less than $10,000</option>
            <option>$10,000 to $49,999</option>
            <option>$50,000 to $99,999</option>
            <option>$100,000 to $199,999</option>
            <option>$200,000 or more</option>
        </select>
    </td>

    <td class="span3" style="font-size:20px"> weight: &nbsp;
        <div class="make-switch" id="chk4">
            <input type="checkbox" id="c4">
        </div>
    </td>

    <td class="span1" style="font-size:20px">
        <input type="text" id="s4" name="incomeWeight" readonly="readonly"
               style="width: 60px; height: 20px; color: #f6931f; font-weight: bold; font-size: 20px;">
    </td>

    <td class="span3 offset3">
        <div class="slider-range-max" id="s4Slider">
        </div>
        <div class="slide-text">
            <span>Min 0.1</span>
            <span>Max 1.0&nbsp;&nbsp;</span>
            <span class="fin"></span>
        </div>
    </td>
</tr>
<!--weather #5  -->
<tr>
    <td class="span1" style="font-size:20px; text-align: center">
        Weather
    </td>
    <td class="span2" style="font-size:20px">
        <select id="weather" name="weather" class="input">
            <option value="" selected="selected">Please Choose...</option>
            <option>Coastal Weather</option>
            <option>Hot Weather</option>
            <option>Cold Weather</option>
            <option>Mountain Weather</option>
        </select>
    </td>

    <td class="span3" style="font-size:20px"> weight: &nbsp;
        <div class="make-switch" id="chk5">
            <input type="checkbox" id="c5">
        </div>
    </td>

    <td class="span1" style="font-size:20px">
        <input type="text" id="s5" name="weatherWeight" readonly="readonly"
               style="width:60px; height: 20px; color: #f6931f; font-weight: bold; font-size: 20px;">
    </td>

    <td class="span2">
        <div class="slider-range-max" id="s5Slider">
        </div>
        <div class="slide-text">
            <span>Min 0.1</span>
            <span>Max 1.0&nbsp;&nbsp;</span>
            <span class="fin"></span>
        </div>
    </td>
</tr>
<%--<!--Temperature #6  -->
<tr>
    <td class="span1" style="font-size:20px; text-align: center">
        Temperature
    </td>
    <td class="span2" style="font-size:20px">
        <select id="temperature" name="temperature" class="input-min">
            <option value="" selected="selected">Please Choose...</option>
            <option value="3">45F~67F</option>
            <option value="2">60F~90F</option>
            <option value="1">50F~72F</option>
            <option value="0">30F~50F</option>
        </select>
    </td>

    <td class="span3" style="font-size:20px"> weight: &nbsp;
        <div class="make-switch" id="chk6">
            <input type="checkbox" id="c6">
        </div>
    </td>

    <td class="span1" style="font-size:20px">
        <input type="text" id="s6" name="temperatureWeight" readonly="readonly"
               style="width: 60px; height: 20px; color: #f6931f; font-weight: bold; font-size: 20px;">
    </td>

    <td class="span3 offset3">
        <div class="slider-range-max" id="s6Slider">
        </div>
        <div class="slide-text">
            <span>Min 0.1</span>
            <span>Max 1.0&nbsp;&nbsp;</span>
            <span class="fin"></span>
        </div>
    </td>
</tr>--%>
<!--Ethnicity #7  -->
<tr>
    <td class="span1" style="font-size:20px; text-align: center">
        Ethnicity
    </td>

    <td class="span2" style="font-size:20px">
        <select id="ethnicity" name="ethnicity" class="input">
            <option value="" selected="selected">Please Choose...</option>
            <option>Hispanic</option>
            <option>Non-hispanic</option>
        </select>
    </td>

    <td class="span3" style="font-size:20px"> weight: &nbsp;
        <div class="make-switch" id="chk7">
            <input type="checkbox" id="c7">
        </div>
    </td>

    <td class="span1" style="font-size:20px">
        <input type="text" id="s7" name="ethnicityWeight" readonly="readonly"
               style="width: 60px; height: 20px; color: #f6931f; font-weight: bold; font-size: 20px;"
               disabled="disabled"/>
    </td>

    <td class="span3 offset3">
        <div class="slider-range-max" id="s7Slider">
        </div>
        <div class="slide-text">
            <span>Min 0.1</span>
            <span>Max 1.0&nbsp;&nbsp;</span>
            <span class="fin"></span>
        </div>
    </td>
</tr>

<!--Race #8  -->
<tr>
    <td class="span1" style="font-size:20px; text-align: center">
        Race
    </td>

    <td class="span1" style="font-size:20px">
        <select id="race" name="race" class="input">
            <option value="" selected="selected">Please Choose...</option>
            <option>Native Hawaiian and Other Pacific Islander</option>
            <option>Black or African American</option>
            <option>American Indian and Alaska Native</option>
            <option>Asian</option>
            <option>White</option>
            <option>Some Other Race</option>
        </select>
    </td>

    <td class="span3" style="font-size:20px"> weight: &nbsp;
        <div class="make-switch" id="chk8">
            <input type="checkbox" id="c8">
        </div>
    </td>

    <td class="span1" style="font-size:20px">
        <input type="text" id="s8" name="raceWeight" readonly="readonly"
               style="width: 60px; height: 20px; color: #f6931f; font-weight: bold; font-size: 20px;">
    </td>

    <td class="span3 offset3">
        <div class="slider-range-max" id="s8Slider">
        </div>
        <div class="slide-text">
            <span>Min 0.1</span>
            <span>Max 1.0&nbsp;&nbsp;</span>
            <span class="fin"></span>
        </div>
    </td>
</tr>
<!--Population #9  -->
<tr>
    <td class="span1" style="font-size:20px; text-align: center">
        Population
    </td>

    <td class="span2" style="font-size:20px">
        <select id="population" name="population" class="input">
            <option value="" selected="selected">Not decided yet</option>
            <option>Less than 10,000</option>
            <option>10,000 to 99,999</option>
            <option>109,000 to 499,999</option>
            <option>500,000 to 999,999</option>
            <option>1,000,000 or more</option>
        </select>
    </td>

    <td class="span3" style="font-size:20px"> weight: &nbsp;
        <div class="make-switch" id="chk9">
            <input type="checkbox" id="c9">
        </div>
    </td>

    <td class="span1" style="font-size:20px">
        <input type="text" id="s9" name="populationWeight" readonly="readonly"
               style="width: 60px; height: 20px; color: #f6931f; font-weight: bold; font-size: 20px;">
    </td>

    <td class="span3 offset3">
        <div class="slider-range-max" id="s9Slider">
        </div>
        <div class="slide-text">
            <span>Min 0.1</span>
            <span>Max 1.0&nbsp;&nbsp;</span>
            <span class="fin"></span>
        </div>
    </td>
</tr>
<!--House #10  -->
<tr>
    <td class="span1" style="font-size:20px; text-align: center">
        House
    </td>

    <td class="span1" style="font-size:20px">
        <select id="house" name="house" class="input">
            <option value="" selected="selected">Please Choose....</option>
            <option>Less than 10,000</option>
            <option>10,000 to 99,999</option>
            <option>109,000 to 499,999</option>
            <option>500,000 to 999,999</option>
            <option>1,000,000 or more</option>
        </select>
    </td>

    <td class="span3" style="font-size:20px"> weight: &nbsp;
        <div class="make-switch" id="chk10">
            <input type="checkbox" id="c10">
        </div>
    </td>

    <td class="span1" style="font-size:20px">
        <input type="text" id="s10" name="houseWeight" readonly="readonly"
               style="width: 60px; height: 20px; color: #f6931f; font-weight: bold; font-size: 20px;">
    </td>

    <td class="span3 offset3">
        <div class="slider-range-max" id="s10Slider">
        </div>
        <div class="slide-text">
            <span>Min 0.1</span>
            <span>Max 1.0&nbsp;&nbsp;</span>
            <span class="fin"></span>
        </div>
    </td>
</tr>


</tbody>

</table>
<div class="acc-wizard-step">
    <a class="btn" class="accordion-toggle" data-toggle="collapse" data-parent="#whole" href="#step1">
        Go Back</a>
    <a class="btn btn-primary" class="accordion-toggle" data-toggle="collapse" data-parent="#whole" href="#step3">Next
        Step</a>

    <div>&nbsp; </div>
</div>

</div>

</div>
<%--</form>--%>
</div>
<!--/.accordion-inner -->

<!-- /#step2 -->
<!-- /.accordion-group -->

<div class="accordion-group">
    <div class="accordion-heading">
        <a class="accordion-toggle" style="font-size: 23px" data-toggle="collapse" data-parent="#whole" href="#step3">
            Step 3: Confirm your information
        </a>
    </div>
    <div id="step3" class="accordion-body collapse" style="height: 0px;">
        <div class="accordion-inner">
            Please review your information, and press submit button to continue.
            <%--<form id="form-step3">--%>

            <div class="acc-wizard-step">

                <button class="btn" type="reset">Go Back</button>
                <button class="btn btn-primary" type="submit">Submit</button>

            </div>
</form>
<%--</form>--%>
</div>
<!--/.accordion-inner -->
</div>
<!-- /#adjusthtml -->
</div>
<!-- /.accordion-group -->
</body>


<%--back up for STEP 4

<div class="accordion-group">
    <div class="accordion-heading">
        <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion-demo"
           href="http://sathomas.me/acc-wizard/#viewpage">
            Test Your Page
        </a>
    </div>
    <div id="test" class="accordion-body collapse" style="height: 0px;">
        <div class="accordion-inner">
            <form id="te">
                <p>
                    Naturally, the last thing you'll want to do is test your
                    page with the accordion wizard. Once you've confirmed that
                    it's working as expected, release it on the world. Your
                    users will definitely appreciate the feedback and guidance
                    it gives to multi-step and complex tasks on your web site.
                </p>
            </form>
        </div>
        <!--/.accordion-inner -->
    </div>
    <!-- /#viewpage -->
</div>
<!-- /.accordion-group -->
--%>


<script type="text/javascript" src="scripts/acc-wizard.min.js"></script>
<script type="text/javascript" src="scripts/jquery.min.js"></script>
<script type="text/javascript" src="scripts/bootstrap.min.js"></script>
<script src="http://code.jquery.com/jquery-1.9.1.js" type="text/javascript"></script>
<script src="scripts/jquery-ui.js" type="text/javascript"></script>
<script src="scripts/hidden.js" type="text/javascript"></script>
<script src="scripts/bootstrap-switch.js" type="text/javascript"></script>
<script src="http://getbootstrap.com/2.3.2/assets/js/google-code-prettify/prettify.js" type="text/javascript"></script>
<%--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>--%>


<script>
    function onNext(parent, panel) {
        hash = "#" + panel.id;
        $(".acc-wizard-sidebar", $(parent))
                .children("li")
                .children("a[href='" + hash + "']")
                .parent("li")
                .removeClass("acc-wizard-todo")
                .addClass("acc-wizard-completed");
    }
    $(window).load(function () {
        $(".acc-wizard").accwizard({onNext: onNext});
    })
</script>

<script>

    $(".slider-range-max").each(function (idx, elm) {
        var name = elm.id.replace('Slider', '');
        $('#' + elm.id).slider({
            range: "max",
            min: 0.1,
            max: 1.0,
            value: 0.1,
            step: 0.1,
            disabled: true,
            slide: function (event, ui) {
                $('#' + name).val(ui.value);
            }
        });
    });
    ////       /* $(".amount").val($(".slider-range-max").slider("value"));
    //        var div = event.target.parentNode;
    //        $(div, '.amount').val($(event.target).slider("value"));
    //    });*/


</script>
<script src="scripts/jquery.backstretch.js"></script>
<script>$.backstretch("images/light.jpeg")</script>

//TODO
</html>

