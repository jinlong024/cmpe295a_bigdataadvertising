<%--
  Created by IntelliJ IDEA.
  User: macbookpro
  Date: 12/9/13
  Time: 12:00 PM
  To change this template use File | Settings | File Templates.
--%>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Evaluate Input</title>

    <%--<link rel="stylesheet" type="text/css" href="css/bootstrap-select.min.css">--%>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">

    <style>
        .mid {
            width: auto;
            display: table;
            margin-left: auto;
            margin-right: auto;
            margin-top: 280px;
            margin-bottom: auto;
        }
    </style>
</head>

<body>
<div class="navbar  navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container">
            <button data-target=".nav-collapse" data-toggle="collapse" class="btn btn-navbar" type="button">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="#" class="brand" style="font-family: Flat-UI-Icons; font-size: 30px;color: #0e90d2">AD Big Data</a>

            <div class="nav-collapse collapse">
                <ul class="nav">
                    <li class="active">
                        <a href="adPlanning">Home</a>
                    </li>
                    <li>
                        <a href="adPlanning">AD Planning</a>
                    </li>
                    <li>
                        <a href="/BigDataAdvertising/advHomepage">Recommend</a>
                    </li>
                    <li>
                        <a href="adEvaluation">Evaluate</a>
                    </li>
                    <li>
                        <a href="/BigDataAdvertising/TwitterDataCollection">Data Collection</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="mid">
        <div class="row">
            <form action='adEvaluation/yelp/location/show' method='POST'>

                <div class="row">
                    <div class="span2" style="font-size: 23px ; text-align: center ">Data Source</div>
                    <div class="span2">
                        <select id="datasource" name="datasource" type="select" class=" validate['required']"
                                onchange="ChangeDropdowns(this.value)">
                            <option>Please Choose...</option>
                            <option>Twitter</option>
                            <option>Yelp</option>
                        </select>
                    </div>
                </div>

                <div id="dimension-div" class="sub-1" name="stylesub1" style="display: none">
                    <div class="span2" style="font-size: 23px ">Dimension</div>
            <span>
            <select id="dimension" name="dimension" data-style="btn-success" onchange="ChangeDropdowns1(this.value)">
                <option value="">Please Choose...</option>
                <option>Category</option>
                <option>Time</option>
                <option>Location</option>
            </select>
                </span>
                </div>

                <div id="timeunit-div" style="display: none" name="stylesub1">
                    <div class="span2" style="font-size: 23px ; text-align: center">Time Unit</div>
            <span>
            <select class="selectpicker" id="timeunit" name="timeunit" data-style="btn-success">
                <option value="">Please Choose...</option>
                <option>Hourly</option>
                <option>Daily</option>
            </select>
            </span>
                </div>

                <div id="city-div" class="row" style="display: none">
                    <div class="span2" style="font-size: 23px; text-align: center">City</div>
                    <div class="span2">
                        <select id="city" name="city" type="select">
                            <option>Please Choose...</option>
                            <option>Mesa</option>
                            <option>Avondale</option>
                            <option>Chandler</option>
                            <option>Scottsdale</option>
                            <option>Phoenix</option>
                        </select>
                    </div>
                </div>

                <div id="timerange-div" class="row" style="display: none; text-align: center">
                    <div class="span2" style="font-size: 23px ">Time Range</div>
                    <div class="span2">
                        <select id="timerange" name="timerange" type="select">
                            <option>Please Choose...</option>
                            <option>Daily</option>
                            <option>Monthly</option>
                        </select>
                    </div>
                </div>

                <div>&nbsp;</div>
                <div>&nbsp;</div>


                <%--            <a  class="btn btn-small" href="/BigDataAdvertising/map?id=${adPrefProfile.id}">Recommend </a>--%>
                <center>
                    <button class="btn btn-success btn-large btn-block" type="submit">Submit</button>
                </center>
                <div>&nbsp;</div>
                <a class="btn btn-large btn-block" type="button" href="/BigDataAdvertising/advHomepage">Back</a>

            </form>
        </div>
    </div>
</div>
</body>

<script src="scripts/bootstrap-select.js"></script>
<script src="scripts/bootstrap.min.js"></script>
<script src="scripts/jquery-1.9.1.js"></script>


<script type="text/javascript">
    function ChangeDropdowns(value) {
        if (value == "Yelp") {
            document.getElementById('dimension-div').style.display = 'block';
            document.getElementById('timeunit-div').style.display = 'none';
        } else {
            document.getElementById('dimension-div').style.display = 'none';
            document.getElementById('timeunit-div').style.display = 'block';
        }
    }
</script>

<script type="text/javascript">
    function ChangeDropdowns1(value) {
        if (value == "Time") {
            document.getElementById('timerange-div').style.display = 'block';
            document.getElementById('city-div').style.display = 'none';
        }
        ;
        if (value == "Location") {
            document.getElementById('city-div').style.display = 'block';
            document.getElementById('timerange-div').style.display = 'none';
        }
        if (value == "Category") {
            document.getElementById('city-div').style.display = 'none';
            document.getElementById('timerange-div').style.display = 'none';
        }
    }
</script>
<script src="scripts/jquery.backstretch.js"></script>
<script>$.backstretch("images/light.jpeg")</script>


</html>