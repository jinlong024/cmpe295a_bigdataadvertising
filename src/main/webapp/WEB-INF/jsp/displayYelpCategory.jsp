<%--
  Created by IntelliJ IDEA.
  User: macbookpro
  Date: 12/8/13
  Time: 2:37 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Yelp Category Review</title>
    <style type="text/css">
        body {
            padding-top: 60px;
            padding-bottom: 40px;
        }
    </style>

    <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
</head>

<body>
<div class="navbar  navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container">
            <button data-target=".nav-collapse" data-toggle="collapse" class="btn btn-navbar" type="button">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="#" class="brand" style="font-family: Flat-UI-Icons; font-size: 30px;color: #0e90d2">AD Big Data</a>

            <div class="nav-collapse collapse">
                <ul class="nav">
                    <li class="active">
                        <a href="adPlanning">Home</a>
                    </li>
                    <li>
                        <a href="adPlanning">AD Planning</a>
                    </li>
                    <li>
                        <a href="/BigDataAdvertising/advHomepage">Recommend</a>
                    </li>
                    <li>
                        <a href="adEvaluation">Evaluate</a>
                    </li>
                    <li>
                        <a href="/BigDataAdvertising/TwitterDataCollection">Data Collection</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row-fluid">
        <div id="chart_div" class="span2 " style="width: 900px; height: 500px;">
        </div>
    </div>

    <div class="row-fluid">
        <div class="span6 offset3">
            <a class="btn btn-success btn-large btn-block  " href="/BigDataAdvertising/adEvaluation"
               type="button">Back</a>
        </div>
    </div>
</div>

</body>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
    google.load("visualization", "1", {packages: ["corechart"]});
    google.setOnLoadCallback(drawChart);
    function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['Product Category', 'User Review', {role: 'annotation'}],
            ['${yelp[0].businessCategory}', ${yelp[0].userCount} , 'Feedbacklevel: ${yelp[0].feedbackLevel}'],
            ['${yelp[1].businessCategory}', ${yelp[1].userCount} , 'Feedbacklevel: ${yelp[1].feedbackLevel}'],
            ['${yelp[2].businessCategory}', ${yelp[2].userCount} , 'Feedbacklevel: ${yelp[2].feedbackLevel}'],
            ['${yelp[3].businessCategory}', ${yelp[3].userCount} , 'Feedbacklevel: ${yelp[3].feedbackLevel}'],
            ['${yelp[4].businessCategory}', ${yelp[4].userCount} , 'Feedbacklevel: ${yelp[4].feedbackLevel}']
        ]);

        var options = {
            title: 'Product User Count',
            hAxis: {title: 'Product Category', titleTextStyle: {color: 'black' }}
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
        chart.draw(data, options);
    }
</script>

<script src="http://code.jquery.com/jquery-1.9.1.js" type="text/javascript"></script>
<script src="//cdn.jsdelivr.net/jquery.backstretch/2.0.3/jquery.backstretch.min.js"></script>
<script>$.backstretch("images/light.jpeg")</script>


</html>