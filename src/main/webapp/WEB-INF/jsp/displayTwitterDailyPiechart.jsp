<%--
  Created by IntelliJ IDEA.
  User: macbookpro
  Date: 11/11/13
  Time: 2:01 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html lang="en">
<head>
    <title>Twitter Daily Review</title>
</head>
<link href="css/nv.d3.css" rel="stylesheet" type="text/css">
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet">

<style>

    body {
        overflow-y: scroll;
    }

    #tbl {
        margin-top: 100px;
        margin-bottom: auto;
    }

    #linechart {
        margin-left: 20px;
        margin-right: auto;
    }

        /*text {*/
        /*font: 12px sans-serif;*/
        /*}*/


</style>
<style type="text/css">
    .navbar-custom {
        background-color: #406ce5;
        color: #ffffff;
        border-radius: 0;
    }

    .navbar-custom .navbar-nav > li > a {
        color: #ffffff;
        padding-left: 20px;
        padding-right: 20px;
    }

    .navbar-custom .navbar-nav > .active > a, .navbar-nav > .active > a:hover, .navbar-nav > .active > a:focus {
        color: #ffffff;
        background-color: transparent;
    }

    .navbar-custom .navbar-nav > li > a:hover, .nav > li > a:focus {
        text-decoration: none;
        background-color: #0083fe;
    }

    .navbar-custom .navbar-brand {
        color: #eeeeee;
    }

    .navbar-custom .navbar-toggle {
        background-color: #eeeeee;
    }

</style>
<%--<link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet">--%>

<body>

<nav class="navbar navbar-custom navbar-fixed-top">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            &lt;%&ndash;<span class="sr-only">Toggle navigation</span>&ndash;%&gt;
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" style="font-family: Flat-UI-Icons; font-size: 30px;color: #0e90d2">AD Big Data</a>
    </div>
    <div class="collapse navbar-collapse">
        <ul class="nav navbar-nav">
            <li class="active">
                <a href="adPlanning">Home</a>
            </li>
            <li>
                <a href="adPlanning">AD Planning</a>
            </li>
            <li>
                <a href="/BigDataAdvertising/advHomepage">Recommend</a>
            </li>
            <li>
                <a href="adEvaluation">Evaluate</a>
            </li>
            <li>
                <a href="/BigDataAdvertising/TwitterDataCollection">Data Collection</a>
            </li>
        </ul>
    </div>
</nav>
<div class="row">
    &nbsp;
</div>
<div class="row">
    &nbsp;
</div>
<div class="container">
    <div class="row">
        <div class="col-md-8" id="piechart" style="width: 700px; height: 350px;">

        </div>
        <div class="col-md-4">
            <table id="tbl" class="table table-bordered  table-condensed" style="width: 500px">
                <tr>
                    <td>Product Name</td>
                    <td>Model Name</td>
                    <td>Manufacturer Name</td>
                    <td>Product Category</td>
                    <td>User Review</td>
                </tr>
                <tr>
                    <td>${feedback[0].productName}</td>
                    <td>${feedback[0].productModel}</td>
                    <td>${feedback[0].productManufacturer}</td>
                    <td>${feedback[0].productCategory}</td>
                    <td>${feedback[0].userCount}</td>

                </tr>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4" id="linechart" style="width: 700px; height: 350px;">
        </div>
        <div class="col-md-4">             <a class="btn btn-success btn-large btn-block " href="/BigDataAdvertising/adEvaluation" type="button">Back</a> </div>
    </div>
</div>
</body>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
    google.load("visualization", "1", {packages: ["corechart"]});
    google.setOnLoadCallback(drawChart);
    function drawChart() {

        var data = google.visualization.arrayToDataTable([
            ['Task', 'Hours per Day'],
            ['Strong Like', ${feedback[0].feedbackLevel.stronglyLikeCount} ],
            ['Like', ${feedback[0].feedbackLevel.likeCount} ],
            ['Neutral', ${feedback[0].feedbackLevel.neutralCount}],
            ['Dislike', ${feedback[0].feedbackLevel.dislikeCount}],
            ['Strongly Dislike', ${feedback[0].feedbackLevel.stronglyDislikeCount}]
        ]);


        var options = {
            title: 'Twitter User FeedBack'
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));
        chart.draw(data, options);
    }
</script>

<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
    google.load("visualization", "1", {packages: ["corechart"]});
    google.setOnLoadCallback(drawChart);
    function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['Year', 'Strong Like', 'Like' , 'Neutral', 'Dislike'  , 'Strongly Dislike' ],
            ['11/30/2013', ${feedback[0].feedbackLevel.stronglyLikeCount}, ${feedback[1].feedbackLevel.likeCount}, ${feedback[0].feedbackLevel.neutralCount}, ${feedback[0].feedbackLevel.dislikeCount},  ${feedback[0].feedbackLevel.stronglyDislikeCount}                                                ],
            ['12/1/2013', ${feedback[1].feedbackLevel.stronglyLikeCount}, ${feedback[1].feedbackLevel.likeCount}, ${feedback[1].feedbackLevel.neutralCount}, ${feedback[1].feedbackLevel.dislikeCount},    ${feedback[1].feedbackLevel.stronglyDislikeCount}                                           ],
            ['12/2/2013', ${feedback[2].feedbackLevel.stronglyLikeCount}, ${feedback[1].feedbackLevel.likeCount}, ${feedback[2].feedbackLevel.neutralCount}, ${feedback[2].feedbackLevel.dislikeCount} ,   ${feedback[2].feedbackLevel.stronglyDislikeCount}                                             ]
        ]);

        var options = {
            title: 'Twitter User Feedback',
            hAxis: {title: 'Date ', titleTextStyle: {color: '#333'}},
            vAxis: {minValue: 0}
        };

        var chart = new google.visualization.AreaChart(document.getElementById('linechart'));
        chart.draw(data, options);
    }
</script>

<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>

</html>
<%--
<html>
<head>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
        google.load("visualization", "1", {packages:["corechart"]});
        google.setOnLoadCallback(drawChart);
        function drawChart() {
            var data = google.visualization.arrayToDataTable([
                ['Task', 'Hours per Day'],
                &lt;%&ndash;['"${adPrefProfile.age}"',     "${adPrefProfile.ageWeight}"],&ndash;%&gt;
                ['Eat',      2],
                ['Commute',  2],
                ['Watch TV', 2],
                ['Sleep',    7]
            ]);

            var options = {
                title: 'My Daily Activities'
            };

            var chart = new google.visualization.PieChart(document.getElementById('piechart'));
            chart.draw(data, options);
        }
    </script>
</head>
<body>
<div id="piechart" style="width: 900px; height: 500px;"></div>
</body>
</html>--%>
