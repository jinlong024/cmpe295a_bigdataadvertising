<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html lang="en-US">
<head>
    <title>AdRecommend Login</title>
</head>
<body>
<link rel="stylesheet"
      href="https://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/css/bootstrap-combined.min.css">
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css"/>
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script class="cssdeck"
        src="http://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/2.3.1/js/bootstrap.min.js"></script>

<style type="text/css">
    .big-one {
        max-width: 600px;
        max-height: 900px;
        padding: 39px 29px 29px;
        margin-left: auto;
        margin-right: auto;
        margin-top: 60px;
        margin-bottom: auto;        /*background-color: #fff;*/
        /*border: 1px solid #e5e5e5;*/
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        border-radius: 5px;
        -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
        -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
        box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
    }
    .well {
        background-color: rgba(245, 245, 245, 0.8);
    }
</style>


<div class="" id=" ">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h3 style="font-size: 25px; color: lawngreen" >Welcome to Big Data Analytics Service System for Advertising and Marketing</h3>
    </div>
    <div class="big-one">
        <div class="well">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#login" data-toggle="tab" style="font-size: 23px;">Login</a></li>
                <li><a href="#create" data-toggle="tab" style="font-size: 23px;">Create Account</a></li>
            </ul>
            <div  class="tab-content">
                <div class="tab-pane active in" id="login">
                    <div>&nbsp;</div>
                    <form id="log" class="form-horizontal" action="login" method="POST">
                        <fieldset>
                            <%--<div id="legend">
                                <legend class="">Login</legend>
                            </div>--%>
                            <div class="control-group">
                                <!-- Username -->
                                <label class="control-label" for="emai">Username ( Email )</label>

                                <div class="controls">
                                    <input type="text" id="emai" name="email" placeholder="" class="input-xlarge">
                                </div>
                            </div>

                            <div class="control-group">
                                <!-- Password-->
                                <label class="control-label" for="pawd">Password</label>

                                <div class="controls">
                                    <input type="password" id="pawd" name="password" placeholder="" class="input-xlarge">
                                </div>
                            </div>


                            <div class="control-group">
                                <!-- Button -->
                                <div class="controls">
                                    <button class="btn btn-success" type="submit">&nbsp;Login&nbsp;</button>
                                    <%--<button class="btn btn-success" type="submit">Login</button>--%>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
                <div class="tab-pane fade" id="create">
                    <div>&nbsp;</div>
                    <form id="register" action="userProfile" method="POST">
                        <label for="email">Email (User name) *</label>
                        <input type="text" id="email" name="email" required="required" class="input-xlarge">
                        <label>Password *</label>
                        <input type="text" id="password" name="password" required="required" class="input-xlarge">
                        <label>Role *</label>
                        <select id="role" name="role" class="input-xlarge">
                            <option value="">--Please Select your role--</option>
                            <option value="admin">Administrator</option>
                            <option value="adver">Advertiser</option>
                            <!--<option value="3">Publisher</option>-->
                        </select>
                        <label>First Name *</label>
                        <input type="text" id="firstName" name="firstName" required="required" class="input-xlarge">
                        <label>Last Name *</label>
                        <input type="text" id="lastName" name="lastName" required="required" class="input-xlarge">
                        <label>Contact Information</label>
                        <textarea id="contact" name="contact" rows="2" class="input-xlarge">
                        </textarea>

                        <div>
                            <button class="btn btn-primary" type="submit">Create Account</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="scripts/jquery.backstretch.js"></script>
<script>$.backstretch("images/light.jpeg")</script>
</body>
</html>