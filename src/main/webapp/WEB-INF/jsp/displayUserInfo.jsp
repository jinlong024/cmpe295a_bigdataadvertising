<%--
  Created by IntelliJ IDEA.
  User: macbookpro
  Date: 9/23/13
  Time: 9:41 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>

    <meta charset="utf-8">
    <title>Homepage</title>
    <style type="text/css">
        body {
            padding-top: 60px;
            padding-bottom: 40px;
        }

        .slide-text {
            text-align: justify;
            padding: 0;
            margin: 0 -10px;
        }

        .slide-text > span {
            display: inline-block;
            margin: auto;
            padding: 0;
            overflow: hidden;
            white-space: nowrap;
        }

        .slide-text > .fin {
            display: inline-block;
            width: 100%;
        }

        .ui-slide {
            width: 100px;
            height: 12px;
        }
    </style>
    <link rel="stylesheet"
          href="https://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/css/bootstrap-combined.min.css">
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css"/>
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
</head>

<body>
<div class="navbar navbar-inverse navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container">
            <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span></span>
                <span></span>
                <span></span>
            </button>
            <a class="brand" href="#">Project name</a>

            <div class="nav-collapse collapse">
                <ul class="nav">
                    <li class="active">
                        <a href="#">Home</a>
                    </li>
                    <li>
                        <a href="">About</a>
                    </li>
                    <li>
                        <a href="">User</a>
                    </li>
                    <li>
                        <a href="">Contact</a>
                    </li>
                </ul>
                <!-- <form class="navbar-form pull-right">
                     <input class="span2" type="text" placeholder="Email">
                     <input class="span2" type="password" placeholder="Password">
                     <button type="submit" class="btn">Sign in</button>
                 </form>-->
                <form class="navbar-form pull-right">

                    <input class="span2" type="text" placeholder="Email"/>


                    <input class="span2" type="password" placeholder="Password"/>

                    <button type="submit" class="btn">Sign in</button>
                </form>
            </div>
            <!--/.nav-collapse -->
        </div>
    </div>
</div>


<div class="container">
    <fieldset>
        <div>
            <h2>
                <center>User Information List</center>
            </h2>
        </div>
    </fieldset>
</div>

<div class="container">
    <table class="table table-bordered  table-condensed" id="tbl">
        <tr>
            <td>#</td>
            <td>User email</td>
            <td>User Role</td>
            <td>User Firstname</td>
            <td>User Lastname</td>
            <td>Contact Information</td>
            <td sytle="text-align:center">
                Operation
            </td>
        </tr>
        <c:forEach var="user" items="${userList}" varStatus="status">
            <tr>
                <td>${status.count}</td>
                <td>${user.email}</td>
                <td>${user.role}</td>
                <td>${user.firstName}</td>
                <td>${user.lastName}</td>
                <td>${user.contact}</td>

                <td>
                    <%--<input type="button" value="delete" onclick="window.location='adminHomepage/delete?email=${user.email}'"/>--%>
                    <a class="btn btn-small" href='adminHomepage/delete?email=${user.email}'/>delete</a>
                    &nbsp;
                        <a data-toggle="modal" class="btn btn-small" href="#update" >update</a>

                    <%--<a class="btn btn-small" href="adminHomepage/findID?email=${user.email}" >update</a>--%>


                </td>
            </tr>
        </c:forEach>
    </table>
</div>

</div>

<div id="update" class="modal hide fade in" style="display: none; ">
    <div class="modal-header">
        <button type="button" class="close" aria-hidden="true" data-dismiss="modal">&times;</button>
        <h3>Edit Your Record Here</h3>
    </div>
    <div class="modal-body">
        <h4>Text in a modal</h4>
        <p>You can add some text here.</p>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn btn-success">Update</a>
        <a href="#" class="btn" data-dismiss="modal">Close</a>
    </div>
</div>

</body>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
</html>