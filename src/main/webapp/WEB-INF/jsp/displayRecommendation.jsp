<%--
  Created by IntelliJ IDEA.
  User: long
  Date: 10/7/13
  Time: 5:05 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="logo" type="image/png" href="demo.png"/>
    <title>Recommendation Radar Chart</title>
    <style>
        body {
            font-family: sans-serif
        }
    </style>
        <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
        <link href="css/bootstrap.min.css" rel="stylesheet">
<%--    <link href="http://sathomas.me/acc-wizard/css/bootstrap.min.css" rel="stylesheet">
    <link href="http://sathomas.me/acc-wizard/css/bootstrap-responsive.min.css" --%>rel="stylesheet">
</head>
<body>
<div class="navbar navbar-inverse navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container">
            <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span></span>
                <span></span>
                <span></span>
            </button>
            <a class="brand" href="#">Project name</a>

            <div class="nav-collapse collapse">
                <ul class="nav">
                    <li class="active">
                        <a href="#">User Home</a>
                    </li>
                    <li>
                        <a href="">Ad Preference</a>
                    </li>
                    <li>
                        <a href="">Record</a>
                    </li>
                    <li>
                        <a href="">Data Collection</a>
                    </li>
                </ul>
                <!-- <form class="navbar-form pull-right">
                     <input class="span2" type="text" placeholder="Email">
                     <input class="span2" type="password" placeholder="Password">
                     <button type="submit" class="btn">Sign in</button>
                 </form>-->
                <form class="navbar-form pull-right">

                    <input class="span2" type="text" placeholder="Email"/>


                    <input class="span2" type="password" placeholder="Password"/>

                    <button type="submit" class="btn">Sign in</button>
                </form>
            </div>
            <!--/.nav-collapse -->
        </div>
    </div>
</div>
<div class="container">
    <fieldset>
        <div class="row-fluid">
            <div class="span12">
                <h2>Example of radar chart</h2>
            </div>
            <div class="row-fluid">
                <div class="span6" id="chart"></div>
                <div class="span6"> sdfasdfasd</div>
            </div>
        </div>
    </fieldset>
</div>


<script src="scripts/d3.v3.min.js"></script>
<script src="scripts/radar-chart-min.js"></script>
<script>
    var d = [
        [
            {axis: "age", value: 13},
            {axis: "education", value: 1},
            {axis: "gender", value: 8},
            {axis: "income", value: 4},
            {axis: "weather", value: 9},
            {axis: "temperature", value: 9},
            {axis: "ethnicity", value: 9},
            {axis: "race", value: 9},
            {axis: "population", value: 9},
            {axis: "house", value: 9}
        ],

        [
            {axis: "age", value: 13},
            {axis: "education", value: 1},
            {axis: "gender", value: 8},
            {axis: "income", value: 4},
            {axis: "weather", value: 9},
            {axis: "temperature", value: 9},
            {axis: "ethnicity", value: 9},
            {axis: "race", value: 9},
            {axis: "population", value: 9},
            {axis: "house", value: 9}
        ]
    ];


    RadarChart.draw("#chart", d);


</script>

</body>
</html>