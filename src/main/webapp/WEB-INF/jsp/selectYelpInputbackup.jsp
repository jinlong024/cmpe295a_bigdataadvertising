<%--
  Created by IntelliJ IDEA.
  User: macbookpro
  Date: 12/9/13
  Time: 12:00 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Evaluate Input</title>

<link rel="stylesheet" type="text/css" href="css/bootstrap-select.min.css">
<link href="css/bootstrap.min.css" rel="stylesheet">

</head>

<body>
<div>
    <form action='/yelpReview/location/show' method='POST'>
        <div>
            <select class="selectpicker" id="YelpCategory" name="YelpCategory" data-style="btn-success">
                <option value="">Please Choose...</option>
                <option>Beauty</option>
                <option>Clothing</option>
                <option>Electronics</option>
                <option>Food</option>
                <option>Home</option>
            </select>
        </div>
        <div>
            <select class="selectpicker" id="YelpLocation" name="YelpLocation">
                <option value="">Please Choose...</option>
                <option>Beauty</option>
                <option>Clothing</option>
                <option>Electronics</option>
                <option>Food</option>
                <option>Home</option>
            </select>
        </div>
        <div>
<%--            <a  class="btn btn-small" href="/BigDataAdvertising/map?id=${adPrefProfile.id}">Recommend </a>--%>
            <button class="btn btn-primary" type="submit">Submit</button>

        </div>

    </form>
</div>

</body>

<script src="scripts/bootstrap-select.js"></script>
<script src="scripts/bootstrap.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>

<script type="text/javascript">
    $(window).on('load', function () {

        $('.selectpicker').selectpicker({
            'selectedText': 'cat'
        });

        // $('.selectpicker').selectpicker('hide');
    });
</script>

</html>