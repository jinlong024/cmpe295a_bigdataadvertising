<%--
  Created by IntelliJ IDEA.
  User: macbookpro
  Date: 11/11/13
  Time: 2:01 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html lang="en">
<head>
    <title></title>
</head>
<link href="css/nv.d3.css" rel="stylesheet" type="text/css">
<style>

    body {
        overflow-y:scroll;
    }

    /*text {*/
        /*font: 12px sans-serif;*/
    /*}*/

    .mypiechart {
        width: 500px;
        border: 2px;
    }
</style>
<body>

displayPiechart.jspdisplayPiechart.jsp<svg id="test1" class="mypiechart"></svg>



<script src="scripts/d3.v3.min.js"></script>
<script src="scripts/nvd3/nv.d3.js"></script>
<script src="scripts/nvd3/legend.js"></script>
<script src="scripts/nvd3/pie.js"></script>
<script src="scripts/nvd3/pieChart.js"></script>
<script src="scripts/nvd3/utils.js"></script>
<script>

    var testdata = [];
    var keys = ["Strong Like","Like","Neutral","Dislike","Strongly Dislike"];
    var values = ["${feedback[0].feedbackLevel.stronglyLikeCount}",
                 "${feedback[0].feedbackLevel.likeCount}",
                 "${feedback[0].feedbackLevel.neutralCount}",
                 "${feedback[0].feedbackLevel.dislikeCount}",
                 "${feedback[0].feedbackLevel.stronglyDislikeCount}"]
    for(var i = 0; i < keys.length; ++i) {
        var axis = keys[i];
        var value = values [i];
        testdata.push({key: axis, y: value});
    }


   <%--var testdata = [--%>
       <%--{--%>
           <%--key: "One",--%>
           <%--y: ${feedback[0].feedbackLevel.dislikeCount}/10--%>
       <%--},--%>
       <%--{--%>
           <%--key: "Two",--%>
           <%--y: ${feedback[0].feedbackLevel.likeCount}--%>
       <%--},--%>
       <%--{--%>
           <%--key: "Three",--%>
           <%--y: ${feedback[0].feedbackLevel.neutralCount}--%>
       <%--},--%>
       <%--{--%>
           <%--key: "Four",--%>
           <%--y: ${feedback[0].feedbackLevel.stronglyDislikeCount}--%>
       <%--},--%>
       <%--{--%>
           <%--key: "Five",--%>
           <%--y: ${feedback[0].feedbackLevel.stronglyLikeCount}--%>
       <%--}--%>
   <%--];--%>



    nv.addGraph(function() {
        var width = 500,
                height = 500;

        var chart = nv.models.pieChart()
                .x(function(d) { return d.key })
                .y(function(d) { return d.y })
                .color(d3.scale.category10().range())
                .width(width)
                .height(height);

        d3.select("#test1")
                .datum(testdata)
                .transition().duration(1200)
                .attr('width', width)
                .attr('height', height)
                .call(chart);

        chart.dispatch.on('stateChange', function(e) { nv.log('New State:', JSON.stringify(e)); });

        return chart;
    });

</script>

</body>
</html>
<%--
<html>
<head>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
        google.load("visualization", "1", {packages:["corechart"]});
        google.setOnLoadCallback(drawChart);
        function drawChart() {
            var data = google.visualization.arrayToDataTable([
                ['Task', 'Hours per Day'],
                &lt;%&ndash;['"${adPrefProfile.age}"',     "${adPrefProfile.ageWeight}"],&ndash;%&gt;
                ['Eat',      2],
                ['Commute',  2],
                ['Watch TV', 2],
                ['Sleep',    7]
            ]);

            var options = {
                title: 'My Daily Activities'
            };

            var chart = new google.visualization.PieChart(document.getElementById('piechart'));
            chart.draw(data, options);
        }
    </script>
</head>
<body>
<div id="piechart" style="width: 900px; height: 500px;"></div>
</body>
</html>--%>
