<%--
  Created by IntelliJ IDEA.
  User: macbookpro
  Date: 10/29/13
  Time: 12:31 AM
  To change this template use File | Settings | File Templates.
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--<!doctype html>--%>
<%--<html>--%>
<html lang="en">
<head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    <title>MAP&Radar</title>
<%--    <style type="text/css">
        html { height: 100% }
        body { height: 100%;width: 100%; margin: 0; padding: 0 }
        #map-canvas { height: 100% }
    </style>--%>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    </head>
<body>
<div class="row-fluid">

<div class="span2" id="map" style="width:50%; height: 100%;"></div>
<div class="span4 offset1" id="chart" style="width:30%; height: 150px;" ></div>
    <div>${demographicProfile.location.city}

<%--
<table>
    &lt;%&ndash;<c:forEach var="adPrefProfile" items="${profileList}"/>&ndash;%&gt;
    <tr>
        &lt;%&ndash;<td>${demographicProfile.race}</td>&ndash;%&gt;
        <td>${adPrefProfile.age}</td>
</tr>
    </table>
--%>
</div>






<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAyE4lcEOBDVTvBFqfyiJPGUD01v2OtCuU&sensor=false">
</script>
<script src="scripts/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
    var locations = [
        [ '<div id="content">'+
                '<div id="siteNotice">'+
                '</div>'+
                '<h3 id="firstHeading" class="firstHeading">Fremont</h4>'+
                '<div id="bodyContent">'+
                '<p>It is reserved for the content details </p>'+
                '</div>'+
                '</div>' , 37.55361666666667, -121.99926666666667, 2],
        ['<div id="content">'+
                '<div id="siteNotice">'+
                '</div>'+
                '<h3 id="firstHeading" class="firstHeading">San Jose</h4>'+
                '<div id="bodyContent">'+
                '<p>It is reserved for the content details </p>'+
                '</div>'+
                '</div>', 37.344833333333334, -121.89351666666667, 1]

    ];


    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 9,
        center: new google.maps.LatLng(37.43945, -121.9901),
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();

    var marker, i;

    for (i = 0; i < locations.length; i++) {
        marker = new google.maps.Marker({
            position:new google.maps.LatLng(locations[i][1], locations[i][2]),
            map: map
        });

        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                infowindow.setContent(locations [i][0]);
                infowindow.open(map, marker);
            }
        })(marker, i));
    }
});
</script>
<script src="scripts/d3.v3.min.js" type="text/javascript"></script>
<script src="scripts/radar-chart.js" type=""></script>
<%--
<script>
    var d = [], c = [];
    var adv_data = ${adPrefProfile}, rec_data = ${demographicProfile};
    var keys = ["age","education","ethnicity","gender","house","income","population","race","weather"];
    if (!adv_data) adv_data = {};
    if (!rec_data) rec_data = {};
    for(var i = 0; i < keys.length; ++i) {
        var key = keys[i];
        d.push({axis: key, value: adv_data[key] || 1});
        c.push({axis: key, value: rec_data[key] || 1});
    }
    console.log(d);

    RadarChart.draw("#chart", [d, c]);

</script>
--%>

<script>
    var d = [], c = [];
    var adv_data = ${adPrefProfile}; rec_data = ${demographic};
    var keys = ["age","education","ethnicity","gender","house","income","population","race","weather"];
    if (!adv_data) adv_data = {};
    if (!rec_data) rec_data = {};
    for(var i = 0; i < keys.length; ++i) {
        var key = keys[i];
        d.push({axis: key, value: adv_data[i] || 1});
        c.push({axis: key, value: rec_data[i] || 1});
    }
    console.log(d);

    RadarChart.draw("#chart", [d, c]);

</script>
<script src="scripts/jquery.backstretch.js"></script>
<script>$.backstretch("images/radar_background.jpg")</script>

</body>
</html>