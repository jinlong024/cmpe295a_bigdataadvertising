<%--
  Created by IntelliJ IDEA.
  User: macbookpro
  Date: 12/8/13
  Time: 2:37 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Yelp Location Review</title>
    <style type="text/css">
        body {
            padding-top: 60px;
            padding-bottom: 40px;
        }
    </style>

    <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
</head>

<body>
<div class="navbar  navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container">
            <button data-target=".nav-collapse" data-toggle="collapse" class="btn btn-navbar" type="button">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="#" class="brand" style="font-family: Flat-UI-Icons; font-size: 30px;color: #0e90d2">AD Big Data</a>

            <div class="nav-collapse collapse">
                <ul class="nav">
                    <li class="active">
                        <a href="adPlanning">Home</a>
                    </li>
                    <li>
                        <a href="adPlanning">AD Planning</a>
                    </li>
                    <li>
                        <a href="/BigDataAdvertising/advHomepage">Recommend</a>
                    </li>
                    <li>
                        <a href="adEvaluation">Evaluate</a>
                    </li>
                    <li>
                        <a href="/BigDataAdvertising/TwitterDataCollection">Data Collection</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div id="chart_div" class="span6 " style="width: 600px; height: 500px;">
        </div>
        <div id="3dpiechart" class="span6 " style="width: 500px; height: 600px;">
        </div>
    </div>

    <div class="row-fluid">
        <div class="span6 offset3">
            <a class="btn btn-success btn-large btn-block  " href="/BigDataAdvertising/adEvaluation"
               type="button">Back</a>
        </div>
    </div>
</div>
</div>
</body>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
    google.load("visualization", "1", {packages: ["corechart"]});
    google.setOnLoadCallback(drawChart);
    function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['Product Category', 'User Review', {role: 'annotation'}],
            ['${yelpcity[0].businessCategory}', ${yelpcity[0].userCount} , 'Feedbacklevel: ${yelpcity[0].feedbackLevel}'],

            ['${yelpcity[1].businessCategory}', ${yelpcity[1].userCount} , 'Feedbacklevel: ${yelpcity[1].feedbackLevel}'],

            ['${yelpcity[2].businessCategory}', ${yelpcity[2].userCount} , 'Feedbacklevel: ${yelpcity[2].feedbackLevel}'],

            ['${yelpcity[3].businessCategory}', ${yelpcity[3].userCount} , 'Feedbacklevel: ${yelpcity[3].feedbackLevel}'],

            ['${yelpcity[4].businessCategory}', ${yelpcity[4].userCount} , 'Feedbacklevel: ${yelpcity[4].feedbackLevel}']
        ]);

        var options = {
            title: 'Product User Count',
            hAxis: {title: 'Product Location : ${yelpcity[0].city}', titleTextStyle: {color: 'black' }}
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
        chart.draw(data, options);
    }
</script>

<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
    google.load("visualization", "1", {packages: ["corechart"]});
    google.setOnLoadCallback(drawChart);
    function drawChart() {

        var data = google.visualization.arrayToDataTable([
            ['Task', 'Hours per Day'],
            ['Electronics', ${yelpcity[0].userCount} ],
            ['Home', ${yelpcity[1].userCount} ],
            ['Beauty', ${yelpcity[2].userCount}],
            ['Food', ${yelpcity[3].userCount}],
            ['Clothing', ${yelpcity[4].userCount}]
        ]);


        var options = {
            title: 'City ${yelpcity[0].city} User FeedBack Count by Category',
            is3D: true

        };

        var chart = new google.visualization.PieChart(document.getElementById('3dpiechart'));
        chart.draw(data, options);
    }
</script>

<script src="http://code.jquery.com/jquery-1.9.1.js" type="text/javascript"></script>
<script src="//cdn.jsdelivr.net/jquery.backstretch/2.0.3/jquery.backstretch.min.js"></script>
<script>$.backstretch("images/light.jpeg")</script>


</html>