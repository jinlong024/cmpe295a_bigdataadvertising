<%--
  Created by IntelliJ IDEA.
  User: macbookpro
  Date: 9/23/13
  Time: 9:41 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">
<head>

    <meta charset="utf-8">
    <title>Homepage</title>


    <%--  <style type="text/css">
          .navbar-custom {
              background-color: #406ce5;
              color: #ffffff;
              border-radius: 0;
          }

          .navbar-custom .navbar-nav > li > a {
              color: #ffffff;
              padding-left: 20px;
              padding-right: 20px;
          }

          .navbar-custom .navbar-nav > .active > a, .navbar-nav > .active > a:hover, .navbar-nav > .active > a:focus {
              color: #ffffff;
              background-color: transparent;
          }

          .navbar-custom .navbar-nav > li > a:hover, .nav > li > a:focus {
              text-decoration: none;
              background-color: #0083fe;
          }

          .navbar-custom .navbar-brand {
              color: #eeeeee;
          }

          .navbar-custom .navbar-toggle {
              background-color: #eeeeee;
          }

      </style>--%>

    <style type="text/css">
        body {
            padding-top: 60px;
            padding-bottom: 40px;
        }

        .slide-text {
            text-align: justify;
            padding: 0;
            margin: 0 -10px;
        }

        .slide-text > span {
            display: inline-block;
            margin: auto;
            padding: 0;
            overflow: hidden;
            white-space: nowrap;
        }

        .slide-text > .fin {
            display: inline-block;
            width: 100%;
        }

        .ui-slide {
            width: 100px;
            height: 12px;
        }

        #advupdate {
            width: 80%;
            height: 85%;
            margin-left: -40%;
            margin-top: -5%;
        }

        #advupdate-body {
            max-height: 85%;
        }

        < style > .slide-text {
            text-align: justify;
            padding: 0;
            margin: 0 -10px;
        }

        .slide-text > span {
            display: inline-block;
            margin: auto;
            padding: 0;
            overflow: hidden;
            white-space: nowrap;
        }

        .slide-text > .fin {
            display: inline-block;
            width: 100%;
        }

        .slider-range-max {
            width: 100%;
            height: 12px;
        }</style>


    <%--<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css">--%>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
    <link href="css/acc-wizard.min.css" rel="stylesheet">
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css"/>
    <link rel="stylesheet" href="css/bootstrap-switch.css" rel="stylesheet">
    <link rel="stylesheet" href="css/flat-ui-fonts.css" rel="stylesheet">

    <%--<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" />--%>

    <%--
        <link rel="stylesheet" href="http://getbootstrap.com/2.3.2/assets/js/google-code-prettify/prettify.css"/>

        <link rel="stylesheet" href="http://getbootstrap.com/2.3.2/assets/css/docs.css"/>

        <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css">

        <link rel="stylesheet"
              href="https://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/css/bootstrap-combined.min.css">
        <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css"/>
    --%>
    <%--
        <style>
            .navbar-inner
            {
                background-color: floralwhite;
                /* remove the gradient */
                background-image: none;
                /* set font color to white */
                color: #ffffff;
                border-radius: 0;

            }

                /* menu items */

                /* set the background of the menu items to pink and default color to white */

            .navbar .nav > li > a {
                /*background-color: pink;*/
                color: #ffffff;
                padding-left: 20px;
                padding-right: 20px;


            }


                /* set hover and focus to lightblue */
            .navbar .nav > li > a:focus,
            .navbar .nav > li > a:hover {
                background-color: #0083fe;
                color: #f0f0f0;
                text-decoration: none;

            }
                /* set active item to darkgreen */
            .navbar .nav > .active > a,
            .navbar .nav > .active > a:hover,
            .navbar .nav > .active > a:focus {

                background-color: transparent;
                color: #ffffff;
            }

                /* set font color and background of the project name (brand) */

            .navbar .brand
            {
                color: #ffffff;
            }
            .navbar .btn btn-navbar {
                background-color: #eeeeee;
            }

        </style>
    --%>

</head>

<body>
<%--<div class="navbar navbar-custom navbar-fixed-top">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            &lt;%&ndash;<span class="sr-only">Toggle navigation</span>&ndash;%&gt;
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand">AD Radar</a>
    </div>
    <div class="collapse navbar-collapse">
        <ul class="nav navbar-nav">
            <li class="active">
                <a href="adPlanning">Home</a>
            </li>
            <li>
                <a href="adPlanning">AD Planning</a>
            </li>
            <li>
                <a href="/BigDataAdvertising/advHomepage">Recommend</a>
            </li>
            <li>
                <a href="adEvaluation">Evaluate</a>
            </li>
            <li>
                <a href="/BigDataAdvertising/adEvaluation">Data Collection</a>
            </li>
        </ul>
    </div>
</div>--%>
<div class="navbar  navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container">
            <button data-target=".nav-collapse" data-toggle="collapse" class="btn btn-navbar" type="button">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="#" class="brand" style="font-family: Flat-UI-Icons; font-size: 30px;color: #0e90d2">AD Big Data</a>

            <div class="nav-collapse collapse">
                <ul class="nav">
                    <li class="active">
                        <a href="adPlanning">Home</a>
                    </li>
                    <li>
                        <a href="adPlanning">AD Planning</a>
                    </li>
                    <li>
                        <a href="/BigDataAdvertising/advHomepage">Recommend</a>
                    </li>
                    <li>
                        <a href="adEvaluation">Evaluate</a>
                    </li>
                    <li>
                        <a href="/BigDataAdvertising/TwitterDataCollection">Data Collection</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <fieldset>
        <div>
            <h2>
                <center>AD Information List</center>
            </h2>
        </div>
    </fieldset>
</div>

<div class="container">
    <table class="table table-bordered  table-condensed" id="tbl">
        <tr class="success">
            <td>#</td>
            <td>Product Name</td>
            <td>Model Name</td>
            <td>Manufacturer Name</td>
            <td>Product Category</td>
            <td>Age</td>
            <td>Education</td>
            <td>Ethnicity</td>
            <td>Gender</td>
            <td>House</td>
            <td>Income</td>
            <td>Population</td>
            <td>Race</td>
            <td>Weather</td>
            <td>
                <center>Operation</center>
            </td>
        </tr>
        <c:forEach var="adPrefProfile" items="${profileList}" varStatus="status">
            <tr>
                <td>${status.count}</td>
                <td>${adPrefProfile.productName}</td>
                <td>${adPrefProfile.productModel}</td>
                <td>${adPrefProfile.productManufacturer}</td>
                <td>${adPrefProfile.productCategory}</td>
                <td>${adPrefProfile.age}${adPrefProfile.ageWeight}</td>
                <td>${adPrefProfile.education}&nbsp;/${adPrefProfile.educationWeight}</td>
                <td>${adPrefProfile.ethnicity}</td>
                <td>${adPrefProfile.sex}</td>
                <td>${adPrefProfile.house}</td>
                <td>${adPrefProfile.income}</td>
                <td>${adPrefProfile.population}</td>
                <td>${adPrefProfile.race}</td>
                <td>${adPrefProfile.weather}</td>

                <td>
                    <div class="row">
                        <div class="span2">
                            <div class="row">
                                <div class="span1">
                                    <a type="button" class="btn btn-warning btn-xs"
                                       href="advHomepage/delete?id=${adPrefProfile.id}">Delete </a>
                                </div>
                                <div class="span1">
                                    <a data-toggle="modal" type="button" class="btn btn-info btn-xs"
                                       href="#advupdate">update</a>
                                </div>
                                <div class="row-fluid">
                                    <div class="span6 offset4">
                                        <a type="button" class="btn btn-primary"
                                           href="/BigDataAdvertising/map?id=${adPrefProfile.id}"> Recommend</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </td>


            </tr>
        </c:forEach>
    </table>
</div>

<%--update modal--%>

<div id="advupdate" class="modal hide fade" style="display: none; ">
<div class="modal-header">
    <button type="button" class="close" aria-hidden="true" data-dismiss="modal">&times;</button>
    <h3>Edit Your Ad Information Here</h3>
</div>
<div class="modal-body" id="advupdate-body">
<form action="advHomepage/update" method="post">
<div>
    <!-- Text input-->
    <label class="control-label" for="productName" style="font-size: 23px">Product
        Name:</label>
</div>
<div class="controls">
    <c:forEach var="adPrefProfile" items="${profileList}">

        <input type="text" id="productName" name="productName"
               value="${adPrefProfile.productName}" class="input-xlarge"
               style="width: 270px; height: 28px; color: #555555;  font-size: 18px;">
    </c:forEach>
    <p class="help-block"></p>

</div>

<label class="control-label" for="productModel" style="font-size: 23px">Model Name:</label>

<div class="controls">
    <input type="text" id="productModel" name="productModel"
           placeholder="" class="input-xlarge"
           style="width: 270px; height: 28px; color: #555555;  font-size: 18px;">

    <p class="help-block"></p>
</div>

<label class="control-label" for="productManufacturer" style="font-size: 23px">Manufacturer:</label>

<div class="controls">
    <input type="text" id="productManufacturer" name="productManufacturer"
           placeholder="" class="input-xlarge"
           style="width: 270px; height: 28px; color: #555555;  font-size: 18px;">
</div>


<label class="control-label" for="productCategory" style="font-size: 23px">Product Category:</label>


<div class="controls">
    <select id="productCategory" name="productCategory">
        <option value="">Please Choose...</option>
        <option value="1">Beauty</option>
        <option value="2">Clothing</option>
        <option value="3">Electronics</option>
        <option value="4">Food</option>
        <option value="5">others</option>
    </select>

    <p class="help-block"></p>
</div>

<%--INPUT AD Preference --%>

<table class="table table-condensed ">
<tbody>
<!--Age number 1-->

<tr>

    <td class="span1 offset 1" style="font-size:20px; text-align:center">
        Age
    </td>

    <td class="span1" style="font-size:20px">
        <select id="age" name="age" class="input">
            <option value="">Please Choose...</option>
            <option value="1">Infant (Under 2)</option>
            <option value="2">Child (2-11)</option>
            <option value="3">Youth (12-17)</option>
            <option value="4">Adult (18-64)</option>
            <option value="5">Senior (65 or over)</option>
        </select>
    </td>

    <td class="span3" style="font-size:20px"> weight: &nbsp;
        <div class="make-switch" id="chk1">
            <input type="checkbox" id="c1">
        </div>
    </td>

    <td class="span1">
        <input type="text" id="s1" name="ageWeight" readonly="readonly"
               style="width: 60px; height: 30px; color: #f6931f;     font-weight: bold; font-size: 20px;">
    </td>

    <td class="span3 offset 3">
        <div class="slider-range-max" id="s1Slider">
        </div>
        <div class="slide-text">
            <span> Min 0.1</span>
            <span>Max 1.0&nbsp;&nbsp;</span>

            <span class="fin"></span>
        </div>
    </td>

</tr>

<%--education #2--%>

<tr>

    <td class="span1" style="font-size:20px; text-align: center">Education
    </td>

    <td class="span1" style="font-size:20px">
        <select id="education" name="education" class="input">
            <option value="" selected="selected">Please Choose...</option>
            <option value="1">Less than high school</option>
            <option value="2">High school graduate</option>
            <option value="3">Some college</option>
            <option value="4">Bachelor's degree or higher</option>
        </select>
    </td>

    <td class="span3" style="font-size:20px"> weight: &nbsp;
        <div class="make-switch" id="chk2">
            <input type="checkbox" id="c2">
        </div>
    </td>

    <td class="span1">
        <input type="text" id="s2" name="educationWeight" readonly="readonly"
               style="width: 60px; height: 30px; color: #f6931f;     font-weight: bold; font-size: 20px;">
    </td>

    <td class="span3 offset3">
        <div class="slider-range-max" id="s2Slider">
        </div>
        <div class="slide-text">
            <span>Min 0.1</span>
            <span>Max 1.0&nbsp;&nbsp;</span>
            <span class="fin"></span>
        </div>
    </td>

</tr>


<%--gender #3--%>

<tr>
    <td class="span1 offset1" style="font-size:20px; text-align: center">
        Gender
    </td>

    <td class="span1" style="font-size:20px">
        <select id="sex" name="sex" class="input">
            <option value="" selected="selected">Please Choose...</option>
            <option value="1">Male</option>
            <option value="2">Female</option>
        </select>
    </td>

    <td class="span3" style="font-size:20px"> weight: &nbsp;
        <div class="make-switch" id="chk3">
            <input type="checkbox" id="c3">
        </div>
    </td>

    <td class="span1">
        <input type="text" id="s3" name="sexWeight" readonly="readonly"
               style="width: 60px; height: 30px; color: #f6931f;     font-weight: bold; font-size: 20px;">
    </td>

    <td class="span3 offset3">
        <div class="slider-range-max" id="s3Slider">
        </div>
        <div class="slide-text">
            <span> Min 0.1</span>
            <span>Max 1.0&nbsp;&nbsp;</span>
            <span class="fin"></span>
        </div>
    </td>
</tr>
<!--Income number4 -->
<tr>
    <td class="span1" style="font-size:20px; text-align: center">
        Income
    </td>

    <td class="span1" style="font-size:20px">
        <select id="income" name="income" class="input">
            <option value="" selected="selected">Please Choose...</option>
            <option value="1">Less than $10,000</option>
            <option value="2">$10,000 to $49,999</option>
            <option value="3">$50,000 to $99,999</option>
            <option value="4">$100,000 to $199,999</option>
            <option value="5">$200,000 or more</option>
        </select>
    </td>

    <td class="span3" style="font-size:20px"> weight: &nbsp;
        <div class="make-switch" id="chk4">
            <input type="checkbox" id="c4">
        </div>
    </td>

    <td class="span1" style="font-size:20px">
        <input type="text" id="s4" name="incomeWeight" readonly="readonly"
               style="width: 60px; height: 30px; color: #f6931f; font-weight: bold; font-size: 20px;">
    </td>

    <td class="span3 offset3">
        <div class="slider-range-max" id="s4Slider">
        </div>
        <div class="slide-text">
            <span>Min 0.1</span>
            <span>Max 1.0&nbsp;&nbsp;</span>
            <span class="fin"></span>
        </div>
    </td>
</tr>
<!--weather #5  -->
<tr>
    <td class="span1" style="font-size:20px; text-align: center">
        Weather
    </td>
    <td class="span1" style="font-size:20px">
        <select id="weather" name="weather" class="input">
            <option value="" selected="selected">Please Choose...</option>
            <option value="1">Coastal Weather</option>
            <option value="2">Hot Weather</option>
            <option value="3">Cold Weather</option>
            <option value="4">Mountain Weather</option>
        </select>
    </td>

    <td class="span3" style="font-size:20px"> weight: &nbsp;
        <div class="make-switch" id="chk5">
            <input type="checkbox" id="c5">
        </div>
    </td>

    <td class="span1" style="font-size:20px">
        <input type="text" id="s5" name="weatherWeight" readonly="readonly"
               style="width:60px; height: 30px; color: #f6931f; font-weight: bold; font-size: 20px;">
    </td>

    <td class="span2">
        <div class="slider-range-max" id="s5Slider">
        </div>
        <div class="slide-text">
            <span>Min 0.1</span>
            <span>Max 1.0&nbsp;&nbsp;</span>
            <span class="fin"></span>
        </div>
    </td>
</tr>
<!--Temperature #6  -->
<tr>
    <td class="span1" style="font-size:20px; text-align: center">
        Temperature
    </td>
    <td class="span1" style="font-size:20px">
        <select id="temperature" name="temperature" class="input-min">
            <option value="" selected="selected">Please Choose...</option>
            <option value="3">45F~67F</option>
            <option value="2">60F~90F</option>
            <option value="1">50F~72F</option>
            <option value="0">30F~50F</option>
        </select>
    </td>

    <td class="span3" style="font-size:20px"> weight: &nbsp;
        <div class="make-switch" id="chk6">
            <input type="checkbox" id="c6">
        </div>
    </td>

    <td class="span1" style="font-size:20px">
        <input type="text" id="s6" name="temperatureWeight" readonly="readonly"
               style="width: 60px; height: 30px; color: #f6931f; font-weight: bold; font-size: 20px;">
    </td>

    <td class="span3 offset3">
        <div class="slider-range-max" id="s6Slider">
        </div>
        <div class="slide-text">
            <span>Min 0.1</span>
            <span>Max 1.0&nbsp;&nbsp;</span>
            <span class="fin"></span>
        </div>
    </td>
</tr>
<!--Ethnicity #7  -->
<tr>
    <td class="span1" style="font-size:20px; text-align: center">
        Ethnicity
    </td>

    <td class="span1" style="font-size:20px">
        <select id="ethnicity" name="ethnicity" class="input">
            <option value="" selected="selected">Please Choose...</option>
            <option value="1">Hispanic</option>
            <option value="2">Non-hispanic</option>
        </select>
    </td>

    <td class="span3" style="font-size:20px"> weight: &nbsp;
        <div class="make-switch" id="chk7">
            <input type="checkbox" id="c7">
        </div>
    </td>

    <td class="span1" style="font-size:20px">
        <input type="text" id="s7" name="ethnicityWeight" readonly="readonly"
               style="width: 60px; height: 30px; color: #f6931f; font-weight: bold; font-size: 20px;"
               disabled="disabled"/>
    </td>

    <td class="span3 offset3">
        <div class="slider-range-max" id="s7Slider">
        </div>
        <div class="slide-text">
            <span>Min 0.1</span>
            <span>Max 1.0&nbsp;&nbsp;</span>
            <span class="fin"></span>
        </div>
    </td>
</tr>

<!--Race #8  -->
<tr>
    <td class="span1" style="font-size:20px; text-align: center">
        Race
    </td>

    <td class="span1" style="font-size:20px">
        <select id="race" name="race" class="input">
            <option value="" selected="selected">Please Choose...</option>
            <option value="1">Native Hawaiian and Other Pacific Islander</option>
            <option value="2">Black or African American</option>
            <option value="3">American Indian and Alaska Native</option>
            <option value="5">Asian</option>
            <option value="6">White</option>
            <option value="4">Some Other Race</option>
        </select>
    </td>

    <td class="span3" style="font-size:20px"> weight: &nbsp;
        <div class="make-switch" id="chk8">
            <input type="checkbox" id="c8">
        </div>
    </td>

    <td class="span1" style="font-size:20px">
        <input type="text" id="s8" name="raceWeight" readonly="readonly"
               style="width: 60px; height: 30px; color: #f6931f; font-weight: bold; font-size: 20px;">
    </td>

    <td class="span3 offset3">
        <div class="slider-range-max" id="s8Slider">
        </div>
        <div class="slide-text">
            <span>Min 0.1</span>
            <span>Max 1.0&nbsp;&nbsp;</span>
            <span class="fin"></span>
        </div>
    </td>
</tr>
<!--Population #9  -->
<tr>
    <td class="span1" style="font-size:20px; text-align: center">
        Population
    </td>

    <td class="span1" style="font-size:20px">
        <select id="population" name="population" class="input">
            <option value="" selected="selected">Not decided yet</option>
            <option value="1">Less than 10,000</option>
            <option value="2">10,000 to 99,999</option>
            <option value="3">109,000 to 499,999</option>
            <option value="4">500,000 to 999,999</option>
            <option value="5">1,000,000 or more</option>
        </select>
    </td>

    <td class="span3" style="font-size:20px"> weight: &nbsp;
        <div class="make-switch" id="chk9">
            <input type="checkbox" id="c9">
        </div>
    </td>

    <td class="span1" style="font-size:20px">
        <input type="text" id="s9" name="populationWeight" readonly="readonly"
               style="width: 60px; height: 30px; color: #f6931f; font-weight: bold; font-size: 20px;">
    </td>

    <td class="span3 offset3">
        <div class="slider-range-max" id="s9Slider">
        </div>
        <div class="slide-text">
            <span>Min 0.1</span>
            <span>Max 1.0&nbsp;&nbsp;</span>
            <span class="fin"></span>
        </div>
    </td>
</tr>
<!--House #10  -->
<tr>
    <td class="span1" style="font-size:20px; text-align: center">
        House
    </td>

    <td class="span1" style="font-size:20px">
        <select id="house" name="house" class="input">
            <option value="" selected="selected">Please Choose....</option>
            <option value="1">Less than 10,000</option>
            <option value="2">10,000 to 99,999</option>
            <option value="3">109,000 to 499,999</option>
            <option value="4">500,000 to 999,999</option>
            <option value="5">1,000,000 or more</option>
        </select>
    </td>

    <td class="span3" style="font-size:20px"> weight: &nbsp;
        <div class="make-switch" id="chk10">
            <input type="checkbox" id="c10">
        </div>
    </td>

    <td class="span1" style="font-size:20px">
        <input type="text" id="s10" name="houseWeight" readonly="readonly"
               style="width: 60px; height: 30px; color: #f6931f; font-weight: bold; font-size: 20px;">
    </td>

    <td class="span3 offset3">
        <div class="slider-range-max" id="s10Slider">
        </div>
        <div class="slide-text">
            <span>Min 0.1</span>
            <span>Max 1.0&nbsp;&nbsp;</span>
            <span class="fin"></span>
        </div>
    </td>
</tr>


</tbody>

</table>


<div class="modal-footer">
    <button class="btn btn-primary" type="submit">Submit</button>
    <a href="#" class="btn" data-dismiss="modal">Close</a>
</div>
</form>
</div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="scripts/jquery.min.js"></script>
<script type="text/javascript" src="scripts/bootstrap.min.js"></script>
<script src="http://code.jquery.com/jquery-1.9.1.js" type="text/javascript"></script>
<script src="scripts/jquery-ui.js" type="text/javascript"></script>
<script src="scripts/hidden.js" type="text/javascript"></script>
<script src="scripts/bootstrap-switch.js" type="text/javascript"></script>
<script src="http://getbootstrap.com/2.3.2/assets/js/google-code-prettify/prettify.js" type="text/javascript"></script>
<script>

    $(".slider-range-max").each(function (idx, elm) {
        var name = elm.id.replace('Slider', '');
        $('#' + elm.id).slider({
            range: "max",
            min: 0.1,
            max: 1.0,
            value: 0.1,
            step: 0.1,
            disabled: true,
            slide: function (event, ui) {
                $('#' + name).val(ui.value);
            }
        });
    });
    ////       /* $(".amount").val($(".slider-range-max").slider("value"));
    //        var div = event.target.parentNode;
    //        $(div, '.amount').val($(event.target).slider("value"));
    //    });*/


</script>


<script src="scripts/jquery.backstretch.js"></script>
<script>$.backstretch("images/light.jpeg")</script>

</body>


</html>