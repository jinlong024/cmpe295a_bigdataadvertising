package edu.sjsu.bigdata.advertising.dao.impl.datacollection;

import edu.sjsu.bigdata.advertising.dao.interfaces.datacollection.FeedbackDao;
import edu.sjsu.bigdata.advertising.model.feedbackAggregation.Feedback;
import edu.sjsu.bigdata.advertising.model.feedbackAggregation.HourlyFeedbackAggregation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * This is the advertisement preference profile service implementation. User:
 * lei zhang Date: 13-8-13
 */
@Repository
public class FeedbackDaoImpl implements FeedbackDao {

	@Autowired
	private MongoTemplate mongoTemplate;

	public static final String COLLECTION_NAME = "adFeedback";

	/**
	 * This method is used to insert or update Advertisement Preference Profile
	 * and return its ID.
	 * 
	 * @param entity
	 *            : This is the advertisement preference profile to be saved.
	 * @return Return null if the operation is failed; Return the profile ID if
	 *         the operation is successful.
	 *         <p/>
	 *         Client Example: Save: POST /recommendation/adprefprofile with the
	 *         advertisement preference profile Form/JSON/XML Update: PUT
	 *         /recommendation/adprefprofile with the advertisement preference
	 *         profile Form/JSON/XML
	 */

	public Feedback saveFeedback(Feedback entity) {
		if (!mongoTemplate.collectionExists(Feedback.class)) {
			mongoTemplate.createCollection(Feedback.class);
		}

		mongoTemplate.insert(entity, COLLECTION_NAME);
		return mongoTemplate.findById(entity.getId(), Feedback.class);
	}
	
	public Feedback updateFeedback(Feedback entity) {
		mongoTemplate.insert(entity, COLLECTION_NAME);	
		return mongoTemplate.findById(entity.getId(), Feedback.class);
	}

	/**
	 * This method is used to delete Advertisement Preference Profile by ID and
	 * return the result if the it has been deleted.
	 * 
	 * @param id
	 *            : This is the advertisement preference profile ID.
	 * @return Return false if the operation is failed; Return true if the
	 *         operation is successful.
	 *         <p/>
	 *         Client Example: Delete: DELETE /recommendation/adprefprofile/{id}
	 */
	public boolean deleteFeedback(String id) {

        Object object = mongoTemplate.findById(id, Feedback.class);
		mongoTemplate.remove(object);

        return true;

	}

	/**
	 * This method is used to get Advertisement Preference Profile by ID.
	 * 
	 * @param id
	 *            : This is the advertisement preference profile ID.
	 * @return Return NULL if the operation is failed; Return the advertisement
	 *         preference profile if the operation is successful.
	 *         <p/>
	 *         Client Example: Get: GET /recommendation/adprefprofile/{id}
	 */
	public Feedback findFeedbackById(String id) {
		return mongoTemplate.findById(id, Feedback.class);
	}

	/**
	 * This method is used to get list of existing Advertisement Preference
	 * Profiles.
	 * 
	 * @return Return NULL if the operation is failed; Return the list of
	 *         existing advertisement preference profiles if the operation is
	 *         successful.
	 *         <p/>
	 *         Client Example: Get All: GET /recommendation/adprefprofile
	 */
	public List<Feedback> findAllFeedback() {
		return mongoTemplate.findAll(Feedback.class, COLLECTION_NAME);
	}
	
    public List<Feedback> findFeedbackByLevel(String feedbackLevel){
    	
		 Query query = new Query(Criteria.where("feedback_level").is(feedbackLevel));
		 
		 return mongoTemplate.find(query, Feedback.class, COLLECTION_NAME);
    	
    }
}
