package edu.sjsu.bigdata.advertising.dao.impl.recommendation;

import edu.sjsu.bigdata.advertising.dao.interfaces.recommendation.AdPrefProfileDao;
import edu.sjsu.bigdata.advertising.model.recommendation.AdPrefProfile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * This is the advertisement preference profile service implementation. User:
 * lei zhang Date: 13-8-13
 */
@Repository
public class AdPrefProfileDaoImpl implements AdPrefProfileDao {

	@Autowired
	private MongoTemplate mongoTemplate;

    public MongoTemplate getMongoTemplate() {
        return mongoTemplate;
    }

    public void setMongoTemplate(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

	public static final String COLLECTION_NAME = "adPrefProfile";

	/**
	 * This method is used to insert or update Advertisement Preference Profile
	 * and return its ID.
	 * 
	 * @param entity
	 *            : This is the advertisement preference profile to be saved.
	 * @return Return null if the operation is failed; Return the profile ID if
	 *         the operation is successful.
	 *         <p/>
	 *         Client Example: Save: POST /recommendation/adprefprofile with the
	 *         advertisement preference profile Form/JSON/XML Update: PUT
	 *         /recommendation/adprefprofile with the advertisement preference
	 *         profile Form/JSON/XML
	 */

	public AdPrefProfile saveProfile(AdPrefProfile entity) {
		if (!mongoTemplate.collectionExists(AdPrefProfile.class)) {
			mongoTemplate.createCollection(AdPrefProfile.class);
		}
		
		mongoTemplate.insert(entity, COLLECTION_NAME);
		return mongoTemplate.findById(entity.getId(), AdPrefProfile.class);
	}
	
	public AdPrefProfile updateProfile(AdPrefProfile entity) {
		mongoTemplate.insert(entity, COLLECTION_NAME);	
		return mongoTemplate.findById(entity.getId(), AdPrefProfile.class);
	}

	/**
	 * This method is used to delete Advertisement Preference Profile by ID and
	 * return the result if the it has been deleted.
	 * 
	 * @param id
	 *            : This is the advertisement preference profile ID.
	 * @return Return false if the operation is failed; Return true if the
	 *         operation is successful.
	 *         <p/>
	 *         Client Example: Delete: DELETE /recommendation/adprefprofile/{id}
	 */
	public boolean deleteProfile(String id) {

        Object object =      mongoTemplate.findById(id,AdPrefProfile.class );
        mongoTemplate.remove(object);

        return true;

   
	}

	/**
	 * This method is used to get Advertisement Preference Profile by ID.
	 * 
	 * @param id
	 *            : This is the advertisement preference profile ID.
	 * @return Return NULL if the operation is failed; Return the advertisement
	 *         preference profile if the operation is successful.
	 *         <p/>
	 *         Client Example: Get: GET /recommendation/adprefprofile/{id}
	 */
	public AdPrefProfile findProfileById(String id) {
		return mongoTemplate.findById(id, AdPrefProfile.class);
	}

	/**
	 * This method is used to get list of existing Advertisement Preference
	 * Profiles.
	 * 
	 * @return Return NULL if the operation is failed; Return the list of
	 *         existing advertisement preference profiles if the operation is
	 *         successful.
	 *         <p/>
	 *         Client Example: Get All: GET /recommendation/adprefprofile
	 */
	public List<AdPrefProfile> findAllProfile() {
		return mongoTemplate.findAll(AdPrefProfile.class, COLLECTION_NAME);
	}
}
