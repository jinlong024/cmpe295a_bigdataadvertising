/**
 * 
 */
package edu.sjsu.bigdata.advertising.dao.impl.security;

import edu.sjsu.bigdata.advertising.dao.interfaces.security.IUserRoleDao;
import edu.sjsu.bigdata.advertising.exception.dao.AdvertisngAccessException;
import edu.sjsu.bigdata.advertising.model.security.HttpUser;
import edu.sjsu.bigdata.advertising.model.security.Role;
import edu.sjsu.bigdata.advertising.model.security.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author yunxue
 *
 */
@Repository
public class UserRoleDaoImpl implements IUserRoleDao {

	@Autowired
	private MongoTemplate mongoTemplate;
	
	public MongoTemplate getMongoTemplate() {
		return mongoTemplate;
	}

	public void setMongoTemplate(MongoTemplate mongoTemplate) {
		this.mongoTemplate = mongoTemplate;
	}
	

	@Override
	public User getUserById(String id) throws AdvertisngAccessException {
		// TODO Auto-generated method stub
		User user = mongoTemplate.findById(id, User.class);
		return user;
	}

	@Override
	public void saveOrUpdateUser(User user) throws AdvertisngAccessException {
		try {
			if (!mongoTemplate.collectionExists(User.class)) {
				mongoTemplate.createCollection(User.class);
			}	
			
			Query query = new Query();
			query.addCriteria(Criteria.where("email").is(user.getEmail()));
	 
			Update update = new Update();
			update.set("email", user.getEmail());
			update.set("password", user.getPassword());
			update.set("firstName", user.getFirstName());
			update.set("lastName", user.getLastName());
			//update.set("roleId", user.getRole().getRoleId());
            update.set("role", user.getRole());
			update.set("contact", user.getContact());
			mongoTemplate.upsert(query, update, User.class);
		}
		catch (DataAccessException e) {
			throw new AdvertisngAccessException(e);
		}
	}
	

	@Override
	public List<User> getAllUser() throws AdvertisngAccessException {
		List<User> users = null;
		try {
			users = (List<User>) mongoTemplate.findAll(User.class);
		}
		catch (DataAccessException e) {
			throw new AdvertisngAccessException(e);
		}
		return users;
	}
	

	@Override
	public void deleteUser(String email) throws AdvertisngAccessException {
		try {
			Query searchQuery = new Query(Criteria.where("email").is(email));
			mongoTemplate.remove(searchQuery, User.class);
		}
		catch (DataAccessException e) {
			throw new AdvertisngAccessException(e);
		}
	}

	@Override
	public Role getRoleById(String id) throws AdvertisngAccessException {
		// TODO Auto-generated method stub
		Role role = null;
		try {	
			role = mongoTemplate.findById(id, Role.class);
		}
		catch (DataAccessException e) {
			throw new AdvertisngAccessException(e);
		}
		
		return role;
	}



	@Override
	public List<Role> getAllRole() throws AdvertisngAccessException {
		List<Role> roles = null;
		try {
			roles = (List<Role>) mongoTemplate.findAll(Role.class);
		}
		catch (DataAccessException e) {
			throw new AdvertisngAccessException(e);
		}
		return roles;
	}
	

	@Override
	public void deleteRole(String roleName) throws AdvertisngAccessException {
		try {
			Query searchQuery = new Query(Criteria.where("role").is(roleName));
			mongoTemplate.remove(searchQuery, Role.class);
			//mongoTemplate.remove(id);
		}
		catch (DataAccessException e) {
			throw new AdvertisngAccessException(e);
		}
		
	}


	@Override
	public void saveOrUpdateRole(Role role) throws AdvertisngAccessException {
		try {
			if (!mongoTemplate.collectionExists(Role.class)) {
				mongoTemplate.createCollection(Role.class);
			}	
			
			Query query = new Query();
			query.addCriteria(Criteria.where("role").is(role.getRole()));
	 
			Update update = new Update();
			update.set("role", role.getRole());
			update.set("description", role.getDescription());
	 
			mongoTemplate.upsert(query, update, Role.class);
		}
		catch (DataAccessException e) {
			throw new AdvertisngAccessException(e);
		}
		
	}
	

	@Override
	public User getUserByEmail(String email) throws AdvertisngAccessException {
		User user = null;
		try {
			Query searchUserQuery = new Query(Criteria.where("email").is(email));
			user = mongoTemplate.findOne(searchUserQuery, User.class);
		}
		catch (DataAccessException e) {
			throw new AdvertisngAccessException(e);
		}
		return user;
	}

	@Override
	public Role getRoleByRole(String roleId) throws AdvertisngAccessException {
		Role role = null;
		try {
			Query searchQuery = new Query(Criteria.where("role").is(roleId));
			role = mongoTemplate.findOne(searchQuery, Role.class);
		}
		catch (DataAccessException e) {
			throw new AdvertisngAccessException(e);
		}
		return role;
	}
	

	@Override
	public HttpUser authenticate(String email, String password) throws AdvertisngAccessException {	
		HttpUser httpUser = new HttpUser();
		User user = getUserByEmail(email);
		httpUser.setUser(user);
		
		if (user == null) {
			httpUser.setAuthResult(-1);
		}
		else {	
			if (!user.getPassword().equalsIgnoreCase(password)) {
				httpUser.setAuthResult(0);
			}
			else {
				httpUser.setAuthResult(1);
			}
		}
		return httpUser;
	}

	
	@Override
	public HttpUser register(User user) throws AdvertisngAccessException {
		HttpUser httpUser = new HttpUser();
		saveOrUpdateUser(user);
		httpUser.setUser(user);
		httpUser.setAuthResult(1);
		return httpUser;
	}

	
	/*@Override
	public void saveRole(Role role) throws AdvertisngAccessException {
		try {
			if (!mongoTemplate.collectionExists(Role.class)) {
				mongoTemplate.createCollection(Role.class);
			}		
			//role.setRoleId(UUID.randomUUID().toString());
			mongoTemplate.save(role);
		}
		catch (DataAccessException e) {
			throw new AdvertisngAccessException(e);
		}
		
	}*/
	
	
	/*@Override
	public void updateRole(Role role) throws AdvertisngAccessException {
		try {	
			Query searchQuery = new Query(Criteria.where("role").is(role.getRole()));
			mongoTemplate.updateFirst(searchQuery, Update.update("description", role.getDescription()), Role.class);
		}
		catch (DataAccessException e) {
			throw new AdvertisngAccessException(e);
		}
		
	}*/

}
