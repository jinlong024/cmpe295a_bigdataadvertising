package edu.sjsu.bigdata.advertising.dao.interfaces.recommendation;

import edu.sjsu.bigdata.advertising.model.recommendation.AdPrefProfile;

import java.util.List;

/**
 * This is the advertisement preference profile data access object interface.
 * User: Lei Zhang
 * Date: August 13, 2013
 */
public interface AdPrefProfileDao {

    /**
     * This method is used to insert Advertisement Preference Profile and return the new object.
     *
     * @param entity: This is the advertisement preference profile to be saved.
     * @return Return null if the operation is failed;
     *         Return the newly created profile if the operation is successful.
     *
    */
    public AdPrefProfile saveProfile(AdPrefProfile entity);
    
    /**
     * This method is used to update Advertisement Preference Profile and return the updated object.
     *
     * @param entity: This is the advertisement preference profile to be updated.
     * @return Return null if the operation is failed;
     *         Return the updated profile object if the operation is successful.
     *
    */
    public AdPrefProfile updateProfile(AdPrefProfile entity);

    /**
     * This method is used to delete Advertisement Preference Profile by ID and return the result
     * if the it has been deleted.
     *
     * @param id: This is the advertisement preference profile ID.
     * @return Return false if the operation is failed;
     *         Return true if the operation is successful.
     *
     * Client Example:
     * Delete: DELETE /recommendation/adprefprofile/{id}
     */
    public boolean deleteProfile(String id);

    /**
     * This method is used to get Advertisement Preference Profile by ID.
     *
     * @param id: This is the advertisement preference profile ID.
     * @return Return NULL if the operation is failed;
     *         Return the advertisement preference profile if the operation is successful.
     *
     * Client Example:
     * Get: GET /recommendation/adprefprofile/{id}
     */
    public AdPrefProfile findProfileById(String id);

    /**
     * This method is used to get list of existing Advertisement Preference Profiles.
     *
     * @return Return NULL if the operation is failed;
     *         Return the list of existing advertisement preference profiles if the operation is successful.
     *
     * Client Example:
     * Get All: GET /recommendation/adprefprofile
     */
    public List<AdPrefProfile> findAllProfile();
}
