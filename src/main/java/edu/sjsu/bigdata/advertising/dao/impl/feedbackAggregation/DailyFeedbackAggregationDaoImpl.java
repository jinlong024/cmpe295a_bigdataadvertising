package edu.sjsu.bigdata.advertising.dao.impl.feedbackAggregation;

import edu.sjsu.bigdata.advertising.dao.interfaces.feedbackAggregation.DailyFeedbackAggregationDao;
import edu.sjsu.bigdata.advertising.model.feedbackAggregation.DailyFeedbackAggregation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * This is the advertisement preference profile service implementation. User:
 * lei zhang Date: 13-8-13
 */
@Repository
public class DailyFeedbackAggregationDaoImpl implements DailyFeedbackAggregationDao {

	@Autowired
	private MongoTemplate mongoTemplate;

	public static final String COLLECTION_NAME = "dailyFeedbackAggregation";

	public DailyFeedbackAggregation saveDailyFeedbackAggregation(DailyFeedbackAggregation entity) {
		if (!mongoTemplate.collectionExists(DailyFeedbackAggregation.class)) {
			mongoTemplate.createCollection(DailyFeedbackAggregation.class);
		}

		mongoTemplate.insert(entity, COLLECTION_NAME);
		return mongoTemplate.findById(entity.getId(), DailyFeedbackAggregation.class);
	}
	
	public DailyFeedbackAggregation updateDailyFeedbackAggregation(DailyFeedbackAggregation entity) {
		mongoTemplate.insert(entity, COLLECTION_NAME);	
		return mongoTemplate.findById(entity.getId(), DailyFeedbackAggregation.class);
	}


	public boolean deleteDailyFeedbackAggregation(String id) {

        Object object = mongoTemplate.findById(id, DailyFeedbackAggregation.class);
		mongoTemplate.remove(object);

        return true;

	}


	public DailyFeedbackAggregation findDailyFeedbackAggregationById(String id) {
		return mongoTemplate.findById(id, DailyFeedbackAggregation.class);
	}

	public List<DailyFeedbackAggregation> findAllDailyFeedbackAggregation() {
		return mongoTemplate.findAll(DailyFeedbackAggregation.class, COLLECTION_NAME);
	}
	
	public List<DailyFeedbackAggregation> findDailyFeedbackAggregationByCategory(String category) {
		 Query query = new Query(Criteria.where("product_category").is(category));
		
		return mongoTemplate.find(query, DailyFeedbackAggregation.class, COLLECTION_NAME);
	}
}
