package edu.sjsu.bigdata.advertising.dao.impl.feedbackAggregation;

import edu.sjsu.bigdata.advertising.dao.interfaces.feedbackAggregation.HourlyFeedbackAggregationDao;
import edu.sjsu.bigdata.advertising.model.feedbackAggregation.HourlyFeedbackAggregation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * This is the advertisement preference profile service implementation. User:
 * lei zhang Date: 13-8-13
 */
@Repository
public class HourlyFeedbackAggregationDaoImpl implements HourlyFeedbackAggregationDao {

	@Autowired
	private MongoTemplate mongoTemplate;

	public static final String COLLECTION_NAME = "hourlyFeedbackAggregation";

	public HourlyFeedbackAggregation saveHourlyFeedbackAggregation(HourlyFeedbackAggregation entity) {
		if (!mongoTemplate.collectionExists(HourlyFeedbackAggregation.class)) {
			mongoTemplate.createCollection(HourlyFeedbackAggregation.class);
		}

		mongoTemplate.insert(entity, COLLECTION_NAME);
		return mongoTemplate.findById(entity.getId(), HourlyFeedbackAggregation.class);
	}
	
	public HourlyFeedbackAggregation updateHourlyFeedbackAggregation(HourlyFeedbackAggregation entity) {
		mongoTemplate.insert(entity, COLLECTION_NAME);	
		return mongoTemplate.findById(entity.getId(), HourlyFeedbackAggregation.class);
	}

	/**
	 * This method is used to delete Advertisement Preference Profile by ID and
	 * return the result if the it has been deleted.
	 * 
	 * @param id
	 *            : This is the advertisement preference profile ID.
	 * @return Return false if the operation is failed; Return true if the
	 *         operation is successful.
	 *         <p/>
	 *         Client Example: Delete: DELETE /recommendation/adprefprofile/{id}
	 */
	public boolean deleteHourlyFeedbackAggregation(String id) {

        Object object = mongoTemplate.findById(id, HourlyFeedbackAggregation.class);
		mongoTemplate.remove(object);

        return true;

	}


	public HourlyFeedbackAggregation findHourlyFeedbackAggregationById(String id) {
		return mongoTemplate.findById(id, HourlyFeedbackAggregation.class);
	}

	public List<HourlyFeedbackAggregation> findAllHourlyFeedbackAggregation() {
		return mongoTemplate.findAll(HourlyFeedbackAggregation.class, COLLECTION_NAME);
	}
	
	 public List<HourlyFeedbackAggregation> findHourlyFeedbackAggregationByCategory(String category){
		 Query query = new Query(Criteria.where("product_category").is(category));
		 
		 return mongoTemplate.find(query, HourlyFeedbackAggregation.class, COLLECTION_NAME);
	 }
}
