/**
 * 
 */
package edu.sjsu.bigdata.advertising.dao.interfaces.security;

import java.util.Collection;
import java.util.List;

import edu.sjsu.bigdata.advertising.exception.dao.AdvertisngAccessException;
import edu.sjsu.bigdata.advertising.model.security.HttpUser;
import edu.sjsu.bigdata.advertising.model.security.Role;
import edu.sjsu.bigdata.advertising.model.security.User;

/**
 * @author yunxue
 *
 */
public interface IUserRoleDao {

	public User getUserById(String id) throws AdvertisngAccessException;
	public User getUserByEmail(String email) throws AdvertisngAccessException;
	public void saveOrUpdateUser(User object) throws AdvertisngAccessException;
	public List<User> getAllUser() throws AdvertisngAccessException;
	public void deleteUser(String Id) throws AdvertisngAccessException;
	
	public Role getRoleById(String id) throws AdvertisngAccessException;
	public Role getRoleByRole(String id) throws AdvertisngAccessException;
	public void saveOrUpdateRole(Role role) throws AdvertisngAccessException;
	public List<Role> getAllRole() throws AdvertisngAccessException;
	public void deleteRole(String roleName) throws AdvertisngAccessException;
	
	public HttpUser authenticate(String email, String password) throws AdvertisngAccessException;
	public HttpUser register(User user) throws AdvertisngAccessException;
}


