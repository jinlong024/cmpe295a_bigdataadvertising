package edu.sjsu.bigdata.advertising.dao.impl.feedbackAggregation;

import edu.sjsu.bigdata.advertising.dao.interfaces.feedbackAggregation.DailyFeedbackAggregationDao;
import edu.sjsu.bigdata.advertising.dao.interfaces.feedbackAggregation.MonthlyFeedbackAggregationDao;
import edu.sjsu.bigdata.advertising.model.feedbackAggregation.DailyFeedbackAggregation;
import edu.sjsu.bigdata.advertising.model.feedbackAggregation.MonthlyFeedbackAggregation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * This is the advertisement preference profile service implementation. 
 * Author:  lei zhang Date: 13-8-13
 */
@Repository
public class MonthlyFeedbackAggregationDaoImpl implements MonthlyFeedbackAggregationDao {

	@Autowired
	private MongoTemplate mongoTemplate;

	public static final String COLLECTION_NAME = "monthlyFeedbackAggregation";

	public MonthlyFeedbackAggregation saveMonthlyFeedbackAggregation(MonthlyFeedbackAggregation entity) {
		if (!mongoTemplate.collectionExists(MonthlyFeedbackAggregation.class)) {
			mongoTemplate.createCollection(MonthlyFeedbackAggregation.class);
		}

		mongoTemplate.insert(entity, COLLECTION_NAME);
		return mongoTemplate.findById(entity.getId(), MonthlyFeedbackAggregation.class);
	}
	
	public MonthlyFeedbackAggregation updateMonthlyFeedbackAggregation(MonthlyFeedbackAggregation entity) {
		mongoTemplate.insert(entity, COLLECTION_NAME);	
		return mongoTemplate.findById(entity.getId(), MonthlyFeedbackAggregation.class);
	}


	public boolean deleteMonthlyFeedbackAggregation(String id) {

        Object object = mongoTemplate.findById(id, MonthlyFeedbackAggregation.class);
		mongoTemplate.remove(object);

        return true;

	}


	public MonthlyFeedbackAggregation findMonthlyFeedbackAggregationById(String id) {
		return mongoTemplate.findById(id, MonthlyFeedbackAggregation.class);
	}

	public List<MonthlyFeedbackAggregation> findAllMonthlyFeedbackAggregation() {
		return mongoTemplate.findAll(MonthlyFeedbackAggregation.class, COLLECTION_NAME);
	}
	
	public List<MonthlyFeedbackAggregation> findMonthlyFeedbackAggregationByCategory(String category) {
		
		 Query query = new Query(Criteria.where("product_category").is(category));
		
		return mongoTemplate.find(query, MonthlyFeedbackAggregation.class, COLLECTION_NAME);
	}


}
