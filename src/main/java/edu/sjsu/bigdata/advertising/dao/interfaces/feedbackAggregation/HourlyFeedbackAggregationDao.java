package edu.sjsu.bigdata.advertising.dao.interfaces.feedbackAggregation;

import edu.sjsu.bigdata.advertising.model.feedbackAggregation.HourlyFeedbackAggregation;

import java.util.List;

/**
 * This is the hourly feedback aggregation data access object layer interface.
 * User: Lei Zhang
 * Date: December 01, 2013
 */
public interface HourlyFeedbackAggregationDao {


    public HourlyFeedbackAggregation saveHourlyFeedbackAggregation(HourlyFeedbackAggregation entity);
    
    public HourlyFeedbackAggregation updateHourlyFeedbackAggregation(HourlyFeedbackAggregation entity);

    public boolean deleteHourlyFeedbackAggregation(String id);

    public HourlyFeedbackAggregation findHourlyFeedbackAggregationById(String id);

    public List<HourlyFeedbackAggregation> findAllHourlyFeedbackAggregation();
    
    public List<HourlyFeedbackAggregation> findHourlyFeedbackAggregationByCategory(String category);
}
