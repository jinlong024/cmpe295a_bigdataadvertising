package edu.sjsu.bigdata.advertising.dao.interfaces.feedbackAggregation;

import edu.sjsu.bigdata.advertising.model.feedbackAggregation.MonthlyFeedbackAggregation;

import java.util.List;

/**
 * This is the monthly feedback aggregation data access object layer interface.
 * User: Lei Zhang
 * Date: December 01, 2013
 */
public interface MonthlyFeedbackAggregationDao {


    public MonthlyFeedbackAggregation saveMonthlyFeedbackAggregation(MonthlyFeedbackAggregation entity);
    
    public MonthlyFeedbackAggregation updateMonthlyFeedbackAggregation(MonthlyFeedbackAggregation entity);

    public boolean deleteMonthlyFeedbackAggregation(String id);

    public MonthlyFeedbackAggregation findMonthlyFeedbackAggregationById(String id);

    public List<MonthlyFeedbackAggregation> findAllMonthlyFeedbackAggregation();
    
    public List<MonthlyFeedbackAggregation> findMonthlyFeedbackAggregationByCategory(String category);
}
