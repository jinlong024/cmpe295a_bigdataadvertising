package edu.sjsu.bigdata.advertising.dao.interfaces.feedbackAggregation;

import edu.sjsu.bigdata.advertising.model.feedbackAggregation.DailyFeedbackAggregation;

import java.util.List;

/**
 * This is the daily feedback aggregation data access object layer interface.
 * User: Lei Zhang
 * Date: December 01, 2013
 */
public interface DailyFeedbackAggregationDao {


    public DailyFeedbackAggregation saveDailyFeedbackAggregation(DailyFeedbackAggregation entity);
    
    public DailyFeedbackAggregation updateDailyFeedbackAggregation(DailyFeedbackAggregation entity);

    public boolean deleteDailyFeedbackAggregation(String id);

    public DailyFeedbackAggregation findDailyFeedbackAggregationById(String id);

    public List<DailyFeedbackAggregation> findAllDailyFeedbackAggregation();
    
    public List<DailyFeedbackAggregation> findDailyFeedbackAggregationByCategory(String category);
}
