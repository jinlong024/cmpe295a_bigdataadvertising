/**
 * 
 */
package edu.sjsu.bigdata.advertising.exception;

/**
 * @author yunxue
 *
 */
public class AdvertisngAccessException extends Exception {
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AdvertisngAccessException() {
		super();
	}

	public AdvertisngAccessException(final String message, final Throwable cause) {
		super(message, cause);
	}

	public AdvertisngAccessException(final String message) {
		super(message);
	}

	public AdvertisngAccessException(final Throwable cause) {
		super(cause);
	}



}
