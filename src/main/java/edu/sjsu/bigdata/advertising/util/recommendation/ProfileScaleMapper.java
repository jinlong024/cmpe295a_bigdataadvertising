package edu.sjsu.bigdata.advertising.util.recommendation;

import java.util.HashMap;
import java.util.Map;

/**
 * Creator: Lei Zhang Date: 8/23/2013
 */

public final class ProfileScaleMapper {

	public static final Map<String, Integer> AGE_SCALE_MAPPER = new HashMap<String, Integer>();
	public static final Map<String, Integer> EDUCATION_SCALE_MAPPER = new HashMap<String, Integer>();
	public static final Map<String, Integer> INCOME_SCALE_MAPPER = new HashMap<String, Integer>();
	public static final Map<String, Integer> SEX_SCALE_MAPPER = new HashMap<String, Integer>();
	public static final Map<String, Integer> ETHNICITY_SCALE_MAPPER = new HashMap<String, Integer>();
	public static final Map<String, Integer> RACE_SCALE_MAPPER = new HashMap<String, Integer>();	
	public static final Map<String, Integer> POPULATION_SCALE_MAPPER = new HashMap<String, Integer>();
	public static final Map<String, Integer> HOUSING_UNIT_SCALE_MAPPER = new HashMap<String, Integer>();
	public static final Map<String, Integer> WEATHER_SCALE_MAPPER = new HashMap<String, Integer>();
	
	static {

		// Education related scale definition
        EDUCATION_SCALE_MAPPER.put(null, 0);
		EDUCATION_SCALE_MAPPER.put("Less than high school", 1);
		EDUCATION_SCALE_MAPPER.put("High school graduate", 2);
		EDUCATION_SCALE_MAPPER.put("Some college", 3);
		EDUCATION_SCALE_MAPPER.put("Bachelor's degree or higher", 4);
		
		
		// Race related scale definition
        RACE_SCALE_MAPPER.put(null, 0);
		RACE_SCALE_MAPPER.put("White", 6);
		RACE_SCALE_MAPPER.put("Black or African American", 2);
		RACE_SCALE_MAPPER.put("American Indian and Alaska Native", 3);
		RACE_SCALE_MAPPER.put("Asian", 5);
		RACE_SCALE_MAPPER.put("Native Hawaiian and Other Pacific Islander", 1);
		RACE_SCALE_MAPPER.put("Some Other Race", 4);
		
		// ETHNICITY related scale definition
        ETHNICITY_SCALE_MAPPER.put(null, 0);
		ETHNICITY_SCALE_MAPPER.put("Hispanic", 1);
		ETHNICITY_SCALE_MAPPER.put("Non-Hispanic", 2);
		
		// Sex related scale definition
        SEX_SCALE_MAPPER.put(null, 0);
		SEX_SCALE_MAPPER.put("Male", 1);
		SEX_SCALE_MAPPER.put("Female", 2);
		
		// Income related scale definition
        INCOME_SCALE_MAPPER.put(null, 0);
		INCOME_SCALE_MAPPER.put("Less than $10,000", 1);
		INCOME_SCALE_MAPPER.put("$10,000 to $49,999", 2);
		INCOME_SCALE_MAPPER.put("$50,000 to $99,999", 3);
		INCOME_SCALE_MAPPER.put("$100,000 to $199,999", 4);
		INCOME_SCALE_MAPPER.put("$200,000 or more", 5);
	
		
		// Age related scale definition
        AGE_SCALE_MAPPER.put(null, 0);
		AGE_SCALE_MAPPER.put("Infant (Under 2)", 1);
		AGE_SCALE_MAPPER.put("Child (2-11)", 2);
		AGE_SCALE_MAPPER.put("Youth (12-17)", 3);
		AGE_SCALE_MAPPER.put("Adult (18-64)", 4);
		AGE_SCALE_MAPPER.put("Senior (65 or over)", 5);
		
		// Population related scale definition
        POPULATION_SCALE_MAPPER.put(null, 0);
		POPULATION_SCALE_MAPPER.put("Less than 10,000", 1);
		POPULATION_SCALE_MAPPER.put("10,000 to 99,999", 2);
		POPULATION_SCALE_MAPPER.put("109,000 to 499,999", 3);
		POPULATION_SCALE_MAPPER.put("500,000 to 999,999", 4);
		POPULATION_SCALE_MAPPER.put("1,000,000 or more", 5);
		
		// Housing unit related scale definition
        HOUSING_UNIT_SCALE_MAPPER.put(null, 0);
		HOUSING_UNIT_SCALE_MAPPER.put("Less than 10,000", 1);
		HOUSING_UNIT_SCALE_MAPPER.put("10,000 to 99,999", 2);
		HOUSING_UNIT_SCALE_MAPPER.put("109,000 to 499,999", 3);
		HOUSING_UNIT_SCALE_MAPPER.put("500,000 to 999,999", 4);
		HOUSING_UNIT_SCALE_MAPPER.put("1,000,000 or more", 5);
		
		// Weather related scale definition
        WEATHER_SCALE_MAPPER.put(null, 0);
		WEATHER_SCALE_MAPPER.put("Coastal Weather", 1);
		WEATHER_SCALE_MAPPER.put("Hot Weather", 2);
		WEATHER_SCALE_MAPPER.put("Cold Weather", 3);
		WEATHER_SCALE_MAPPER.put("Mountain Weather", 4);
		

	}

}
