package edu.sjsu.bigdata.advertising.util.recommendation;

import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.text.ParseException;

import java.util.Calendar;
import java.util.Date;


/**
 * Creator: Lei Zhang
 * Project: 2
 * Date: 5/2/13 
 */

public final class DateConverter {

	public static Date toDate(String dateStr)
			throws ParseException {
		
		String dateFormatStr = "yyyy-MM-dd:HH:mm:ss";
		DateFormat df = new SimpleDateFormat(dateFormatStr);
		
		if (dateStr == null || dateStr.length() == 0)
			return null;
		
		return df.parse(dateStr);
	}
	
	public static Date getOneHourAgo(Date date)
			throws ParseException {
		
		System.out.println("Current time is " + date);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.HOUR_OF_DAY, -1);
		Date oneHourAgoDate = calendar.getTime();
		
		System.out.println("One hour ago time is " + oneHourAgoDate);
		
		return oneHourAgoDate;
	}
	
	public static Date getOneDayAgo(Date date)
			throws ParseException {
		
		System.out.println("Current time is " + date);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DAY_OF_MONTH, -1);
		
		Date oneHourAgoDate = calendar.getTime();
		
		System.out.println("One day ago time is " + oneHourAgoDate);
		
		return oneHourAgoDate;
	}
	
	public static Date getOneMonthAgo(Date date)
			throws ParseException {
		
		System.out.println("Current time is " + date);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.MONTH, -1);
		
		Date oneMonthAgoDate = calendar.getTime();
		
		System.out.println("One month ago time is " + oneMonthAgoDate);
		
		return oneMonthAgoDate;
	}
	
	public static Date getTwoMonthAgo(Date date)
			throws ParseException {
		
		System.out.println("Current time is " + date);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.MONTH, -2);
		
		Date twoMonthAgoDate = calendar.getTime();
		
		System.out.println("Two month ago time is " + twoMonthAgoDate);
		
		return twoMonthAgoDate;
	}
	
	public static Date getDateWithRoundHour(Date date)
			throws ParseException {
		
		System.out.println("Current time is " + date);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		
		Date dateWithRoundHour = calendar.getTime();
		
		System.out.println("Day with Round Hour is " + dateWithRoundHour);
		
		return dateWithRoundHour;
	}

}
