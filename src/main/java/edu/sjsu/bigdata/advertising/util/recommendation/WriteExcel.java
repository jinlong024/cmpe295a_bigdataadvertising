package edu.sjsu.bigdata.advertising.util.recommendation;

import java.io.File;
import java.io.IOException;
import java.util.Locale;

import jxl.CellView;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.UnderlineStyle;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

//http://www.vogella.com/articles/JavaExcel/article.html
public class WriteExcel {

	private static int rawNumber=0;
	private WritableCellFormat timesBoldUnderline;
	private WritableCellFormat times;
	private String inputFile;
	private WritableWorkbook workbook = null;
	private WritableSheet excelSheet = null;

	public WriteExcel(String inputFile) {
		this.inputFile = inputFile;
		try {
			init();
		} catch (WriteException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void init() throws IOException, WriteException {
		File file = new File(inputFile);
		WorkbookSettings wbSettings = new WorkbookSettings();

		wbSettings.setLocale(new Locale("en", "EN"));

		workbook = Workbook.createWorkbook(file, wbSettings);
		workbook.createSheet("Report", 0);
		excelSheet = workbook.getSheet(0);
		createLabel(excelSheet);
		
		
	}

	public void write() throws IOException, WriteException {
	
		//createContent(column1, column2);

		workbook.write();
		
	}

	public void close() throws IOException, WriteException {
		workbook.close();
	}

	private void createLabel(WritableSheet sheet) throws WriteException {
		// Lets create a times font
		WritableFont times10pt = new WritableFont(WritableFont.TIMES, 10);
		// Define the cell format
		times = new WritableCellFormat(times10pt);
		// Lets automatically wrap the cells
		times.setWrap(true);

		// Create create a bold font with unterlines
		WritableFont times10ptBoldUnderline = new WritableFont(
				WritableFont.TIMES, 10, WritableFont.BOLD, false,
				UnderlineStyle.SINGLE);
		timesBoldUnderline = new WritableCellFormat(times10ptBoldUnderline);
		// Lets automatically wrap the cells
		timesBoldUnderline.setWrap(true);

		CellView cv = new CellView();
		cv.setFormat(times);
		cv.setFormat(timesBoldUnderline);
		cv.setAutosize(true);

	}

	public  void createContent(String column1, String column2) throws WriteException,
			RowsExceededException {

		// Now a bit of text
		//for (int i = 0; i < 20; i++) {
			// First column
			addLabel(excelSheet, 0, rawNumber, column1);
			
			// Second column
			addLabel(excelSheet, 1, rawNumber, column2);
			
			rawNumber++;
		//}
	}

	private void addLabel(WritableSheet sheet, int column, int row, String s)
			throws WriteException, RowsExceededException {
		Label label;
		label = new Label(column, row, s, times);
		sheet.addCell(label);
	}

	public static void main(String[] args) throws WriteException, IOException {
		WriteExcel test = new WriteExcel("c:/temp/sentiment_trainingset.xls");

		test.createContent("Hello", "World");
		test.createContent("world", "hello");
		test.write();
	
		test.close();
		System.out
				.println("Please check the result file under c:/temp/sentiment_trainingset.xls ");
	}
}