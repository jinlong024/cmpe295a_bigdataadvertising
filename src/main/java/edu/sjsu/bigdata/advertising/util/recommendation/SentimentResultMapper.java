package edu.sjsu.bigdata.advertising.util.recommendation;

import java.util.HashMap;
import java.util.Map;

/**
 * Creator: Lei Zhang 
 * Date: 8/23/2013
 */

public final class SentimentResultMapper {

	public static final Map<Integer, String> SENTIMENTRESULT_MAPPER = new HashMap<Integer, String>();
	
	static {

		// Education related scale definition
		SENTIMENTRESULT_MAPPER.put(-2, "STRONGLY_DISLIKE");
		SENTIMENTRESULT_MAPPER.put(-1, "DISLIKE");
		SENTIMENTRESULT_MAPPER.put(0, "NEUTRAL");
		SENTIMENTRESULT_MAPPER.put(1, "LIKE");
		SENTIMENTRESULT_MAPPER.put(2, "STRONGLY_LIKE");
		
		
		

	}

}
