/**
 * 
 */
package edu.sjsu.bigdata.advertising.service.datacollector.yelp;

import edu.sjsu.bigdata.advertising.exception.dao.AdvertisngAccessException;
import edu.sjsu.bigdata.advertising.service.datacollector.yelp.aggregation.ReviewByBusinessAggregation;
import edu.sjsu.bigdata.advertising.service.datacollector.yelp.aggregation.ReviewByCategoryAggregation;
import edu.sjsu.bigdata.advertising.service.datacollector.yelp.aggregation.ReviewByDailyTimeAggregation;
import edu.sjsu.bigdata.advertising.service.datacollector.yelp.aggregation.ReviewByLocationAggregation;
import edu.sjsu.bigdata.advertising.service.datacollector.yelp.aggregation.ReviewByMonthlyTimeAggregation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * @author yunxue
 *
 */
@Service
public class YelpDataServiceImpl implements IYelpDataService {
	
	//@Autowired
	private IYelpDataDao yelpDataDao;
	
	
	public IYelpDataDao getYelpDataDao() {
		return yelpDataDao;
	}


	public void setYelpDataDao(IYelpDataDao yelpDataDao) {
		this.yelpDataDao = yelpDataDao;
	}


	@Override
	public YelpBusiness getYelpBusinessById(String bId) throws AdvertisngAccessException {
		YelpBusiness business = yelpDataDao.getYelpBusinessById(bId);
		return business;
	}


	@Override
	public void insertYelpBusiness(YelpBusiness business) throws AdvertisngAccessException {
		yelpDataDao.insertYelpBusiness(business);
	}


	@Override
	public List<YelpBusiness> getAllYelpBusiness() throws AdvertisngAccessException {
		List<YelpBusiness> businesses = yelpDataDao.getAllYelpBusiness();
		return businesses;
	}


	@Override
	public void deleteYelpBusiness(String bId) throws AdvertisngAccessException {
		yelpDataDao.deleteYelpBusiness(bId);
		
	}


	@Override
	public YelpReview getYelpReviewById(String rId) throws AdvertisngAccessException {
		YelpReview review = yelpDataDao.getYelpReviewById(rId);
		return review;
	}


	@Override
	public void insertYelpReview(YelpReview review) throws AdvertisngAccessException {
		yelpDataDao.insertYelpReview(review);
	}


	@Override
	public List<YelpReview> getAllYelpReview() throws AdvertisngAccessException {
		List<YelpReview> reviews = yelpDataDao.getAllYelpReview();
		return reviews;
	}


	@Override
	public void deleteYelpReview(String rId) throws AdvertisngAccessException {
		yelpDataDao.deleteYelpReview(rId);
	}
	
	@Override
	public void aggregateBusiness(String businessId) throws AdvertisngAccessException {
		List <YelpReview> reviews =  yelpDataDao.getAllYelpReview();
		for (YelpReview review: reviews) {
			if (review.getBusiness_id() == businessId) {
			}
		}
	}
	
	
	@Override
	public void insertYelpBusinessAggregation(ReviewByBusinessAggregation bAgg) throws AdvertisngAccessException {
		yelpDataDao.insertYelpBusinessAggregation(bAgg);
	}
	
	@Override
	public void insertYelpLocationAggregation(ReviewByLocationAggregation lAgg) throws AdvertisngAccessException {
		yelpDataDao.insertYelpLocationAggregation(lAgg);
	}
	
	@Override
	public void insertYelpCategoryAggregation(ReviewByCategoryAggregation cAgg) throws AdvertisngAccessException {
		yelpDataDao.insertYelpCategoryAggregation(cAgg);
	}


	@Override
	public List<ReviewByBusinessAggregation> getAllYelpBusinessAggregationData() throws AdvertisngAccessException {
		List<ReviewByBusinessAggregation> bAggs = yelpDataDao.getAllYelpBusinessAggregationData();
		return bAggs;
	}


	@Override
	public List<ReviewByLocationAggregation> getAllYelpLocationAggregationData() throws AdvertisngAccessException {
		List<ReviewByLocationAggregation> lAggs = yelpDataDao.getAllYelpLocationAggregationData();
		return lAggs;
	}


	@Override
	public List<ReviewByCategoryAggregation> getAllYelpCategoryAggregationData() throws AdvertisngAccessException {
		List<ReviewByCategoryAggregation> cAggs = yelpDataDao.getAllYelpCategoryAggregationData();
		return cAggs;
	}


	@Override
	public void insertYelpDailyTimeAggregation(ReviewByDailyTimeAggregation dAgg) throws AdvertisngAccessException {
		yelpDataDao.insertYelpDailyTimeAggregation(dAgg);
	}


	@Override
	public void insertYelpMonthlyTimeAggregation(ReviewByMonthlyTimeAggregation mAgg) throws AdvertisngAccessException {
		yelpDataDao.insertYelpMonthlyTimeAggregation(mAgg);
	}


	@Override
	public List<ReviewByDailyTimeAggregation> getAllYelpDailyTimeAggregationData() throws AdvertisngAccessException {
		List<ReviewByDailyTimeAggregation> dAggs = yelpDataDao.getAllYelpDailyTimeAggregationData();
		return dAggs;
	}


	@Override
	public List<ReviewByMonthlyTimeAggregation> getAllYelpMonlyTimeAggregationData() throws AdvertisngAccessException {
		List<ReviewByMonthlyTimeAggregation> mAggs = yelpDataDao.getAllYelpMonlyTimeAggregationData();
		return mAggs;
	}


	@Override
	public ReviewByCategoryAggregation findCategoryAggregationByName(String category) throws AdvertisngAccessException {
		ReviewByCategoryAggregation agg = yelpDataDao.findCategoryAggregationByName(category);
		return agg;
	}


	@Override
	public ReviewByLocationAggregation findLocationAggregationByName(String category, String city) throws AdvertisngAccessException {
		ReviewByLocationAggregation agg = yelpDataDao.findLocationAggregationByName(category, city);
		return agg;
	}


	@Override
	public ReviewByBusinessAggregation findBusinessAggregationByName(String bname) throws AdvertisngAccessException {
		ReviewByBusinessAggregation agg = yelpDataDao.findBusinessAggregationByName(bname);
		return agg;
	}


	/*@Override
	public ReviewByDailyTimeAggregation findDailyTimeAggregationByValue(String category, String date) throws AdvertisngAccessException {
		ReviewByDailyTimeAggregation agg = yelpDataDao.findDailyTimeAggregationByValue(category, date);
		return agg;
	}
	
	@Override
	public ReviewByMonthlyTimeAggregation findMonthlyTimeAggregationByValue(String category, String month) throws AdvertisngAccessException {
		ReviewByMonthlyTimeAggregation agg = yelpDataDao.findMonthlyTimeAggregationByValue(category, month);
		return agg;
	}*/

	
	
	
	@Override
	public List<ReviewByLocationAggregation> findLocationAggregationByName(String city) throws AdvertisngAccessException {
		List<ReviewByLocationAggregation> aggs = yelpDataDao.findLocationAggregationByName(city);
		return aggs;
	}

	@Override
	public List<ReviewByDailyTimeAggregation> findDailyTimeAggregationByValue(String startDate, String endDate) throws AdvertisngAccessException {
		List<ReviewByDailyTimeAggregation> aggs = yelpDataDao.findDailyTimeAggregationByValue(startDate, endDate);
		return aggs;
	}
	
	@Override
	public List<ReviewByMonthlyTimeAggregation> findMonthlyTimeAggregationByValue(String startMonth, String endMonth) throws AdvertisngAccessException {
		List<ReviewByMonthlyTimeAggregation> aggs = yelpDataDao.findMonthlyTimeAggregationByValue(startMonth, endMonth);
		return aggs;
	}
}
