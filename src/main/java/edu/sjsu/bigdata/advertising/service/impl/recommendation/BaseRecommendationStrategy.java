package edu.sjsu.bigdata.advertising.service.impl.recommendation;


import edu.sjsu.bigdata.advertising.model.recommendation.AdCondition;
import edu.sjsu.bigdata.advertising.model.recommendation.AdPrefProfile;
import edu.sjsu.bigdata.advertising.model.recommendation.DemographicProfile;
import edu.sjsu.bigdata.advertising.model.recommendation.RecommendationResult;
import edu.sjsu.bigdata.advertising.service.interfaces.recommendation.AdPrefProfileService;
import edu.sjsu.bigdata.advertising.service.interfaces.recommendation.DemographicProfileService;
import edu.sjsu.bigdata.advertising.service.interfaces.recommendation.RecommendationStrategy;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * This is the class to implement Recommendation common Algorithm. 
 * Author: Lei Zhang
 * Date: 8-13-2013
 */
@Service
public abstract class BaseRecommendationStrategy implements
		RecommendationStrategy {


	private AdPrefProfileService adPrefProfileService;

	private DemographicProfileService demographicProfileService;
	
	protected final static Integer numberOfDimensions = 9;

	private AdPrefProfile adPrefProfile = null;

	private List<DemographicProfile> demographicProfiles = null;

    public AdPrefProfileService getAdPrefProfileService() {
        return adPrefProfileService;
    }

    public void setAdPrefProfileService(AdPrefProfileService adPrefProfileService) {
        this.adPrefProfileService = adPrefProfileService;
    }

    public DemographicProfileService getDemographicProfileService() {
        return demographicProfileService;
    }

    public void setDemographicProfileService(DemographicProfileService demographicProfileService) {
        this.demographicProfileService = demographicProfileService;
    }

	public RecommendationResult getRecommendation(String id) {

       
		adPrefProfile = adPrefProfileService.findProfileById(id);

      
		demographicProfiles = demographicProfileService.findAllProfile();

		
		//2147483647 is the maximum integer value
		Double baseDistance = new Double(2147483647.0);

		Double result = 0.0;

		DemographicProfile targetDemographicProfile = null;

		for (DemographicProfile demographicProfile : demographicProfiles) {
			result = calculateDistance(adPrefProfile, demographicProfile);
			if (result < baseDistance) {
				baseDistance = result;
				targetDemographicProfile = demographicProfile;
			}
		}

		RecommendationResult recommendation = new RecommendationResult();

		recommendation.setAdId(id);
		recommendation.setDemographicProfileId(targetDemographicProfile.getId());

		return recommendation;
	}

	public RecommendationResult getRecommendation(AdCondition condition) {

		return null;
	}

	abstract Double calculateDistance(AdPrefProfile adPrefProfile,
			DemographicProfile demographicProfile);
}
