/**
 * 
 */
package edu.sjsu.bigdata.advertising.service.security;

import java.util.List;

import edu.sjsu.bigdata.advertising.exception.dao.AdvertisngAccessException;
import edu.sjsu.bigdata.advertising.model.security.HttpUser;
import edu.sjsu.bigdata.advertising.model.security.Role;
import edu.sjsu.bigdata.advertising.model.security.User;

/**
 * @author yunxue
 *
 */
public interface IUserSecurityService {
	public User getUserById(String userId) throws AdvertisngAccessException;
	public User getUserByEmail(String email) throws AdvertisngAccessException;
	public List<User> getAllUsers() throws AdvertisngAccessException;
	public void addUser(User user) throws AdvertisngAccessException;
	public void deleteUser(String email)  throws AdvertisngAccessException;
	
	
	public Role getRoleById(String roleId) throws AdvertisngAccessException;
	public Role getRoleByRole(String roleName) throws AdvertisngAccessException;
	public List<Role> getAllUserRoles() throws AdvertisngAccessException;
	public void addRole(Role role)  throws AdvertisngAccessException;
	public void deleteRole(String roleName) throws AdvertisngAccessException;

	public HttpUser authenticate(String email, String password) throws AdvertisngAccessException;
	public void register(User user) throws AdvertisngAccessException;
}
