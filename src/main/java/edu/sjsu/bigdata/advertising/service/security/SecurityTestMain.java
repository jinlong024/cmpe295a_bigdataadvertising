/**
 * 
 */
package edu.sjsu.bigdata.advertising.service.security;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import edu.sjsu.bigdata.advertising.dao.impl.security.UserRoleDaoImpl;
import edu.sjsu.bigdata.advertising.dao.interfaces.security.IUserRoleDao;
import edu.sjsu.bigdata.advertising.exception.dao.AdvertisngAccessException;
import edu.sjsu.bigdata.advertising.model.security.HttpUser;
import edu.sjsu.bigdata.advertising.model.security.Role;
import edu.sjsu.bigdata.advertising.model.security.User;

/**
 * @author yuxue
 *
 */
public class SecurityTestMain {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//UserRoleDao uService = new UserRoleDaoImpl();
		//UserPrivilegeService securityService = new UserPrivilegeService();

		List<Role> roles = null;
		List<User> users = null;
		
		ApplicationContext appContext = new ClassPathXmlApplicationContext("applicationContext.xml");
		//UserRoleDao uService = (UserRoleDaoImpl) appContext.getBean("userRoleDao");	
		IUserSecurityService uService = (UserSecurityServiceImpl)appContext.getBean("userSecurityService");
		
		//MongoTemplate tmpl = (MongoTemplate) appContext.getBean("mongoTemplate");	
		//uService.setMongoTemplate(tmpl);
		
		try {
			//////Query searchUserQuery = new Query(Criteria.where("role").is("ADMIN"));
			//////role = tmpl.findOne(searchUserQuery, Role.class);
			
			//////role = uService.getRoleById("5216c82c57171f23d2b8ddb3");
			////System.out.println("1. find - role by ROlE : " + role);
			/////System.out.println(role.getRole() + ", " + role.getDescription());
			
			/*roles = (List<Role>) uService.getAllRole();
			for (int i=0; i<roles.size(); i++) {
				System.out.println(roles.get(i).getRole() + ", " + roles.get(i).getDescription());
			}*/
			
			//ADMIN, administrator
			
			
			
			/*role = uService.getRoleByRole("HONEYYYY");
			if (role == null) {
				role = new Role();
				role.setRole("HONEYYYY");
				role.setDescription("Happy lifeeeeeeeee");
			}
			else {
				role.setDescription("I loveeee it");
			}
			uService.saveOrUpdateRole(role);
			System.out.println("Done saveOrUpdateRole\n\n");*/
			
			
			/*uService.deleteRole("HONEYYYY");
			roles = (List<Role>) uService.getAllRole();
			for (int i=0; i<roles.size(); i++) {
				System.out.println(roles.get(i).getRole() + ", " + roles.get(i).getDescription());
			}*/
			
			
			/*users = (List<User>) uService.getAllUser();
			for (int i=0; i<users.size(); i++) {
				System.out.println(users.get(i).getUserId() + ", " + users.get(i).getPassword() 
							+  users.get(i).getEmail());
			}*/		
			
			/////User user = uService.getUserById("5216c92b57171f23d2b8ddb6");
			/////System.out.println(user.getEmail() + ",  " + user.getLastName());
			
			/*User user = uService.getUserByEmail("kiwi@ccc.com");
			if (user == null) {
				user = new User();
				user.setEmail("kiwi@ccc.com");
				user.setFirstName("Candy");
				user.setLastName("Kiwi");
				user.setPassword("kiwi");
				user.setRoleId("5216c89957171f23d2b8ddb4");
				user.setContact("333-333-3333");
			}
			else {
				//user.setEmail("aaa@aaa.com");
				//user.setPassword("app");
				//user.setFirstName("Apple");
				//user.setLastName("Red");
				user.setContact("111-111-1111");
				//user.setRoleId("5216c89957171f23d2b8ddb4");
			}
			uService.saveOrUpdateUser(user);
			System.out.println("Done saveOrUpdateUser\n\n");*/
			
			//////uService.deleteUser("kiwi@ccc.com");
			
			
			/*HttpUser httpUser = uService.authenticate("tomato@ggg.com", "tomato");
			System.out.println(httpUser.getAuthResult());
			if (httpUser.getAuthResult() == -1) {
				System.out.println("Registering.......");
				Role role = uService.getRoleByRole("ADMIN");
				User user = new User();
				user.setEmail("tomato@ggg.com");
				user.setFirstName("Giggle");
				user.setLastName("Tomato");
				user.setPassword("tomato");
				user.setRole(role);
				user.setContact("777-777-777");
				uService.register(user);
				User ur = uService.getUserByEmail("tomato@ggg.com");
				httpUser.setUser(ur);
			}
			else if (httpUser.getAuthResult() == 0) {
				System.out.println("FAIL, Try log in again");
			}
			else if (httpUser.getAuthResult() == 1) {
				System.out.println("PASS");
			}
			
			if (httpUser.getUser() != null) {
				System.out.println(httpUser.getUser().getUserId() +  ", " + httpUser.getUser().getEmail() +
						", " + httpUser.getUser().getPassword() + ", " + httpUser.getUser().getFirstName() 
						+ ", " + httpUser.getUser().getLastName() + ", " +httpUser.getUser().getContact() );
			}*/
 			
			/*users = (List<User>) uService.getAllUsers();
			for (int i=0; i<users.size(); i++) {
				System.out.println(users.get(i).getUserId() +  ", " + users.get(i).getEmail() +
							", " + users.get(i).getPassword() + ", " + users.get(i).getFirstName() 
							+ ", " + users.get(i).getLastName() + ", " + users.get(i).getContact() );
			}*/
			
		} 
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}

}
