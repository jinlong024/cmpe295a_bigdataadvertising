package edu.sjsu.bigdata.advertising.service.impl.recommendation;


import edu.sjsu.bigdata.advertising.dao.interfaces.recommendation.DemographicProfileDao;
import edu.sjsu.bigdata.advertising.model.recommendation.DemographicProfile;
import edu.sjsu.bigdata.advertising.service.interfaces.recommendation.DemographicProfileService;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;


/**
 * This is the advertisement preference profile service implementation. User:
 * lei zhang Date: 13-8-13
 */
@Service
public class DemographicProfileServiceImpl implements DemographicProfileService {

	@Autowired
	private DemographicProfileDao demographicProfileDao;


	/**
	 * This method is used to insert or update Advertisement Preference Profile
	 * and return its ID.
	 * 
	 * @param entity
	 *            : This is the advertisement preference profile to be saved.
	 * @return Return null if the operation is failed; Return the profile ID if
	 *         the operation is successful.
	 */

	public DemographicProfile saveProfile(DemographicProfile entity) {

		return demographicProfileDao.saveProfile(entity);
	}
	
	public DemographicProfile updateProfile(DemographicProfile entity) {
		return demographicProfileDao.updateProfile(entity);
	}

	/**
	 * This method is used to delete Advertisement Preference Profile by ID and
	 * return the result if the it has been deleted.
	 * 
	 * @param id
	 *            : This is the advertisement preference profile ID.
	 * @return Return false if the operation is failed; Return true if the
	 *         operation is successful.
	 */
	public boolean deleteProfile(String id) {

		return demographicProfileDao.deleteProfile(id);
	}

	/**
	 * This method is used to get Advertisement Preference Profile by ID.
	 * 
	 * @param id
	 *            : This is the advertisement preference profile ID.
	 * @return Return NULL if the operation is failed; Return the advertisement
	 *         preference profile if the operation is successful.
	 */
	public DemographicProfile findProfileById(String id) {
		return demographicProfileDao.findProfileById(id);
	}

	/**
	 * This method is used to get list of existing Advertisement Preference
	 * Profiles.
	 * 
	 * @return Return NULL if the operation is failed; Return the list of
	 *         existing advertisement preference profiles if the operation is
	 *         successful.
	 */
	public List<DemographicProfile> findAllProfile() {
		return demographicProfileDao.findAllProfile();
	}
}
