package edu.sjsu.bigdata.advertising.service.impl.recommendation;


import org.springframework.stereotype.Component;


import edu.sjsu.bigdata.advertising.model.recommendation.AdPrefProfile;
import edu.sjsu.bigdata.advertising.model.recommendation.DemographicProfile;

import edu.sjsu.bigdata.advertising.util.recommendation.ProfileScaleMapper;

/**
 * This is the class to implement Synthesis Index Strategy (SIS) Recommendation Algorithm. 
 * Author: Lei Zhang 
 * Date: 13-8-13
 */
@Component
public class SynthesisRecommendationStrategy extends BaseRecommendationStrategy {

	Double calculateDistance(AdPrefProfile adPrefProfile,
			DemographicProfile demographicProfile) {

		Double distance = 0.0;
		Double adProfileArea = 0.0;
		Double demographicProfileArea = 0.0;

        try{
		demographicProfileArea = demographicProfileArea
				+ 0.5
				* Math.sin(2 * Math.PI / numberOfDimensions)
				* ProfileScaleMapper.AGE_SCALE_MAPPER.get(demographicProfile
						.getAge())
				* ProfileScaleMapper.EDUCATION_SCALE_MAPPER.get(demographicProfile
						.getEducation());
		
		demographicProfileArea = demographicProfileArea
				+ 0.5
				* Math.sin(2 * Math.PI / numberOfDimensions)
				* ProfileScaleMapper.EDUCATION_SCALE_MAPPER.get(demographicProfile
						.getEducation())
				* ProfileScaleMapper.INCOME_SCALE_MAPPER.get(demographicProfile
						.getIncome());
		
		demographicProfileArea = demographicProfileArea
				+ 0.5
				* Math.sin(2 * Math.PI / numberOfDimensions)
				* ProfileScaleMapper.INCOME_SCALE_MAPPER.get(demographicProfile
						.getIncome())
				* ProfileScaleMapper.SEX_SCALE_MAPPER.get(demographicProfile
						.getSex());

		demographicProfileArea = demographicProfileArea
				+ 0.5
				* Math.sin(2 * Math.PI / numberOfDimensions)
				* ProfileScaleMapper.SEX_SCALE_MAPPER.get(demographicProfile
						.getSex())
				* ProfileScaleMapper.ETHNICITY_SCALE_MAPPER.get(demographicProfile
						.getEthnicity());
		
		demographicProfileArea = demographicProfileArea
				+ 0.5
				* Math.sin(2 * Math.PI / numberOfDimensions)
				* ProfileScaleMapper.ETHNICITY_SCALE_MAPPER.get(demographicProfile
						.getEthnicity())
				* ProfileScaleMapper.RACE_SCALE_MAPPER.get(demographicProfile
						.getRace());


		demographicProfileArea = demographicProfileArea
				+ 0.5
				* Math.sin(2 * Math.PI / numberOfDimensions)
				* ProfileScaleMapper.RACE_SCALE_MAPPER.get(demographicProfile
						.getRace())
				* ProfileScaleMapper.POPULATION_SCALE_MAPPER.get(demographicProfile
						.getPopulation());


		demographicProfileArea = demographicProfileArea
				+ 0.5
				* Math.sin(2 * Math.PI / numberOfDimensions)
				* ProfileScaleMapper.POPULATION_SCALE_MAPPER.get(demographicProfile
						.getPopulation())
				* ProfileScaleMapper.HOUSING_UNIT_SCALE_MAPPER.get(demographicProfile
						.getHouse());

		demographicProfileArea = demographicProfileArea
				+ 0.5
				* Math.sin(2 * Math.PI / numberOfDimensions)
				* ProfileScaleMapper.HOUSING_UNIT_SCALE_MAPPER.get(demographicProfile
						.getHouse())
				* ProfileScaleMapper.WEATHER_SCALE_MAPPER.get(demographicProfile
						.getWeather());

		demographicProfileArea = demographicProfileArea
				+ 0.5
				* Math.sin(2 * Math.PI / numberOfDimensions)
				* ProfileScaleMapper.WEATHER_SCALE_MAPPER.get(demographicProfile
						.getWeather())
				* ProfileScaleMapper.AGE_SCALE_MAPPER.get(demographicProfile
						.getAge());

		adProfileArea = adProfileArea
				+ 0.5
				* Math.sin(2 * Math.PI / numberOfDimensions)
				* ProfileScaleMapper.AGE_SCALE_MAPPER.get(adPrefProfile
						.getAge())
				* ProfileScaleMapper.EDUCATION_SCALE_MAPPER.get(adPrefProfile
						.getEducation());
		
		adProfileArea = adProfileArea
				+ 0.5
				* Math.sin(2 * Math.PI / numberOfDimensions)
				* ProfileScaleMapper.EDUCATION_SCALE_MAPPER.get(adPrefProfile
						.getEducation())
				* ProfileScaleMapper.INCOME_SCALE_MAPPER.get(adPrefProfile
						.getIncome());
		
		adProfileArea = adProfileArea
				+ 0.5
				* Math.sin(2 * Math.PI / numberOfDimensions)
				* ProfileScaleMapper.INCOME_SCALE_MAPPER.get(adPrefProfile
						.getIncome())
				* ProfileScaleMapper.SEX_SCALE_MAPPER.get(adPrefProfile
						.getSex());

		adProfileArea = adProfileArea
				+ 0.5
				* Math.sin(2 * Math.PI / numberOfDimensions)
				* ProfileScaleMapper.SEX_SCALE_MAPPER.get(adPrefProfile
						.getSex())
				* ProfileScaleMapper.ETHNICITY_SCALE_MAPPER.get(adPrefProfile
						.getEthnicity());
		
		adProfileArea = adProfileArea
				+ 0.5
				* Math.sin(2 * Math.PI / numberOfDimensions)
				* ProfileScaleMapper.ETHNICITY_SCALE_MAPPER.get(adPrefProfile
						.getEthnicity())
				* ProfileScaleMapper.RACE_SCALE_MAPPER.get(adPrefProfile
						.getRace());


		adProfileArea = adProfileArea
				+ 0.5
				* Math.sin(2 * Math.PI / numberOfDimensions)
				* ProfileScaleMapper.RACE_SCALE_MAPPER.get(adPrefProfile
						.getRace())
				* ProfileScaleMapper.POPULATION_SCALE_MAPPER.get(adPrefProfile
						.getPopulation());


		adProfileArea = adProfileArea
				+ 0.5
				* Math.sin(2 * Math.PI / numberOfDimensions)
				* ProfileScaleMapper.POPULATION_SCALE_MAPPER.get(adPrefProfile
						.getPopulation())
				* ProfileScaleMapper.HOUSING_UNIT_SCALE_MAPPER.get(adPrefProfile
						.getHouse());

		adProfileArea = adProfileArea
				+ 0.5
				* Math.sin(2 * Math.PI / numberOfDimensions)
				* ProfileScaleMapper.HOUSING_UNIT_SCALE_MAPPER.get(adPrefProfile
						.getHouse())
				* ProfileScaleMapper.WEATHER_SCALE_MAPPER.get(adPrefProfile
						.getWeather());

		adProfileArea = adProfileArea
				+ 0.5
				* Math.sin(2 * Math.PI / numberOfDimensions)
				* ProfileScaleMapper.WEATHER_SCALE_MAPPER.get(adPrefProfile
						.getWeather())
				* ProfileScaleMapper.AGE_SCALE_MAPPER.get(adPrefProfile
						.getAge());

		distance = Math.abs(new Double(adProfileArea - demographicProfileArea));
        }catch(Exception e)   {
            distance = 0.0;
            System.out.println("Catching exception when calculating distance:" + e);

        }

		return distance ;

	}
}
