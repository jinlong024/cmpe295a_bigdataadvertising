/**
 * 
 */
package edu.sjsu.bigdata.advertising.service.datacollector.yelp.aggregation;

import java.io.Serializable;
import java.util.Date;

/**
 * @author yunxue
 *
 */
public class ReviewByCategoryAggregation implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String aggregationId;
	private String businessCategory;
	private Integer userCount;
	private Date timeStamp;
	private Integer feedbackLevel;
	
	//private String businessId;
	//private String businessName;
	
	public String getAggregationId() {
		return aggregationId;
	}
	public void setAggregationId(String aggregationId) {
		this.aggregationId = aggregationId;
	}
	
	public String getBusinessCategory() {
		return businessCategory;
	}
	public void setBusinessCategory(String businessCategory) {
		this.businessCategory = businessCategory;
	}
	
	public Integer getUserCount() {
		return userCount;
	}
	public void setUserCount(Integer userCount) {
		this.userCount = userCount;
	}
	
	public Date getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;
	}
	
	public Integer getFeedbackLevel() {
		return feedbackLevel;
	}
	public void setFeedbackLevel(Integer feedbackLevel) {
		this.feedbackLevel = feedbackLevel;
	}
	
	/*public String getBusinessId() {
		return businessId;
	}
	public void setBusinessId(String businessId) {
		this.businessId = businessId;
	}
	
	public String getBusinessName() {
		return businessName;
	}
	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}*/
	
}
