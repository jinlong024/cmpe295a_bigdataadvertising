/**
 * 
 */
package edu.sjsu.bigdata.advertising.service.datacollector.yelp.aggregation;

import java.io.Serializable;
import java.util.Date;

/**
 * @author yunxue
 *
 */
public class ReviewByDailyTimeAggregation extends ReviewByCategoryAggregation {

	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */
	
	private String dailyTime;
	
	public String getDailyTime() {
		return dailyTime;
	}
	public void setDailyTime(String dailyTime) {
		this.dailyTime = dailyTime;
	}

	
}
