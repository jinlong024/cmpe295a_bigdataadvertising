/**
 * 
 */
package edu.sjsu.bigdata.advertising.service.datacollector.yelp;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * @author yunxue
 *
 */
public class JsonReaderUtil {

	//public final static String PATH = "/Yun_Csco/SJSUCourses/Fall2013/CMPE295B/YelpAcademicDataset";
	

	public static List<YelpReview> read(String fileName) {
		
		//String yelpDataFile = PATH + "/" + fileName;
		String packageName = "edu.sjsu.bigdata.advertising.service.datacollector.yelp";
		BufferedReader br = null;
		String line = "";
		List<YelpReview> yelpRList = new ArrayList<YelpReview>();
		 
		try {
			br = new BufferedReader(new FileReader(fileName));
			while ((line = br.readLine()) != null) {
				line = line.trim();
			    YelpReview yelpPojo = (YelpReview) JsonUtil.processYelpJson(line, "YelpReview", packageName);
			    //if (yelpPojo.getBusiness_id().equalsIgnoreCase("j0C1t08TLW_iPPdC-8P_vQ")) {
			    //if (yelpPojo.getBusiness_id().equalsIgnoreCase("4iTRjN_uAdAb7_YZDVHJdg")) {
			    	yelpRList.add(yelpPojo);
			    //}
			}
		} 
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		catch (IOException e) {
			e.printStackTrace();
		} 
		finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	 
		//System.out.println("Reading Yekp data Done");
		return yelpRList;
	  }
	
	
	
	public static List<YelpBusiness> readYelpBusiness(String fileName) {
		String packageName = "edu.sjsu.bigdata.advertising.service.datacollector.yelp";
		BufferedReader br = null;
		String line = "";
		List<YelpBusiness> yelpBList = new ArrayList<YelpBusiness>();
		 
		try {
			br = new BufferedReader(new FileReader(fileName));
			while ((line = br.readLine()) != null) {
				line = line.trim();
				YelpBusiness business = (YelpBusiness) JsonUtil.processYelpJson(line, "YelpBusiness", packageName);
				yelpBList.add(business);
			}
		} 
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		catch (IOException e) {
			e.printStackTrace();
		} 
		finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	 
		//System.out.println("Reading Yekp data Done");
		return yelpBList;
	  }
}
