/**
 * 
 */
package edu.sjsu.bigdata.advertising.service.datacollector.yelp;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import edu.sjsu.bigdata.advertising.dao.impl.security.UserRoleDaoImpl;
import edu.sjsu.bigdata.advertising.dao.interfaces.security.IUserRoleDao;
import edu.sjsu.bigdata.advertising.exception.dao.AdvertisngAccessException;
import edu.sjsu.bigdata.advertising.model.security.HttpUser;
import edu.sjsu.bigdata.advertising.model.security.Role;
import edu.sjsu.bigdata.advertising.model.security.User;
import edu.sjsu.bigdata.advertising.service.datacollector.yelp.aggregation.ReviewByBusinessAggregation;
import edu.sjsu.bigdata.advertising.service.datacollector.yelp.aggregation.ReviewByCategoryAggregation;
import edu.sjsu.bigdata.advertising.service.datacollector.yelp.aggregation.ReviewByDailyTimeAggregation;
import edu.sjsu.bigdata.advertising.service.datacollector.yelp.aggregation.ReviewByLocationAggregation;
import edu.sjsu.bigdata.advertising.service.datacollector.yelp.aggregation.ReviewByMonthlyTimeAggregation;
import edu.sjsu.bigdata.advertising.service.datacollector.yelp.aggregation.YelpReviewWithBInfo;
import edu.sjsu.bigdata.advertising.service.security.IUserSecurityService;
import edu.sjsu.bigdata.advertising.service.security.UserSecurityServiceImpl;

/**
 * @author yunxue
 *
 */
public class YelpMongoPersistencyTestMain {
	
	private Map<String, YelpBusiness> businessMap = new HashMap<String, YelpBusiness>();
	
	private List<YelpReviewWithBInfo> yBInfos = new ArrayList<YelpReviewWithBInfo>();
	
	private Map<String, List<YelpReviewWithBInfo>> businessAggMap = new HashMap<String, List<YelpReviewWithBInfo>>();
	private Map<String, List<YelpReviewWithBInfo>> locationAggMap = new HashMap<String, List<YelpReviewWithBInfo>>();
	private Map<String, List<YelpReviewWithBInfo>> categoryAggMap = new HashMap<String, List<YelpReviewWithBInfo>>();
	
	private Map<String, List<YelpReviewWithBInfo>> dailyTimeAggMap = new HashMap<String, List<YelpReviewWithBInfo>>();
	private Map<String, List<YelpReviewWithBInfo>> monthlyTimeAggMap = new HashMap<String, List<YelpReviewWithBInfo>>();
	
	private List<ReviewByBusinessAggregation> byBusinessAggregation = new ArrayList<ReviewByBusinessAggregation>();
	private List<ReviewByLocationAggregation> byLocationAggregation = new ArrayList<ReviewByLocationAggregation>();
	private List<ReviewByCategoryAggregation> byCategoryAggregation = new ArrayList<ReviewByCategoryAggregation>();
	
	private List<ReviewByDailyTimeAggregation> byDailyTimeAggregation = new ArrayList<ReviewByDailyTimeAggregation>();
	private List<ReviewByMonthlyTimeAggregation> byMonthlyTimeAggregation = new ArrayList<ReviewByMonthlyTimeAggregation>();
	
	private ApplicationContext appContext = new ClassPathXmlApplicationContext("applicationContext.xml");
	private IYelpDataService yService = (YelpDataServiceImpl)appContext.getBean("yelpDataService");
	
	
	private final String categoryDefault = "No Category";
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//UserRoleDao uService = new UserRoleDaoImpl();
		//UserPrivilegeService securityService = new UserPrivilegeService();
		YelpMongoPersistencyTestMain mainn = new YelpMongoPersistencyTestMain();
		mainn.fillInData();
	}

	
	public void fillInData() {
		List<YelpBusiness> yelpBusinesses = null;
		List<YelpReview> yelpReviews = null;
				
		try {
			// {"business_id": "usAsSV36QmUej8--yvN-dg", "full_address": "845 W Southern Ave\nPhoenix, AZ 85041", 
			// "open": true, "categories": ["Food", "Grocery"], "city": "Phoenix", "review_count": 5, 
			// "name": "Food City", "neighborhoods": [], "longitude": -112.0853773, "state": "AZ", "stars": 3.5, 
			// "latitude": 33.392209899999997, "type": "business"}
			
			/*YelpBusiness business = new YelpBusiness();
			String[] cate = {"Food", "Grocery"};
			String[] neighborhoods = {};
			
			business.setBusiness_id("usAsSV36QmUej8--yvN-dg");
			business.setCategories(cate);
			business.setCity("Phoenix");
			business.setFull_address("845 W Southern Ave\nPhoenix, AZ 85041");
			business.setLatitude(33.392209899999997);
			business.setLongitude(-112.0853773);
			business.setName("Food City");
			business.setNeighborhoods(neighborhoods);
			business.setOpen(true);
			business.setReview_count(5);
			business.setStars(3.5);
			business.setState("AZ");
			business.setType("business");
			
			yService.insertYelpBusiness(business);
			System.out.println("Done insert Yelp Business\n\n");*/
			
			
			/*yService.deleteYelpBusiness("usAsSV36QmUej8--yvN-dg");
			System.out.println("Done delete a record from MongoDB Yelp Business\n\n");*/
			
			
			/*YelpBusiness bu = yService.getYelpBusinessById("XRdwAqzuG_SwVFFlOzLmEQ");
			if (bu != null) {
				System.out.print(bu.getBusiness_id() + ": ");
				System.out.println(bu.getName());
			}*/
			
			yelpBusinesses = yService.getAllYelpBusiness();
			if (yelpBusinesses != null) {
				System.out.println("++++++++++++++++" + yelpBusinesses.size() + "++++++++++++++++\n\n");
				for (int i=0; i<yelpBusinesses.size(); i++) {
					//System.out.println(yelpBusinesses.get(i).getBusiness_id());
					//System.out.println(yelpBusinesses.get(i).getName());
					if( !businessMap.containsKey(yelpBusinesses.get(i).getBusiness_id()) ) {
						businessMap.put(yelpBusinesses.get(i).getBusiness_id(), yelpBusinesses.get(i));
					}
				}
			}
			/*yService.deleteYelpReview("CqTHqxG9xXYy0QQEW6BrKQ");
			System.out.println("Done delete a record of CqTHqxG9xXYy0QQEW6BrKQ from MongoDB Yelp Review\n\n");*/
			
			/*YelpReview re = yService.getYelpReviewById("O7jDv3ntT_rsFrd7qPrC4Q");
			if (re != null) {
				System.out.print(re.getReview_id() + ": ");
				System.out.println(re.getText());
			}*/
			
			yelpReviews = yService.getAllYelpReview();
			String city = null;
			String businessName = null;
			String category = null;
			
			if (yelpReviews != null) {
				System.out.println("++++++++++++++++" + yelpReviews.size() + "++++++++++++++++");
				
				for(int j=0; j<yelpReviews.size(); j++) {
					YelpReviewWithBInfo ybInfo = new YelpReviewWithBInfo();
					if( businessMap.containsKey(yelpReviews.get(j).getBusiness_id()) ) {
						city = businessMap.get(yelpReviews.get(j).getBusiness_id()).getCity();
						businessName = businessMap.get(yelpReviews.get(j).getBusiness_id()).getName();
						if ( businessMap.get(yelpReviews.get(j).getBusiness_id()).getCategories() != null && 
							 businessMap.get(yelpReviews.get(j).getBusiness_id()).getCategories().length > 0 ) {
							category = businessMap.get(yelpReviews.get(j).getBusiness_id()).getCategories()[0];	
						}
						else {
							category = categoryDefault;
						}
						ybInfo.setCity(city);
						ybInfo.setCategory(category);
						ybInfo.setBusinessName(businessName);
						ybInfo.setYelpReview(yelpReviews.get(j));
						yBInfos.add(ybInfo);
					}
				}
			}//end of if
			
	
			//populate with 3 maps for location, category and businessId
			obtainAggregationMaps();
			
			//aggregate 3 maps for location, cetogry and businessId. Run only one time already, comment out now
			//populateWithBusinessAggregationIntoDB();
			//populateWithLocationAggregationIntoDB();
			//populateWithCategoryAggregationIntoDB();
			//populateWithDailyTimeAggregationIntoDB();
			populateWithMonthlyTimeAggregationIntoDB();
			
			List<ReviewByBusinessAggregation> aggBusinessList = yService.getAllYelpBusinessAggregationData();
			if (aggBusinessList != null) {
				System.out.println("++++++++++++++++" + aggBusinessList.size() + "++++++++++++++++");
				System.out.println("++++++++++++++++" + aggBusinessList.get(1).getBusinessId() + ", " +
						 aggBusinessList.get(1).getBusinessName() + ", " + 
						 aggBusinessList.get(1).getUserCount() + ", " + 
						 aggBusinessList.get(1).getFeedbackLevel() + "++++++++++++++++");
			}
			
			List<ReviewByLocationAggregation> aggLocationList = yService.getAllYelpLocationAggregationData();
			if (aggLocationList != null) {
				System.out.println("++++++++++++++++" + aggLocationList.size() + "++++++++++++++++");
				System.out.println("++++++++++++++++" + aggLocationList.get(1).getCity() + ", "  +
						 aggLocationList.get(1).getUserCount() + ", " + 
						aggLocationList.get(1).getFeedbackLevel() + "++++++++++++++++");
			}
			
			List<ReviewByCategoryAggregation> aggCateList = yService.getAllYelpCategoryAggregationData();
			if (aggCateList != null) {
				System.out.println("++++++++++++++++" + aggCateList.size() + "++++++++++++++++");
				System.out.println("++++++++++++++++" + aggCateList.get(1).getBusinessCategory() + ", " +
						aggCateList.get(1).getUserCount() + ", " + 
						aggCateList.get(1).getFeedbackLevel() + "++++++++++++++++");
			}
			
		}//end of try
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	
	
	public void populateWithBusinessAggregationIntoDB() {
		for (Map.Entry<String, List<YelpReviewWithBInfo>> entry : businessAggMap.entrySet()) {
			//System.out.println("Key : " + entry.getKey() + " Value : " + entry.getValue());
			String businessId  = entry.getKey();
			List<YelpReviewWithBInfo> bInfo = entry.getValue();
			String bName = bInfo.get(0).getBusinessName();
			
			int userCount = bInfo.size();
			int stars = 0;
			int avgFeedback = 0;
			for (int p=0; p<bInfo.size(); p++) {
				stars += ( bInfo.get(p).getYelpReview().getStars() -3);
			}
			avgFeedback = stars/userCount;
			
			ReviewByBusinessAggregation bAgg = new ReviewByBusinessAggregation();
			bAgg.setBusinessId(businessId);
			bAgg.setBusinessName(bName);
			bAgg.setUserCount(userCount);
			bAgg.setFeedbackLevel(avgFeedback);
			bAgg.setTimeStamp(new Date());
			byBusinessAggregation.add(bAgg);
		}
		
		//save into Mongodb
		for (int w=0; w<byBusinessAggregation.size(); w++) {
			try {
				yService.insertYelpBusinessAggregation(byBusinessAggregation.get(w));
			} catch (AdvertisngAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	
	public void populateWithLocationAggregationIntoDB() {
		for (Map.Entry<String, List<YelpReviewWithBInfo>> entry : locationAggMap.entrySet()) {
			//System.out.println("Key : " + entry.getKey() + " Value : " + entry.getValue());
			String[] keys = new String[2];
			int i = 0;
			StringTokenizer st =  new StringTokenizer(entry.getKey(), "_");
			while (st.hasMoreElements()) {
				keys[i] = (String)st.nextElement();
				i++;
			}
			String cityId  = keys[0];
			String cate =  keys[1];
			
			List<YelpReviewWithBInfo> bInfo = entry.getValue();
			
			int userCount = bInfo.size();
			int stars = 0;
			int avgFeedback = 0;
			for (int p=0; p<bInfo.size(); p++) {
				stars += ( bInfo.get(p).getYelpReview().getStars() -3);
			}
			avgFeedback = stars/userCount;
			
			ReviewByLocationAggregation lAgg = new ReviewByLocationAggregation();
			//lAgg.setBusinessId(bInfo.get(0).getYelpReview().getBusiness_id());
			//lAgg.setBusinessName(bInfo.get(0).getBusinessName());
			lAgg.setCity(cityId);
			lAgg.setUserCount(userCount);
			lAgg.setFeedbackLevel(avgFeedback);
			lAgg.setTimeStamp(new Date());
			lAgg.setBusinessCategory(cate);
			byLocationAggregation.add(lAgg);
		}
		
		//save into Mongodb
		for (int v=0; v<byLocationAggregation.size(); v++) {
			try {
				yService.insertYelpLocationAggregation(byLocationAggregation.get(v));
			} catch (AdvertisngAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	
	public void populateWithCategoryAggregationIntoDB() {
		for (Map.Entry<String, List<YelpReviewWithBInfo>> entry : categoryAggMap.entrySet()) {
			//System.out.println("Key : " + entry.getKey() + " Value : " + entry.getValue());
			String businessCategory  = entry.getKey();
			List<YelpReviewWithBInfo> bInfo = entry.getValue();
			
			int userCount = bInfo.size();
			int stars = 0;
			int avgFeedback = 0;
			for (int p=0; p<bInfo.size(); p++) {
				stars += ( bInfo.get(p).getYelpReview().getStars() -3);
			}
			avgFeedback = stars/userCount;
			
			ReviewByCategoryAggregation cAgg = new ReviewByCategoryAggregation();
			//cAgg.setBusinessId(businessId);
			//cAgg.setBusinessName(bInfo.get(0).getBusinessName());
			cAgg.setUserCount(userCount);
			cAgg.setFeedbackLevel(avgFeedback);
			cAgg.setTimeStamp(new Date());
			cAgg.setBusinessCategory(businessCategory);
			byCategoryAggregation.add(cAgg);
		}
		
		//save into Mongodb
		for (int u=0; u<byCategoryAggregation.size(); u++) {
			try {
				yService.insertYelpCategoryAggregation(byCategoryAggregation.get(u));
			} catch (AdvertisngAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	


	public void populateWithMonthlyTimeAggregationIntoDB() {
		for (Map.Entry<String, List<YelpReviewWithBInfo>> entry : monthlyTimeAggMap.entrySet()) {
			//System.out.println("Key : " + entry.getKey() + " Value : " + entry.getValue());
			String[] keys = new String[2];
			int i = 0;
			StringTokenizer st =  new StringTokenizer(entry.getKey(), "_");
			while (st.hasMoreElements()) {
				keys[i] = (String)st.nextElement();
				i++;
			}
			String montlyTime  = keys[0];
			String cate =  keys[1];
			
			List<YelpReviewWithBInfo> bInfo = entry.getValue();
			
			int userCount = bInfo.size();
			int stars = 0;
			int avgFeedback = 0;
			for (int p=0; p<bInfo.size(); p++) {
				stars += ( bInfo.get(p).getYelpReview().getStars() -3);
			}
			avgFeedback = stars/userCount;
			
			ReviewByMonthlyTimeAggregation mAgg = new ReviewByMonthlyTimeAggregation();
			mAgg.setMonthlyTime(montlyTime);
			mAgg.setUserCount(userCount);
			mAgg.setFeedbackLevel(avgFeedback);
			mAgg.setTimeStamp(new Date());
			mAgg.setBusinessCategory(cate);
			byMonthlyTimeAggregation.add(mAgg);
		}
		
		//save into Mongodb
		for (int v=0; v<byMonthlyTimeAggregation.size(); v++) {
			try {
				yService.insertYelpMonthlyTimeAggregation(byMonthlyTimeAggregation.get(v));
			} catch (AdvertisngAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	
	public void populateWithDailyTimeAggregationIntoDB() {
		for (Map.Entry<String, List<YelpReviewWithBInfo>> entry : dailyTimeAggMap.entrySet()) {
			//System.out.println("Key : " + entry.getKey() + " Value : " + entry.getValue());
			String[] keys = new String[2];
			int i = 0;
			StringTokenizer st =  new StringTokenizer(entry.getKey(), "_");
			while (st.hasMoreElements()) {
				keys[i] = (String)st.nextElement();
				i++;
			}
			String dailyTime  = keys[0];
			String cate =  keys[1];
			
			List<YelpReviewWithBInfo> bInfo = entry.getValue();
			
			int userCount = bInfo.size();
			int stars = 0;
			int avgFeedback = 0;
			for (int p=0; p<bInfo.size(); p++) {
				stars += ( bInfo.get(p).getYelpReview().getStars() -3);
			}
			avgFeedback = stars/userCount;
			
			ReviewByDailyTimeAggregation dAgg = new ReviewByDailyTimeAggregation();
			dAgg.setDailyTime(dailyTime);
			dAgg.setUserCount(userCount);
			dAgg.setFeedbackLevel(avgFeedback);
			dAgg.setTimeStamp(new Date());
			dAgg.setBusinessCategory(cate);
			byDailyTimeAggregation.add(dAgg);
		}
		
		//save into Mongodb
		for (int v=0; v<byDailyTimeAggregation.size(); v++) {
			try {
				yService.insertYelpDailyTimeAggregation(byDailyTimeAggregation.get(v));
			} catch (AdvertisngAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	
	
	public void obtainAggregationMaps() {
		if (yBInfos.size() > 0) {
			for (int m=0; m<yBInfos.size(); m++) {
				
				/*if( ! businessAggMap.containsKey(yBInfos.get(m).getYelpReview().getBusiness_id())) {
					List<YelpReviewWithBInfo> ybInfoBList = new ArrayList<YelpReviewWithBInfo>();
					YelpReviewWithBInfo  ybInfoElm = yBInfos.get(m);
					ybInfoBList.add(ybInfoElm);
					businessAggMap.put(yBInfos.get(m).getYelpReview().getBusiness_id(), ybInfoBList);
				}
				else {
					List<YelpReviewWithBInfo> ybBInfoList =  businessAggMap.get(yBInfos.get(m).getYelpReview().getBusiness_id());
					YelpReviewWithBInfo  ybInfoElm = yBInfos.get(m);
					ybBInfoList.add(ybInfoElm);
					businessAggMap.put(yBInfos.get(m).getYelpReview().getBusiness_id(), ybBInfoList);
				}*/
				
			
				/*if( ! categoryAggMap.containsKey(yBInfos.get(m).getCategory()) ) {
					List<YelpReviewWithBInfo> ybInfCList = new ArrayList<YelpReviewWithBInfo>();
					YelpReviewWithBInfo  ybInfoElm = yBInfos.get(m);
					ybInfCList.add(ybInfoElm);
					categoryAggMap.put(yBInfos.get(m).getCategory(), ybInfCList);
				}
				else {
					List<YelpReviewWithBInfo> ybInfCList =  categoryAggMap.get(yBInfos.get(m).getCategory());
					YelpReviewWithBInfo  ybInfoElm = yBInfos.get(m);
					ybInfCList.add(ybInfoElm);
					categoryAggMap.put(yBInfos.get(m).getCity(), ybInfCList);
				}*/
				
				/*String lcKey = yBInfos.get(m).getCity().trim() + "_" + yBInfos.get(m).getCategory();
				if( ! locationAggMap.containsKey(lcKey) ) {
					List<YelpReviewWithBInfo> ybInfLList = new ArrayList<YelpReviewWithBInfo>();
					YelpReviewWithBInfo  ybInfoElm = yBInfos.get(m);
					ybInfLList.add(ybInfoElm);
					locationAggMap.put(lcKey, ybInfLList);
				}
				else {
					List<YelpReviewWithBInfo> ybInfLList =  locationAggMap.get(lcKey);
					YelpReviewWithBInfo  ybInfoElm = yBInfos.get(m);
					ybInfLList.add(ybInfoElm);
					locationAggMap.put(lcKey, ybInfLList);
				}*/
			
				
				/*String dtcKey = yBInfos.get(m).getYelpReview().getDate().trim() + "_" + yBInfos.get(m).getCategory();
				if( ! dailyTimeAggMap.containsKey(dtcKey) ) {
					List<YelpReviewWithBInfo> ybInfLList = new ArrayList<YelpReviewWithBInfo>();
					YelpReviewWithBInfo  ybInfoElm = yBInfos.get(m);
					ybInfLList.add(ybInfoElm);
					dailyTimeAggMap.put(dtcKey, ybInfLList);
				}
				else {
					List<YelpReviewWithBInfo> ybInfLList =  dailyTimeAggMap.get(dtcKey);
					YelpReviewWithBInfo  ybInfoElm = yBInfos.get(m);
					ybInfLList.add(ybInfoElm);
					dailyTimeAggMap.put(dtcKey, ybInfLList);
				}*/
				
				
				String mtcKey = yBInfos.get(m).getYelpReview().getDate().substring(0,  7).trim() + "_" + yBInfos.get(m).getCategory();
				if( ! monthlyTimeAggMap.containsKey(mtcKey) ) {
					List<YelpReviewWithBInfo> ybInfLList = new ArrayList<YelpReviewWithBInfo>();
					YelpReviewWithBInfo  ybInfoElm = yBInfos.get(m);
					ybInfLList.add(ybInfoElm);
					monthlyTimeAggMap.put(mtcKey, ybInfLList);
				}
				else {
					List<YelpReviewWithBInfo> ybInfLList =  monthlyTimeAggMap.get(mtcKey);
					YelpReviewWithBInfo  ybInfoElm = yBInfos.get(m);
					ybInfLList.add(ybInfoElm);
					monthlyTimeAggMap.put(mtcKey, ybInfLList);
				}
			
			}
		}//end of if
	}
}
