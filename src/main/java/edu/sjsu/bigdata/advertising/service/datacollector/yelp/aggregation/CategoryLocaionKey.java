/**
 * 
 */
package edu.sjsu.bigdata.advertising.service.datacollector.yelp.aggregation;

import java.io.Serializable;

/**
 * @author yunxue
 *
 */
public class CategoryLocaionKey implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String city;
	private String category;
	
	
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	
	
	public String getCategoryLocaionKey() {
		
		return this.category.trim() + "_" + this.city.trim();
	}
	

}
