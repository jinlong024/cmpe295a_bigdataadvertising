/**
 * 
 */
package edu.sjsu.bigdata.advertising.service.datacollector.yelp;

import java.io.IOException;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author yunxue
 *
 */
public class JsonUtil {
	
	public static String buildJsonInput(Object object) {
		String json = null;
		ObjectMapper mapper = new ObjectMapper();
		try {
			json = mapper.writeValueAsString(object);
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return json;

	}
	
	
	public static Object processYelpJson(String json, String className,	String packageName) {
		Object object = null;
		ObjectMapper mapper = new ObjectMapper();

		className = packageName + "." + className;
		json = json.trim();

		try {
			if (json.length() != 0) {
				object = mapper.readValue(json, Class.forName(className));
			}
			else {}
		} 
		catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		} 
		catch (JsonParseException e) {
			e.printStackTrace();
		} 
		catch (JsonMappingException e) {
			e.printStackTrace();
		} 
		catch (IOException e) {
			e.printStackTrace();
		}

		return object;
	}

}
