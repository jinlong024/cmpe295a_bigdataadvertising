package edu.sjsu.bigdata.advertising.service.impl.datacollector.common;

import edu.sjsu.bigdata.advertising.dao.interfaces.datacollection.FeedbackDao;
import edu.sjsu.bigdata.advertising.model.feedbackAggregation.Feedback;
import edu.sjsu.bigdata.advertising.service.interfaces.datacollection.FeedbackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * This is the advertisement preference profile service implementation. User:
 * lei zhang Date: 13-8-13
 */
@Service
public class FeedbackServiceImpl implements FeedbackService {

	@Autowired
	private FeedbackDao feedbackDao;

	/**
	 * This method is used to insert or update Advertisement Preference Profile
	 * and return its ID.
	 * 
	 * @param entity
	 *            : This is the advertisement preference profile to be saved.
	 * @return Return null if the operation is failed; Return the profile ID if
	 *         the operation is successful.
	 */

	public Feedback save(Feedback entity) {
	
		return feedbackDao.saveFeedback(entity);
	}
	
	public Feedback update(Feedback entity) {
		return feedbackDao.updateFeedback(entity);
	}

	/**
	 * This method is used to delete Advertisement Preference Profile by ID and
	 * return the result if the it has been deleted.
	 * 
	 * @param id
	 *            : This is the advertisement preference profile ID.
	 * @return Return false if the operation is failed; Return true if the
	 *         operation is successful.

	 */
	public boolean delete(String id) {

		return feedbackDao.deleteFeedback(id);
	}

	/**
	 * This method is used to get Advertisement Preference Profile by ID.
	 * 
	 * @param id
	 *            : This is the advertisement preference profile ID.
	 * @return Return NULL if the operation is failed; Return the advertisement
	 *         preference profile if the operation is successful.
	 */
	public Feedback findById(String id) {
		return feedbackDao.findFeedbackById(id);
	}

	/**
	 * This method is used to get list of existing Advertisement Preference
	 * Profiles.
	 * 
	 * @return Return NULL if the operation is failed; Return the list of
	 *         existing advertisement preference profiles if the operation is
	 *         successful.
	 */
	public List<Feedback> findAll() {
		return feedbackDao.findAllFeedback();
	}
}
