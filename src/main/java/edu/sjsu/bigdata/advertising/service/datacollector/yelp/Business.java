/**
 * 
 */
package edu.sjsu.bigdata.advertising.service.datacollector.yelp;

import java.io.Serializable;

/**
 * @author yunxue
 *
 */
public class Business  implements Serializable  {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String name;
	public String mobileUrl;
	
	public Double rating;
	public Integer reviewCount;
	
	public String url;
	public String phone;
	
	public String snippetText;
	public String location;
	public Address address;
	
	public Business(String name, String mobileUrl, Double rating, Integer reviewCount, String url, 
					String phone, String snippetText, Address address) {
		this.name = name;
		this.url = url;
		this.mobileUrl = mobileUrl;
		this.rating = rating;
		this.reviewCount = reviewCount;
		this.phone = phone;
		this.snippetText = snippetText;
		this.address = address;
		
	}

	
	public Business(String name, String mobileUrl, Double rating, Integer reviewCount, String url, 
					String phone, String snippetText, String location) {
		this.name = name;
		this.url = url;
		this.mobileUrl = mobileUrl;
		this.rating = rating;
		this.reviewCount = reviewCount;
		this.phone = phone;
		this.snippetText = snippetText;
		this.location = location;
		
	}
	
	
	@Override
	public String toString() {
		//return name + "\n" + url + "\n" + rating + "\n" +  reviewCount + "\n" + phone + "\n" + snippetText + "\n" + location;
		return name + "\n" + url + "\n" + rating + "\n" +  reviewCount + "\n" + phone + "\n" + snippetText + "\n" + address.displayAddress;
	}

}
