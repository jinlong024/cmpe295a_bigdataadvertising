/**
 * 
 */
package edu.sjsu.bigdata.advertising.service.datacollector.yelp;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author yunxue
 *
 */
public class YelpReader {

	public final static String PATH = "/Yun_Csco/SJSUCourses/Fall2013/CMPE295B/YelpAcademicDataset";
	public final static String JSON_FILENAME = "yelpData.json"; 
	//public final static String CSV_FILENAME = "yelpData.csv"; 
	public final static String CSV_FILENAME = "yelpData-oct30.csv"; 
	
	
	public final static String BUSINESS_JSON_FILENAME = "yelpBusinessData.json"; 

	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String cleanStr = null;
		
		try {
			//read from json data file
			List<YelpReview> yelpDataList = JsonReaderUtil.read(PATH + "/" + JSON_FILENAME);
			
			List<Review> reviews = new ArrayList<Review>();
			
			long reviewCount = yelpDataList.size();
			System.out.println("+++++++++++++++++++++Review Counts: " + reviewCount + "++++++++++++++++++++++++\n");
			
			//wrtie selected businessId data into a CSV file
			FileWriter writer = new FileWriter(PATH + "/" + CSV_FILENAME );
			writer.append("Ranking");
		    writer.append(',');
		    writer.append("Review");
		    writer.append('\n');
		    
			for (int i=0; i<reviewCount; i++) {
				/*System.out.println(yelpDataList.get(i).getVotes().getFunny());
				System.out.println(yelpDataList.get(i).getVotes().getUseful());
				System.out.println(yelpDataList.get(i).getVotes().getCool());
				System.out.println(yelpDataList.get(i).getUser_id());
				System.out.println(yelpDataList.get(i).getReview_id());
				System.out.println(yelpDataList.get(i).getStars());
				System.out.println(yelpDataList.get(i).getDate());
				System.out.println(yelpDataList.get(i).getType());
				System.out.println(yelpDataList.get(i).getBusiness_id());*/
				
				Review review = new Review();
				cleanStr = yelpDataList.get(i).getText();
				cleanStr = cleanStr.replace("\n", " ");
				
				cleanStr = "\"" + cleanStr + "\""; 
				
				review.setText(cleanStr);
				review.setRanking(yelpDataList.get(i).getStars());
				reviews.add(review);
				
				System.out.println(review.getRanking());
				System.out.println(review.getText());
				System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
				
				writer.append(review.getRanking()+"");
				writer.append(',');
			    writer.append(review.getText());
			    writer.append('\n');
		
			    
			}
	
		    writer.flush();
		    writer.close();
		}
		catch(IOException e) {
		     e.printStackTrace();
		} 
	}

}
