/**
 * 
 */
package edu.sjsu.bigdata.advertising.service.datacollector.yelp;

import edu.sjsu.bigdata.advertising.exception.dao.AdvertisngAccessException;
import edu.sjsu.bigdata.advertising.service.datacollector.yelp.aggregation.ReviewByBusinessAggregation;
import edu.sjsu.bigdata.advertising.service.datacollector.yelp.aggregation.ReviewByCategoryAggregation;
import edu.sjsu.bigdata.advertising.service.datacollector.yelp.aggregation.ReviewByDailyTimeAggregation;
import edu.sjsu.bigdata.advertising.service.datacollector.yelp.aggregation.ReviewByLocationAggregation;
import edu.sjsu.bigdata.advertising.service.datacollector.yelp.aggregation.ReviewByMonthlyTimeAggregation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author yunxue
 *
 */
@Repository
public class YelpDataDaoImpl implements IYelpDataDao {

	@Autowired
	private MongoTemplate mongoTemplate;
	
	public MongoTemplate getMongoTemplate() {
		return mongoTemplate;
	}

	public void setMongoTemplate(MongoTemplate mongoTemplate) {
		this.mongoTemplate = mongoTemplate;
	}
	
	
	@Override
	public YelpBusiness getYelpBusinessById(String bId) throws AdvertisngAccessException {
		Query query = new Query(Criteria.where("business_id").is(bId));
		
		//YelpBusiness yelpBusiness = mongoTemplate.findById(bId, YelpBusiness.class);
		
		YelpBusiness yelpBusiness =  mongoTemplate.findOne(query, YelpBusiness.class);
		return yelpBusiness;
	}

	
	
	@Override
	public void insertYelpBusiness(YelpBusiness business) throws AdvertisngAccessException {
		try {
			if (!mongoTemplate.collectionExists(YelpBusiness.class)) {
				mongoTemplate.createCollection(YelpBusiness.class);
			}	
			mongoTemplate.insert(business);
			
			/*if (!mongoTemplate.collectionExists("yelpBusinesses")) {
				mongoTemplate.createCollection("yelpBusinesses");
			}
			mongoTemplate.insert(business, "yelpBusinesses");*/
			
		}
		catch (DataAccessException e) {
			throw new AdvertisngAccessException(e);
		}
	}

	
	
	@Override
	public List<YelpBusiness> getAllYelpBusiness() throws AdvertisngAccessException {
		List<YelpBusiness> businesses = null;
		try {
			businesses = (List<YelpBusiness>) mongoTemplate.findAll(YelpBusiness.class);
		}
		catch (DataAccessException e) {
			throw new AdvertisngAccessException(e);
		}
		return businesses;
	}
	
	

	@Override
	public void deleteYelpBusiness(String bId) throws AdvertisngAccessException {
		try {
			Query query = new Query(Criteria.where("business_id").is(bId));
			mongoTemplate.remove(query, YelpBusiness.class);
			//////mongoTemplate.remove(query, "yelpBusinesses");
		}
		catch (DataAccessException e) {
			throw new AdvertisngAccessException(e);
		}
		
	}

	@Override
	public YelpReview getYelpReviewById(String rId) throws AdvertisngAccessException {
		Query query = new Query(Criteria.where("review_id").is(rId));
		YelpReview yelpreview =  mongoTemplate.findOne(query, YelpReview.class);
		return yelpreview;
	}
	

	@Override
	public void insertYelpReview(YelpReview review) throws AdvertisngAccessException {
		try {
			if (!mongoTemplate.collectionExists(YelpReview.class)) {
				mongoTemplate.createCollection(YelpReview.class);
			}	
			mongoTemplate.insert(review);
		}
		catch (DataAccessException e) {
			throw new AdvertisngAccessException(e);
		}
		
	}

	
	@Override
	public List<YelpReview> getAllYelpReview() throws AdvertisngAccessException {
		List<YelpReview> reviews = null;
		try {
			reviews = (List<YelpReview>) mongoTemplate.findAll(YelpReview.class);
		}
		catch (DataAccessException e) {
			throw new AdvertisngAccessException(e);
		}
		return reviews;
	}

	
	@Override
	public void deleteYelpReview(String rId) throws AdvertisngAccessException {
		try {
			Query query = new Query(Criteria.where("review_id").is(rId));
			mongoTemplate.remove(query, YelpReview.class);
		}
		catch (DataAccessException e) {
			throw new AdvertisngAccessException(e);
		}
		
	}

	
	@Override
	public void insertYelpBusinessAggregation(ReviewByBusinessAggregation bAgg) throws AdvertisngAccessException {
		try {
			if (!mongoTemplate.collectionExists(ReviewByBusinessAggregation.class)) {
				mongoTemplate.createCollection(ReviewByBusinessAggregation.class);
			}	
			mongoTemplate.insert(bAgg);
		}
		catch (DataAccessException e) {
			throw new AdvertisngAccessException(e);
		}
	}
	
	
	@Override
	public void insertYelpLocationAggregation(ReviewByLocationAggregation lAgg) throws AdvertisngAccessException {
		try {
			if (!mongoTemplate.collectionExists(ReviewByLocationAggregation.class)) {
				mongoTemplate.createCollection(ReviewByLocationAggregation.class);
			}	
			mongoTemplate.insert(lAgg);
		}
		catch (DataAccessException e) {
			throw new AdvertisngAccessException(e);
		}
	}
	
	
	@Override
	public void insertYelpCategoryAggregation(ReviewByCategoryAggregation cAgg) throws AdvertisngAccessException {
		try {
			if (!mongoTemplate.collectionExists(ReviewByCategoryAggregation.class)) {
				mongoTemplate.createCollection(ReviewByCategoryAggregation.class);
			}	
			mongoTemplate.insert(cAgg);
		}
		catch (DataAccessException e) {
			throw new AdvertisngAccessException(e);
		}
	}

	@Override
	public List<ReviewByBusinessAggregation> getAllYelpBusinessAggregationData() throws AdvertisngAccessException {
		// TODO Auto-generated method stub
		List<ReviewByBusinessAggregation> bAggs = null;
		try {
			bAggs = (List<ReviewByBusinessAggregation>) mongoTemplate.findAll(ReviewByBusinessAggregation.class);
		}
		catch (DataAccessException e) {
			throw new AdvertisngAccessException(e);
		}
		return bAggs;
	}

	@Override
	public List<ReviewByLocationAggregation> getAllYelpLocationAggregationData() throws AdvertisngAccessException{
		List<ReviewByLocationAggregation> lAggs = null;
		try {
			lAggs = (List<ReviewByLocationAggregation>) mongoTemplate.findAll(ReviewByLocationAggregation.class);
		}
		catch (DataAccessException e) {
			throw new AdvertisngAccessException(e);
		}
		return lAggs;
	}

	@Override
	public List<ReviewByCategoryAggregation> getAllYelpCategoryAggregationData() throws AdvertisngAccessException {
		List<ReviewByCategoryAggregation> cAggs = null;
		try {
			cAggs = (List<ReviewByCategoryAggregation>) mongoTemplate.findAll(ReviewByCategoryAggregation.class);
		}
		catch (DataAccessException e) {
			throw new AdvertisngAccessException(e);
		}
		return cAggs;
	}

	
	@Override
	public void insertYelpDailyTimeAggregation(ReviewByDailyTimeAggregation dAgg) throws AdvertisngAccessException {
		try {
			if (!mongoTemplate.collectionExists(ReviewByDailyTimeAggregation.class)) {
				mongoTemplate.createCollection(ReviewByDailyTimeAggregation.class);
			}	
			mongoTemplate.insert(dAgg);
		}
		catch (DataAccessException e) {
			throw new AdvertisngAccessException(e);
		}	
	}

	@Override
	public void insertYelpMonthlyTimeAggregation(ReviewByMonthlyTimeAggregation mAgg) throws AdvertisngAccessException {
		try {
			if (!mongoTemplate.collectionExists(ReviewByMonthlyTimeAggregation.class)) {
				mongoTemplate.createCollection(ReviewByMonthlyTimeAggregation.class);
			}	
			mongoTemplate.insert(mAgg);
		}
		catch (DataAccessException e) {
			throw new AdvertisngAccessException(e);
		}
	}

	@Override
	public List<ReviewByDailyTimeAggregation> getAllYelpDailyTimeAggregationData() throws AdvertisngAccessException {
		List<ReviewByDailyTimeAggregation> dAggs = null;
		try {
			dAggs = (List<ReviewByDailyTimeAggregation>) mongoTemplate.findAll(ReviewByDailyTimeAggregation.class);
		}
		catch (DataAccessException e) {
			throw new AdvertisngAccessException(e);
		}
		return dAggs;
	}

	@Override
	public List<ReviewByMonthlyTimeAggregation> getAllYelpMonlyTimeAggregationData() throws AdvertisngAccessException {
		List<ReviewByMonthlyTimeAggregation> mAggs = null;
		try {
			mAggs = (List<ReviewByMonthlyTimeAggregation>) mongoTemplate.findAll(ReviewByMonthlyTimeAggregation.class);
		}
		catch (DataAccessException e) {
			throw new AdvertisngAccessException(e);
		}
		return mAggs;
	}

	@Override
	public ReviewByCategoryAggregation findCategoryAggregationByName(String category) throws AdvertisngAccessException {
		// query to search category
		Query query = new Query(Criteria.where("businessCategory").is(category));
		ReviewByCategoryAggregation cateAgg = mongoTemplate.findOne(query, ReviewByCategoryAggregation.class);
		return cateAgg;
	}

	@Override
	public ReviewByLocationAggregation findLocationAggregationByName(String category, String city) throws AdvertisngAccessException {
		Query query = new Query(Criteria.where("businessCategory").is(category).and ("city").is(city));
		ReviewByLocationAggregation cityAgg = mongoTemplate.findOne(query, ReviewByLocationAggregation.class);
		return cityAgg;
	}

	@Override
	public ReviewByBusinessAggregation findBusinessAggregationByName(String bname) throws AdvertisngAccessException {
		Query query = new Query(Criteria.where("businessName").is(bname));
		ReviewByBusinessAggregation bAgg = mongoTemplate.findOne(query, ReviewByBusinessAggregation.class);
		return bAgg;
	}

	/*@Override
	public ReviewByDailyTimeAggregation findDailyTimeAggregationByValue(String category, String date) throws AdvertisngAccessException {
		Query query = new Query(Criteria.where("businessCategory").is(category).and ("dailyTime").is(date));
		ReviewByDailyTimeAggregation dAgg = mongoTemplate.findOne(query, ReviewByDailyTimeAggregation.class);
		return dAgg;
	}

	@Override
	public ReviewByMonthlyTimeAggregation findMonthlyTimeAggregationByValue(String category, String month) throws AdvertisngAccessException {
		Query query = new Query(Criteria.where("businessCategory").is(category).and ("monthlyTime").is(month));
		ReviewByMonthlyTimeAggregation mAgg = mongoTemplate.findOne(query, ReviewByMonthlyTimeAggregation.class);
		return mAgg;
	}*/

	
	
	@Override
	public List<ReviewByDailyTimeAggregation> findDailyTimeAggregationByValue(
			String startDate, String endDate) throws AdvertisngAccessException {
		Query query = new Query(Criteria.where("dailyTime").gte(startDate).lte(endDate));
		List<ReviewByDailyTimeAggregation> dAggs = mongoTemplate.find(query, ReviewByDailyTimeAggregation.class);
		return dAggs;
	}

	@Override
	public List<ReviewByLocationAggregation> findLocationAggregationByName(String city)
			throws AdvertisngAccessException {
		Query query = new Query(Criteria.where("city").is(city));
		List<ReviewByLocationAggregation> cAggs = mongoTemplate.find(query, ReviewByLocationAggregation.class);
		return cAggs;
	}

	@Override
	public List<ReviewByMonthlyTimeAggregation> findMonthlyTimeAggregationByValue(
			String startMonth, String endMonth) throws AdvertisngAccessException {
		//Query query = new Query(Criteria.where("monthlyTime").is(month));
		Query query = new Query(Criteria.where("monthlyTime").gte(startMonth).lte(endMonth));
		List<ReviewByMonthlyTimeAggregation> mAggs = mongoTemplate.find(query, ReviewByMonthlyTimeAggregation.class);
		return mAggs;
	}
}
