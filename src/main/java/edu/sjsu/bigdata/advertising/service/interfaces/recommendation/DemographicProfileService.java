package edu.sjsu.bigdata.advertising.service.interfaces.recommendation;

import edu.sjsu.bigdata.advertising.model.recommendation.DemographicProfile;

import java.util.List;

/**
 * This is the advertisement preference profile service interface.
 * User: Lei Zhang
 * Date: August 13, 2013
 */
public interface DemographicProfileService {

    /**
     * This method is used to insert or update Advertisement Preference Profile and return its ID.
     *
     * @param entity: This is the advertisement preference profile to be saved.
     * @return Return null if the operation is failed;
     *         Return the profile ID if the operation is successful.
     *
     */
    public DemographicProfile saveProfile(DemographicProfile entity);
    
    public DemographicProfile updateProfile(DemographicProfile entity);

    /**
     * This method is used to delete Advertisement Preference Profile by ID and return the result
     * if the it has been deleted.
     *
     * @param id: This is the advertisement preference profile ID.
     * @return Return false if the operation is failed;
     *         Return true if the operation is successful.
     *
     */
    public boolean deleteProfile(String id);

    /**
     * This method is used to get Advertisement Preference Profile by ID.
     *
     * @param id: This is the advertisement preference profile ID.
     * @return Return NULL if the operation is failed;
     *         Return the advertisement preference profile if the operation is successful.
     *
     */
    public DemographicProfile findProfileById(String id);

    /**
     * This method is used to get list of existing Advertisement Preference Profiles.
     *
     * @return Return NULL if the operation is failed;
     *         Return the list of existing advertisement preference profiles if the operation is successful.
     *
     */
    public List<DemographicProfile> findAllProfile();
}
