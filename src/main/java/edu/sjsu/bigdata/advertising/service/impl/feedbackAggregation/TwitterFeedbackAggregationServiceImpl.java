package edu.sjsu.bigdata.advertising.service.impl.feedbackAggregation;

import edu.sjsu.bigdata.advertising.dao.interfaces.datacollection.FeedbackDao;
import edu.sjsu.bigdata.advertising.dao.interfaces.feedbackAggregation.DailyFeedbackAggregationDao;
import edu.sjsu.bigdata.advertising.dao.interfaces.feedbackAggregation.HourlyFeedbackAggregationDao;
import edu.sjsu.bigdata.advertising.dao.interfaces.feedbackAggregation.MonthlyFeedbackAggregationDao;
import edu.sjsu.bigdata.advertising.model.feedbackAggregation.DailyFeedbackAggregation;
import edu.sjsu.bigdata.advertising.model.feedbackAggregation.Feedback;
import edu.sjsu.bigdata.advertising.model.feedbackAggregation.FeedbackLevelDistribution;
import edu.sjsu.bigdata.advertising.model.feedbackAggregation.HourlyFeedbackAggregation;
import edu.sjsu.bigdata.advertising.model.feedbackAggregation.MonthlyFeedbackAggregation;
import edu.sjsu.bigdata.advertising.service.interfaces.feedbackAggregation.FeedbackAggregationService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * The Twitter API (API 1.1) Application.
 * 
 * http://www.java-tutorial.ch/framework/twitter-with-java-tutorial
 */
@Service
public class TwitterFeedbackAggregationServiceImpl implements
		FeedbackAggregationService {

	@Autowired
	private MonthlyFeedbackAggregationDao monthlyFeedbackAggregationDao;

	@Autowired
	private DailyFeedbackAggregationDao dailyFeedbackAggregationDao;

	@Autowired
	private HourlyFeedbackAggregationDao hourlyFeedbackAggregationDao;
	
	@Autowired
	private FeedbackDao feedbackDao;

	public List<MonthlyFeedbackAggregation> findAllMonthlyFeedbackAggregation() {
		return monthlyFeedbackAggregationDao
				.findAllMonthlyFeedbackAggregation();
	}

	public List<MonthlyFeedbackAggregation> findMonthlyFeedbackAggregationByCategory(
			String category) {
		return monthlyFeedbackAggregationDao
				.findMonthlyFeedbackAggregationByCategory(category);
	}

	public List<DailyFeedbackAggregation> findAllDailyFeedbackAggregation() {
		return dailyFeedbackAggregationDao.findAllDailyFeedbackAggregation();
	}

	public List<DailyFeedbackAggregation> findDailyFeedbackAggregationByCategory(
			String category) {
		return dailyFeedbackAggregationDao
				.findDailyFeedbackAggregationByCategory(category);
	}

	public List<HourlyFeedbackAggregation> findAllHourlyFeedbackAggregation() {
		return hourlyFeedbackAggregationDao.findAllHourlyFeedbackAggregation();
	}

	public List<HourlyFeedbackAggregation> findHourlyFeedbackAggregationByCategory(
			String category) {
		return hourlyFeedbackAggregationDao
				.findHourlyFeedbackAggregationByCategory(category);
	}

	public void aggregationHourlyFeedback(String fromTime, String toTime) {
		List<Feedback> stronglyDislikeList = feedbackDao.findFeedbackByLevel("STRONGLY_DISLIKE");
		List<Feedback> dislikeList = feedbackDao.findFeedbackByLevel("DISLIKE");
		List<Feedback> neutralList = feedbackDao.findFeedbackByLevel("NEUTRAL");
		List<Feedback> likeList = feedbackDao.findFeedbackByLevel("LIKE");
		List<Feedback> stronglylikeList = feedbackDao.findFeedbackByLevel("STRONGLY_LIKE");
		
		Feedback feedback = neutralList.get(0);
		HourlyFeedbackAggregation feedbackAggregation = new HourlyFeedbackAggregation();
		feedbackAggregation.setProductCategory(feedback.getProductCategory());
		feedbackAggregation.setProductManufacturer(feedback.getProductManufacturer());
		feedbackAggregation.setProductModel(feedback.getProductModel());
		feedbackAggregation.setProductName(feedback.getProductName());
		feedbackAggregation.setTimeStamp(fromTime);
		
		FeedbackLevelDistribution levelDistribution = new FeedbackLevelDistribution();
		
		levelDistribution.setStronglyDislikeCount(stronglyDislikeList.size());
		levelDistribution.setDislikeCount(dislikeList.size());
		levelDistribution.setNeutralCount(neutralList.size());
		levelDistribution.setLikeCount(likeList.size());
		levelDistribution.setStronglyLikeCount(stronglylikeList.size());
		
		feedbackAggregation.setFeedbackLevel(levelDistribution);
		
		hourlyFeedbackAggregationDao.saveHourlyFeedbackAggregation(feedbackAggregation);
	}

	public void aggregationDailyFeedback(String fromTime, String toTime) {
		
		List<Feedback> stronglyDislikeList = feedbackDao.findFeedbackByLevel("STRONGLY_DISLIKE");
		List<Feedback> dislikeList = feedbackDao.findFeedbackByLevel("DISLIKE");
		List<Feedback> neutralList = feedbackDao.findFeedbackByLevel("NEUTRAL");
		List<Feedback> likeList = feedbackDao.findFeedbackByLevel("LIKE");
		List<Feedback> stronglylikeList = feedbackDao.findFeedbackByLevel("STRONGLY_LIKE");
		
		Feedback feedback = neutralList.get(0);
		DailyFeedbackAggregation feedbackAggregation = new DailyFeedbackAggregation();
		feedbackAggregation.setProductCategory(feedback.getProductCategory());
		feedbackAggregation.setProductManufacturer(feedback.getProductManufacturer());
		feedbackAggregation.setProductModel(feedback.getProductModel());
		feedbackAggregation.setProductName(feedback.getProductName());
		feedbackAggregation.setTimeStamp(fromTime);
		
		FeedbackLevelDistribution levelDistribution = new FeedbackLevelDistribution();
		
		levelDistribution.setStronglyDislikeCount(stronglyDislikeList.size());
		levelDistribution.setDislikeCount(dislikeList.size());
		levelDistribution.setNeutralCount(neutralList.size());
		levelDistribution.setLikeCount(likeList.size());
		levelDistribution.setStronglyLikeCount(stronglylikeList.size());
		
		feedbackAggregation.setFeedbackLevel(levelDistribution);
		
		dailyFeedbackAggregationDao.saveDailyFeedbackAggregation(feedbackAggregation);

	}

	public void aggregationMonthlyFeedback(String fromTime, String toTime) {

		List<Feedback> stronglyDislikeList = feedbackDao.findFeedbackByLevel("STRONGLY_DISLIKE");
		List<Feedback> dislikeList = feedbackDao.findFeedbackByLevel("DISLIKE");
		List<Feedback> neutralList = feedbackDao.findFeedbackByLevel("NEUTRAL");
		List<Feedback> likeList = feedbackDao.findFeedbackByLevel("LIKE");
		List<Feedback> stronglylikeList = feedbackDao.findFeedbackByLevel("STRONGLY_LIKE");
		
		Feedback feedback = neutralList.get(0);
		MonthlyFeedbackAggregation feedbackAggregation = new MonthlyFeedbackAggregation();
		feedbackAggregation.setProductCategory(feedback.getProductCategory());
		feedbackAggregation.setProductManufacturer(feedback.getProductManufacturer());
		feedbackAggregation.setProductModel(feedback.getProductModel());
		feedbackAggregation.setProductName(feedback.getProductName());
		feedbackAggregation.setTimeStamp(fromTime);
		
		FeedbackLevelDistribution levelDistribution = new FeedbackLevelDistribution();
		
		levelDistribution.setStronglyDislikeCount(stronglyDislikeList.size());
		levelDistribution.setDislikeCount(dislikeList.size());
		levelDistribution.setNeutralCount(neutralList.size());
		levelDistribution.setLikeCount(likeList.size());
		levelDistribution.setStronglyLikeCount(stronglylikeList.size());
		
		feedbackAggregation.setFeedbackLevel(levelDistribution);
		
		monthlyFeedbackAggregationDao.saveMonthlyFeedbackAggregation(feedbackAggregation);
	}

}
