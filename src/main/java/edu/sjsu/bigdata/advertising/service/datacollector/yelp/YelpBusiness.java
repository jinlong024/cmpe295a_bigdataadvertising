package edu.sjsu.bigdata.advertising.service.datacollector.yelp;

import java.io.Serializable;


/*Business Data
==============
{
	"business_id": "0lEp4vISRmOXa8Xz2pWhbw", 
 	"full_address": "1935 E Camelback Rd\nPhoenix, AZ 85016", 
 	"open": true, 
 	"categories": ["Soup", "American (New)", "Sandwiches", "Restaurants"], 
 	"city": "Phoenix", 
 	"review_count": 54, 
 	"name": "Cafe Zupas", 
 	"neighborhoods": [], 
 	"longitude": -112.041489, 
	"state": "AZ", 
 	"stars": 4.0, 
 	"latitude": 33.508735999999999, 
 	"type": "c"
}*/

public class YelpBusiness implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String business_id;
	private String name;
	private Boolean open;
	private String[] categories;
	
	private Integer review_count;
	private String[] neighborhoods;
	
	private Double longitude;
	private Double latitude;
	private String city;
	private String state;
	private String full_address;
	private Double stars;
	private String type;
	
	
	public String getBusiness_id() {
		return business_id;
	}
	public void setBusiness_id(String business_id) {
		this.business_id = business_id;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public Boolean getOpen() {
		return open;
	}
	public void setOpen(Boolean open) {
		this.open = open;
	}
	
	public String[] getCategories() {
		return categories;
	}
	public void setCategories(String[] categories) {
		this.categories = categories;
	}
	
	public Integer getReview_count() {
		return review_count;
	}
	public void setReview_count(Integer review_count) {
		this.review_count = review_count;
	}
	
	public String[] getNeighborhoods() {
		return neighborhoods;
	}
	public void setNeighborhoods(String[] neighborhoods) {
		this.neighborhoods = neighborhoods;
	}
	
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	
	public String getFull_address() {
		return full_address;
	}
	public void setFull_address(String full_address) {
		this.full_address = full_address;
	}
	
	public Double getStars() {
		return stars;
	}
	public void setStars(Double stars) {
		this.stars = stars;
	}
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

	
}
