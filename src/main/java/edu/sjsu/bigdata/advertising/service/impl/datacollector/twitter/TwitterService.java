package edu.sjsu.bigdata.advertising.service.impl.datacollector.twitter;

import edu.sjsu.bigdata.advertising.machinelearning.classify.ClassificationService;
import edu.sjsu.bigdata.advertising.model.common.Location;
import edu.sjsu.bigdata.advertising.model.feedbackAggregation.Feedback;
import edu.sjsu.bigdata.advertising.service.interfaces.datacollection.FeedbackService;
import edu.sjsu.bigdata.advertising.util.recommendation.SentimentResultMapper;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import twitter4j.*;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.ConfigurationBuilder;

import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

/**
 * This is the class to use Twitter API (API 1.1) and collect twitter
 * collection. It also calls sentiment analysis module to get tweets feedback
 * result into MongoDB for further aggregation.
 * 
 * http://www.java-tutorial.ch/framework/twitter-with-java-tutorial
 */
@Service
public class TwitterService {

	private final Logger logger = Logger.getLogger(TwitterService.class
			.getName());

	@Autowired
	private FeedbackService feedbackService;

	private Feedback feedback;

	private String feedbackLevel;

	private String tweetText;

	private Twitter twitter = null;

	private static int numberTweets = 0;

	// @Autowired
	private ClassificationService classificationService = new ClassificationService();

	public static void main(String[] args) {

		try {
			new TwitterService().search();
		} catch (RowsExceededException e) {
			System.out.println("Catch exception during twitter search:" + e);
			e.printStackTrace();
		} catch (WriteException e) {
			System.out.println("Catch exception during twitter search:" + e);
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("Catch exception during twitter search:" + e);
			e.printStackTrace();
		}

	}

	public TwitterService() {
		init();
	}

	public void init() {
		ConfigurationBuilder cb = new ConfigurationBuilder();

		cb.setDebugEnabled(true)
				.setOAuthConsumerKey("sD0epmrdlLN7Yjk0u8qtsA")
				.setOAuthConsumerSecret(
						"hfTBgtKcaaWwFH5lWneO7bV2snpEDNKuBxijzxI6xE")
				.setOAuthAccessToken(
						"1289335860-6C9cdEkpsC03lV6HdJ6MSPoNcBMaAVXPnleN5yj")
				.setOAuthAccessTokenSecret(
						"VpvY5DhnOBCPPrZ5JlOQWJOJ44PftTdp5MiTHInOO7M");

		TwitterFactory tf = new TwitterFactory(cb.build());
		twitter = tf.getInstance();

		try {

			RequestToken requestToken = twitter.getOAuthRequestToken();

			AccessToken accessToken = null;

			while (null == accessToken) {

				logger.fine("Open the following URL and grant access to your account:");

				logger.fine(requestToken.getAuthorizationURL());

				try {

					accessToken = twitter.getOAuthAccessToken(requestToken);

				} catch (TwitterException te) {

					if (401 == te.getStatusCode()) {

						logger.severe("Unable to get the access token.");

					} else {

						te.printStackTrace();

					}

				}

			}

			logger.info("Got access token.");

			logger.info("Access token: " + accessToken.getToken());

			logger.info("Access token secret: " + accessToken.getTokenSecret());

		} catch (IllegalStateException ie) {

			// access token is already available, or consumer key/secret is
			// not set.

			if (!twitter.getAuthorization().isEnabled()) {

				logger.severe("OAuth consumer key/secret is not set.");

				return;

			}

		} catch (TwitterException te) {

			te.printStackTrace();

			logger.severe("Failed to get timeline: " + te.getMessage());

		}
	}

	public void search() throws RowsExceededException, WriteException,
			IOException {

		try {
			Query query = new Query("Apple iPhone5");
			QueryResult result;
			do {
				result = twitter.search(query);
				List<Status> tweets = result.getTweets();

				for (Status tweet : tweets) {

					// screenName = tweet.getUser().getScreenName();

					String tweetLocation = tweet.getUser().getLocation();

					String placeName = null;
					if (tweet.getPlace() != null) {
						placeName = tweet.getPlace().getFullName();
					}
					String timeStamp = tweet.getCreatedAt().toString();
					tweetText = tweet.getText();

					System.out.println("location is:" + tweetLocation);
					System.out.println("placeName is:" + placeName);
					System.out.println("timeStamp is:" + timeStamp);
					System.out.println("tweetText is:" + tweetText);

					int feedbackResult = classificationService
							.predict(tweetText);

					feedback = new Feedback();
					feedback.setProductCategory("Electronics");
					feedback.setProductManufacturer("Apple");
					feedback.setProductName("iPhone");
					feedback.setProductModel("5S");
					feedback.setTimeStamp(timeStamp);
					Location location = new Location();
					location.setCountry(tweetLocation);
					feedback.setLocation(location);

					feedback.setFeedbackLevel(SentimentResultMapper.SENTIMENTRESULT_MAPPER
							.get(feedbackResult));
					feedbackService.save(feedback);

					numberTweets++;
					System.out.println("Number of Tweets is " + numberTweets);

				}

			} while (((query = result.nextQuery()) != null)
					&& (numberTweets < 5000));

			System.out.println("Get All Tweets, exit ");

		} catch (TwitterException te) {
			te.printStackTrace();
			System.out.println("Failed to search tweets: " + te.getMessage());
			System.exit(-1);
		}

	}

	
	/**
	 * This method is used to search Tweets based on manufacturer + product name + model.
	 * 
	 * @param category: This is the product category
	 * @param manufacturer: This is the product manufacturer
	 * @param name: This is the product name
	 * @param model: This is the product model
	 * @return Return null
	 */
	public void search(String category, String manufacturer, String name,
			String model) throws RowsExceededException, WriteException,
			IOException {
		
		StringBuilder queryCriteria = new StringBuilder();
		
		queryCriteria.append(manufacturer);
		queryCriteria.append(" ");
		queryCriteria.append(name);
		queryCriteria.append(model);

		try {
			Query query = new Query(queryCriteria.toString());
			QueryResult result;
			do {
				result = twitter.search(query);
				List<Status> tweets = result.getTweets();

				for (Status tweet : tweets) {

					String tweetLocation = tweet.getUser().getLocation();

					String placeName = null;
					if (tweet.getPlace() != null) {
						placeName = tweet.getPlace().getFullName();
					}
					String timeStamp = tweet.getCreatedAt().toString();
					tweetText = tweet.getText();

					System.out.println("location is:" + tweetLocation);
					System.out.println("placeName is:" + placeName);
					System.out.println("timeStamp is:" + timeStamp);
					System.out.println("tweetText is:" + tweetText);

					int feedbackResult = classificationService
							.predict(tweetText);

					feedback = new Feedback();
					feedback.setProductCategory(category);
					feedback.setProductManufacturer(manufacturer);
					feedback.setProductName(name);
					feedback.setProductModel(model);
					feedback.setTimeStamp(timeStamp);
					Location location = new Location();
					location.setCountry(tweetLocation);
					feedback.setLocation(location);

					feedback.setFeedbackLevel(SentimentResultMapper.SENTIMENTRESULT_MAPPER
							.get(feedbackResult));
					feedbackService.save(feedback);

					numberTweets++;
					System.out.println("Number of Tweets is " + numberTweets);

				}

			} while (((query = result.nextQuery()) != null)
					&& (numberTweets < 5000));

			System.out.println("Get All Tweets, exit ");

		} catch (TwitterException te) {
			te.printStackTrace();
			System.out.println("Failed to search tweets: " + te.getMessage());
			System.exit(-1);
		}

	}

}