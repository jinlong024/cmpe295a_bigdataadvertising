package edu.sjsu.bigdata.advertising.service.impl.recommendation;


import org.springframework.stereotype.Component;


import edu.sjsu.bigdata.advertising.model.recommendation.AdPrefProfile;
import edu.sjsu.bigdata.advertising.model.recommendation.DemographicProfile;
import edu.sjsu.bigdata.advertising.util.recommendation.ProfileScaleMapper;

/**
 * This is the class to implement Single Index without weight Strategy (SIOWS)
 * Recommendation Algorithm. Author: Lei Zhang Date: 13-8-13
 */
@Component
public class NoWeightRecommendationStrategy extends BaseRecommendationStrategy {

	Double calculateDistance(AdPrefProfile adPrefProfile,
			DemographicProfile demographicProfile) {

		Double distance = 0.0;

		distance = distance + Math.abs(ProfileScaleMapper.AGE_SCALE_MAPPER.get(adPrefProfile
				.getAge())
				- ProfileScaleMapper.AGE_SCALE_MAPPER.get(demographicProfile
						.getAge()));
		
		distance = distance + Math.abs(ProfileScaleMapper.EDUCATION_SCALE_MAPPER.get(adPrefProfile
				.getEducation())
				- ProfileScaleMapper.EDUCATION_SCALE_MAPPER.get(demographicProfile
						.getEducation()));
		
		distance = distance + Math.abs(ProfileScaleMapper.INCOME_SCALE_MAPPER.get(adPrefProfile
				.getIncome())
				- ProfileScaleMapper.INCOME_SCALE_MAPPER.get(demographicProfile
						.getIncome()));
		
		distance = distance + Math.abs(ProfileScaleMapper.SEX_SCALE_MAPPER.get(adPrefProfile
				.getSex())
				- ProfileScaleMapper.SEX_SCALE_MAPPER.get(demographicProfile
						.getSex()));
		
		distance = distance + Math.abs(ProfileScaleMapper.ETHNICITY_SCALE_MAPPER.get(adPrefProfile
				.getEthnicity())
				- ProfileScaleMapper.ETHNICITY_SCALE_MAPPER.get(demographicProfile
						.getEthnicity()));
		
		distance = distance + Math.abs(ProfileScaleMapper.RACE_SCALE_MAPPER.get(adPrefProfile
				.getRace())
				- ProfileScaleMapper.RACE_SCALE_MAPPER.get(demographicProfile
						.getRace()));
		
		distance = distance + Math.abs(ProfileScaleMapper.POPULATION_SCALE_MAPPER.get(adPrefProfile
				.getPopulation())
				- ProfileScaleMapper.POPULATION_SCALE_MAPPER.get(demographicProfile
						.getPopulation()));
		
		distance = distance + Math.abs(ProfileScaleMapper.HOUSING_UNIT_SCALE_MAPPER.get(adPrefProfile
				.getHouse())
				- ProfileScaleMapper.HOUSING_UNIT_SCALE_MAPPER.get(demographicProfile
						.getHouse()));
		
		distance = distance + Math.abs(ProfileScaleMapper.WEATHER_SCALE_MAPPER.get(adPrefProfile
				.getWeather())
				- ProfileScaleMapper.WEATHER_SCALE_MAPPER.get(demographicProfile
						.getWeather()));
		
		
		return distance/ numberOfDimensions;

	}

}
