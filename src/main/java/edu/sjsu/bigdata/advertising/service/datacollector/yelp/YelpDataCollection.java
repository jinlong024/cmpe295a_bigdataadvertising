/**
 * 
 */
package edu.sjsu.bigdata.advertising.service.datacollector.yelp;


import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.scribe.builder.ServiceBuilder;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.oauth.OAuthService;

/**
 * @author yunxue
 *
 */
public class YelpDataCollection {
	
	  private OAuthService service;
	  private Token accessToken;

	  //public static YelpDataCollection getYelp(Context context) {
	  public YelpDataCollection getYelp(String consumerKey, String consumerSecret, String token, String tokenSecret) {
		  return new YelpDataCollection(consumerKey, consumerSecret, token, tokenSecret);
	  }

	  /**
	   * @param consumerKey Consumer key
	   * @param consumerSecret Consumer secret
	   * @param token Token
	   * @param tokenSecret Token secret
	   */
	  public YelpDataCollection(String consumerKey, String consumerSecret, String token, String tokenSecret) {
	    this.service = new ServiceBuilder().provider(YelpApii.class).apiKey(consumerKey).apiSecret(consumerSecret).build();
	    this.accessToken = new Token(token, tokenSecret);
	  }

	  /**
	   * Search with term and location.
	   *
	   * @param term Search term
	   * @param latitude Latitude
	   * @param longitude Longitude
	   * @return JSON string response
	   */
	  public String search(String term, double latitude, double longitude) {
	    OAuthRequest request = new OAuthRequest(Verb.GET, "http://api.yelp.com/v2/search");
	    request.addQuerystringParameter("term", term);
	    request.addQuerystringParameter("ll", latitude + "," + longitude);
	    this.service.signRequest(this.accessToken, request);
	    Response response = request.send();
	    return response.getBody();
	  }

	  /**
	   * Search with term string location.
	   *
	   * @param term Search term
	   * @param latitude Latitude
	   * @param longitude Longitude
	   * @return JSON string response
	   */
	  public String search(String term, String location) {
	    OAuthRequest request = new OAuthRequest(Verb.GET, "http://api.yelp.com/v2/search");
	    request.addQuerystringParameter("term", term);
	    request.addQuerystringParameter("location", location);
	    this.service.signRequest(this.accessToken, request);
	    Response response = request.send();
	    return response.getBody();
	  }
	  
	  
	  //busineess search
	  //j0C1t08TLW_iPPdC-8P_vQ
	  /**
	   * Search with businssId string
	   *
	   * @param businssId Search businssId
	   * @return JSON string response
	   */
	  public String businessSearch(String businessId) {
	    OAuthRequest request = new OAuthRequest(Verb.GET, "http://api.yelp.com/v2/business");
	    request.addQuerystringParameter("id", businessId);
	    this.service.signRequest(this.accessToken, request);
	    Response response = request.send();
	    return response.getBody();
	  }
	  
	  
	  
	  /**
	   * Map the data in Json obtained from Yelp2 API to Java Pojo
	   * 
	   * @param string
	   * @return
	   * @throws JSONException
	   */
	  public static List<Business> processJson(String string) throws JSONException {
			JSONObject json = new JSONObject(string);
			JSONArray businesses = json.getJSONArray("businesses");
			ArrayList<Business> businessObjs = new ArrayList<Business>(businesses.length());
			for (int i = 0; i < businesses.length(); i++) {
				JSONObject business = businesses.getJSONObject(i);
				
				
				String location  = business.optString("location");
				JSONObject addJson = new JSONObject(location);
				Address address = new Address(addJson.optString("state_code"), addJson.optString("address"),  
						addJson.optString("postal_code"), addJson.optString("country_code"), addJson.optString("city"), addJson.optString("display_address"));
				
				
				businessObjs.add( new Business(business.optString("name"), business.optString("mobile_url"), business.optDouble("rating"),  
										business.optInt("review_count"), business.optString("url"), business.optString("display_phone"), 
										business.optString("snippet_text"), address) );
			}
			
			return businessObjs;
		}
		

}
