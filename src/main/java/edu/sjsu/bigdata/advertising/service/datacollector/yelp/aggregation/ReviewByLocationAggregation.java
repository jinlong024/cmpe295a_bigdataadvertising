/**
 * 
 */
package edu.sjsu.bigdata.advertising.service.datacollector.yelp.aggregation;

import java.io.Serializable;
import java.util.Date;

/**
 * @author yunxue
 *
 */
public class ReviewByLocationAggregation extends ReviewByCategoryAggregation {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String city;
	
	//private String businessId;
	//private String businessName;
		
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	
	
}
