package edu.sjsu.bigdata.advertising.service.interfaces.feedbackAggregation;

import edu.sjsu.bigdata.advertising.model.feedbackAggregation.DailyFeedbackAggregation;
import edu.sjsu.bigdata.advertising.model.feedbackAggregation.HourlyFeedbackAggregation;
import edu.sjsu.bigdata.advertising.model.feedbackAggregation.MonthlyFeedbackAggregation;

import java.util.List;

/**
 * This is the advertisement hourly, daily and monthly feedback aggregation service
 * interface. 
 * Author: Lei Zhang 
 * Date: November 13, 2013
 */
public interface FeedbackAggregationService {
	
	public void aggregationHourlyFeedback(String fromTime, String toTime);
	
	public void aggregationDailyFeedback(String fromTime, String toTime);
	
	public void aggregationMonthlyFeedback(String fromTime, String toTime);

	public List<MonthlyFeedbackAggregation> findAllMonthlyFeedbackAggregation();
	
	public List<MonthlyFeedbackAggregation> findMonthlyFeedbackAggregationByCategory(
			String category);

	public List<DailyFeedbackAggregation> findAllDailyFeedbackAggregation();
	
	public List<DailyFeedbackAggregation> findDailyFeedbackAggregationByCategory(
			String category);

	public List<HourlyFeedbackAggregation> findAllHourlyFeedbackAggregation();

	public List<HourlyFeedbackAggregation> findHourlyFeedbackAggregationByCategory(
			String category);
	


}
