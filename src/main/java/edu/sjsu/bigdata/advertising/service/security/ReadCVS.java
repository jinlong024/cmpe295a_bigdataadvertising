/**
 * 
 */
package edu.sjsu.bigdata.advertising.service.security;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * @author yunxue
 *
 */
public class ReadCVS {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ReadCVS obj = new ReadCVS();
		obj.read();
	}

	
	public void read() {
		 
		String csvFile = "/Yun_Csco/SJSUCourses/Fall2013/CMPE226/TeamProjects/mesowest0924/mesowest_data09182013_1614.csv";
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = " \\s+";
	 
		try {
				int i = 0;
			br = new BufferedReader(new FileReader(csvFile));
			while ((line = br.readLine()) != null) {
	 
			        // use space as separator
				String[] fields = line.split(cvsSplitBy);
	 
				System.out.println(i + ": " + fields[1] + ", " + fields[2] + ", " 
							+ fields[3] + ", "+ fields[4] + ", "
							+ fields[5] + ", " + fields[6]
							+ fields[5] + ", " + fields[6]
							+ fields[7] + ", " + fields[8]					
							+ fields[9] + ", " + fields[10]
							+ fields[11] + ", " + fields[12]
							+ fields[13]);
	 
			}
	 
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	 
		System.out.println("Done");
	  }
	 
	
	
}
