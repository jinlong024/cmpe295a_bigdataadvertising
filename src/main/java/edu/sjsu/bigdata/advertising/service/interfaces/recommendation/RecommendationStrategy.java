package edu.sjsu.bigdata.advertising.service.interfaces.recommendation;


import edu.sjsu.bigdata.advertising.model.recommendation.AdCondition;
import edu.sjsu.bigdata.advertising.model.recommendation.RecommendationResult;

public interface RecommendationStrategy {
	
		
	/**
	 * This method is used to get recommendation based on the Advertisement Preference Profile ID.
	 *
	 * @param id: This is the advertisement preference profile ID.
	 * @return Return NULL if the operation is failed;
	 *         Return the recommendation result if the operation is successful.
	 *         
	 */
	public RecommendationResult getRecommendation(String id);

	/**
	 * This method is used to get recommendation based on the Advertisement Preference Profile ID.
	 *
	 * @param condition: This is the advertisement conditions which include a list of Advertisement Preference Profile ID
	 *                      and the location to display an advertisement.
	 * @return Return NULL if the operation is failed;
	 *         Return the recommendation result if the operation is successful.
	 *         
	 */
	public RecommendationResult getRecommendation(AdCondition condition);

    public void setAdPrefProfileService(AdPrefProfileService adPrefProfileService) ;


    public void setDemographicProfileService(DemographicProfileService demographicProfileService) ;

}
