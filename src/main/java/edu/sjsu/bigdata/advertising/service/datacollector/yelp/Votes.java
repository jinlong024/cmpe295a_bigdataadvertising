/**
 * 
 */
package edu.sjsu.bigdata.advertising.service.datacollector.yelp;

import java.io.Serializable;

/**
 * @author yunxue
 *
 */
public class Votes implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer funny;
	private Integer useful;
	private Integer cool;
	
	public Integer getFunny() {
		return funny;
	}
	public void setFunny(Integer funny) {
		this.funny = funny;
	}
	
	public Integer getUseful() {
		return useful;
	}
	public void setUseful(Integer useful) {
		this.useful = useful;
	}
	
	public Integer getCool() {
		return cool;
	}
	public void setCool(Integer cool) {
		this.cool = cool;
	}
}
