/**
 * 
 */
package edu.sjsu.bigdata.advertising.service.datacollector.yelp.aggregation;

import java.io.Serializable;
import java.util.Date;

/**
 * @author yunxue
 *
 */
public class ReviewByBusinessAggregation implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String aggregationId;
	private String businessId;
	private String businessName;
	private Integer userCount;
	private Date timeStamp;
	//private FeedbackLevel feedbackLevel;
	private Integer feedbackLevel;
	
	public String getAggregationId() {
		return aggregationId;
	}
	public void setAggregationId(String aggregationId) {
		this.aggregationId = aggregationId;
	}

	public String getBusinessId() {
		return businessId;
	}
	public void setBusinessId(String businessId) {
		this.businessId = businessId;
	}
	
	public String getBusinessName() {
		return businessName;
	}
	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}
	
	public Integer getUserCount() {
		return userCount;
	}
	public void setUserCount(Integer userCount) {
		this.userCount = userCount;
	}
	
	public Date getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;
	}
	
	public Integer getFeedbackLevel() {
		return feedbackLevel;
	}
	public void setFeedbackLevel(Integer feedbackLevel) {
		this.feedbackLevel = feedbackLevel;
	}
	
}
