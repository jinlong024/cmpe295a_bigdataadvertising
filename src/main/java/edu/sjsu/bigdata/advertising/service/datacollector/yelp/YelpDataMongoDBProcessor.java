/**
 * 
 */
package edu.sjsu.bigdata.advertising.service.datacollector.yelp;

//i/mport java.io.FileWriter;
//import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import edu.sjsu.bigdata.advertising.exception.dao.AdvertisngAccessException;
/**
 * @author yunxue
 *
 */
public class YelpDataMongoDBProcessor {


	//public final static String PATH = "/Yun_Csco/SJSUCourses/Fall2013/CMPE295B/YelpAcademicDataset";
	public final static String PATH = "/Yun_Csco/SJSUCourses/Fall2013/CMPE295B/YelpAcademicDataset/YelpBigData";
	
	public final static String CSV_FILENAME = "yelp_academic_dataset_business-OCT30.csv"; 
	
	public final static String BUSINESS_JSON_FILENAME = "yelp_academic_dataset_business.json"; 
	public final static String REVIEW_JSON_FILENAME = "yelp_academic_dataset_reviewN06.json";
	///////public final static String REVIEW_JSON_FILENAME = "yelp_review_data.json";
	
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		ApplicationContext appContext = new ClassPathXmlApplicationContext("applicationContext.xml");
		IYelpDataService yService = (YelpDataServiceImpl)appContext.getBean("yelpDataService");
		
		
		List<YelpBusiness> yelpBusinesses = null;
		List<YelpReview> yelpReviews = null;
		
		try {
			/*
			//read from Yelp business json data file
			yelpBusinesses = JsonReaderUtil.readYelpBusiness(PATH + "/" + BUSINESS_JSON_FILENAME);
			
			long businessCount = yelpBusinesses.size();
			System.out.println("+++++++++++++++++++++Business Counts: " + businessCount + "++++++++++++++++++++++++\n");
		    
			for (int i=0; i<businessCount; i++) {
				yService.insertYelpBusiness(yelpBusinesses.get(i));
			}*/
			
			//read from Yelp review json data file
			yelpReviews = JsonReaderUtil.read(PATH + "/" + REVIEW_JSON_FILENAME);
			
			long reviewCount = yelpReviews.size();
			System.out.println("+++++++++++++++++++++Review Counts: " + reviewCount + "++++++++++++++++++++++++\n");
		    
			for (int i=0; i<reviewCount; i++) {
				yService.insertYelpReview(yelpReviews.get(i));
			}
		}
		catch(AdvertisngAccessException e) {
		     e.printStackTrace();
		} 
	}


}
