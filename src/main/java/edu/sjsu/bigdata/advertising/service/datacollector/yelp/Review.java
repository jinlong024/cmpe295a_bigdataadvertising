/**
 * 
 */
package edu.sjsu.bigdata.advertising.service.datacollector.yelp;

/**
 * @author yunxue
 *
 */
public class Review extends YelpReview {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private int ranking;
	
	public int getRanking() {
		return ranking;
	}
	public void setRanking(int ranking) {
		if (ranking == 5)
			this.ranking = 2;
		else if (ranking == 4)
			this.ranking = 1;
		else if (ranking == 3)
			this.ranking = 0;
		else if (ranking == 2)
			this.ranking = -1;
		else if (ranking == 1)
			this.ranking = -2;
		else 
			this.ranking = 0;
	}
	
	/*public void setRanking(int ranking) { 
		this.ranking = ranking;
	}*/
}
