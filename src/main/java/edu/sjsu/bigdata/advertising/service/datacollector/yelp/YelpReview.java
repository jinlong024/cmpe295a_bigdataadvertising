/**
 * 
 */
package edu.sjsu.bigdata.advertising.service.datacollector.yelp;

import java.io.Serializable;

/**
 * @author yunxue
 *
 */


/*
 Review Data
==============
{
	"votes": {"funny": 0, "useful": 4, "cool": 2}, 
	"user_id": "_7el1cOgnfkNfmZKi277bQ", 
	"review_id": "rbI4SgEjR5_mFD0AAJpL4Q", 
	"stars": 4, 
	"date": "2011-12-08", 
	
	"text": "Zupas officially opens today!" 
	
	"type": "review", 
	"business_id": "0lEp4vISRmOXa8Xz2pWhbw"
}
 */
public class YelpReview implements Serializable  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private Votes votes;
	private String user_id;
	private String review_id;
	private Integer stars;
	private String date;
	private String text;
	private String type;
	private String business_id;
	//private String ranking;
	

	public Votes getVotes() {
		return votes;
	}
	public void setVotes(Votes votes) {
		this.votes = votes;
	}
	
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	
	public String getReview_id() {
		return review_id;
	}
	public void setReview_id(String review_id) {
		this.review_id = review_id;
	}
	
	public Integer getStars() {
		return stars;
	}
	public void setStars(Integer stars) {
		this.stars = stars;
	}
	
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	public String getBusiness_id() {
		return business_id;
	}
	public void setBusiness_id(String business_id) {
		this.business_id = business_id;
	}

	
	/*public String getRanking() {
		return ranking;
	}
	public void setRanking(String ranking) {
		this.ranking = ranking;
	}*/

}
