/**
 * 
 */
package edu.sjsu.bigdata.advertising.service.datacollector.yelp.aggregation;

import java.io.Serializable;

/**
 * @author yunxue
 *
 */
public class FeedbackLevel  implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String stronglyDislike;
	private String dislike;
	private String neutral;
	private String like;
	private String stronglyLike;
	
	public String getStronglyLike() {
		return stronglyLike;
	}
	public void setStronglyLike(String stronglyLike) {
		this.stronglyLike = stronglyLike;
	}
	
	public String getLike() {
		return like;
	}
	public void setLike(String like) {
		this.like = like;
	}

	public String getNeutral() {
		return neutral;
	}
	public void setNeutral(String neutral) {
		this.neutral = neutral;
	}
	
	public String getDislike() {
		return dislike;
	}
	public void setDislike(String dislike) {
		this.dislike = dislike;
	}

	public String getStronglyDislike() {
		return stronglyDislike;
	}
	public void setStronglyDislike(String stronglyDislike) {
		this.stronglyDislike = stronglyDislike;
	}
	
}
