/**
 * 
 */
package edu.sjsu.bigdata.advertising.service.security;

import edu.sjsu.bigdata.advertising.dao.interfaces.security.IUserRoleDao;
import edu.sjsu.bigdata.advertising.exception.dao.AdvertisngAccessException;
import edu.sjsu.bigdata.advertising.model.security.HttpUser;
import edu.sjsu.bigdata.advertising.model.security.Role;
import edu.sjsu.bigdata.advertising.model.security.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * @author yunxue
 *
 */
@Service
public class UserSecurityServiceImpl implements IUserSecurityService {
	
	@Autowired
	private IUserRoleDao userRoleDao;
	
	
	
	public IUserRoleDao getUserRoleDao() {
		return userRoleDao;
	}


	public void setUserRoleDao(IUserRoleDao userRoleDao) {
		this.userRoleDao = userRoleDao;
	}

	

	public User getUserById(String userId) throws AdvertisngAccessException {
		User user = null;
		// TODO
		user = userRoleDao.getUserById(userId);
		return user;
	}
	
	
	public User getUserByEmail(String email) throws AdvertisngAccessException {
		User user = null;
		// TODO
		user = userRoleDao.getUserByEmail(email);
		return user;
	}
	
	
	public List<User> getAllUsers() throws AdvertisngAccessException {
		List<User> users = userRoleDao.getAllUser();
		return users;
	}
	
	
	public void addUser(User user) throws AdvertisngAccessException {
		userRoleDao.saveOrUpdateUser(user);
	}
	
	
	public void deleteUser(String email)  throws AdvertisngAccessException {
		userRoleDao.deleteUser(email);
	}
	
	
	public Role getRoleById(String roleId) throws AdvertisngAccessException {
		Role role = null;
		role = userRoleDao.getRoleById(roleId);
		return role;
	}
	
	
	public Role getRoleByRole(String roleName) throws AdvertisngAccessException {
		Role role = null;
		role = userRoleDao.getRoleByRole(roleName);
		return role;
	}
	
	
	public List<Role> getAllUserRoles() throws AdvertisngAccessException {
		List<Role> roles = userRoleDao.getAllRole();
		return roles;
	}
	
	
	public void addRole(Role role)  throws AdvertisngAccessException {
		userRoleDao.saveOrUpdateRole(role);
	}
	
	
	public void deleteRole(String roleName) throws AdvertisngAccessException {
		userRoleDao.deleteRole(roleName);
	}

	

	public HttpUser authenticate(String email, String password) throws AdvertisngAccessException {	
		HttpUser user = userRoleDao.authenticate(email, password);
		return user;
	}

	
	
	public void register(User user) throws AdvertisngAccessException {
		userRoleDao.saveOrUpdateUser(user);
	}
}
