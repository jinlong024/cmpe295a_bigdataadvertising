/**
 * 
 */
package edu.sjsu.bigdata.advertising.service.datacollector.yelp.aggregation;

import java.io.Serializable;

import edu.sjsu.bigdata.advertising.service.datacollector.yelp.YelpReview;

/**
 * @author yunxue
 *
 */
public class YelpReviewWithBInfo implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private YelpReview yelpReview;

	private String city;
	private String businessName;
	private String category;
	
	
	public YelpReview getYelpReview() {
		return yelpReview;
	}
	public void setYelpReview(YelpReview yelpReview) {
		this.yelpReview = yelpReview;
	}

	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	
	public String getBusinessName() {
		return businessName;
	}
	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}
	
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}

}
