package edu.sjsu.bigdata.advertising.service.impl.recommendation;

import edu.sjsu.bigdata.advertising.dao.interfaces.recommendation.AdPrefProfileDao;
import edu.sjsu.bigdata.advertising.model.recommendation.AdPrefProfile;
import edu.sjsu.bigdata.advertising.service.interfaces.recommendation.AdPrefProfileService;


import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;


import java.util.List;


/**
 * This is the advertisement preference profile service implementation. User:
 * lei zhang Date: 13-8-13
 */
@Service
public class AdPrefProfileServiceImpl implements AdPrefProfileService {

	@Autowired
	private AdPrefProfileDao adPrefProfileDao;

	public static final String COLLECTION_NAME = "adPrefProfile";

	/**
	 * This method is used to insert or update Advertisement Preference Profile
	 * and return its ID.
	 * 
	 * @param entity
	 *            : This is the advertisement preference profile to be saved.
	 * @return Return null if the operation is failed; Return the profile ID if
	 *         the operation is successful.
	 */

	public AdPrefProfile saveProfile(AdPrefProfile entity) {
	
		return adPrefProfileDao.saveProfile(entity);
	}
	
	public AdPrefProfile updateProfile(AdPrefProfile entity) {
		return adPrefProfileDao.updateProfile(entity);
	}

	/**
	 * This method is used to delete Advertisement Preference Profile by ID and
	 * return the result if the it has been deleted.
	 * 
	 * @param id
	 *            : This is the advertisement preference profile ID.
	 * @return Return false if the operation is failed; Return true if the
	 *         operation is successful.

	 */
	public boolean deleteProfile(String id) {

		return adPrefProfileDao.deleteProfile(id);
	}

	/**
	 * This method is used to get Advertisement Preference Profile by ID.
	 * 
	 * @param id
	 *            : This is the advertisement preference profile ID.
	 * @return Return NULL if the operation is failed; Return the advertisement
	 *         preference profile if the operation is successful.
	 */
	public AdPrefProfile findProfileById(String id) {
		return adPrefProfileDao.findProfileById(id);
	}

	/**
	 * This method is used to get list of existing Advertisement Preference
	 * Profiles.
	 * 
	 * @return Return NULL if the operation is failed; Return the list of
	 *         existing advertisement preference profiles if the operation is
	 *         successful.
	 */
	public List<AdPrefProfile> findAllProfile() {
		return adPrefProfileDao.findAllProfile();
	}
}
