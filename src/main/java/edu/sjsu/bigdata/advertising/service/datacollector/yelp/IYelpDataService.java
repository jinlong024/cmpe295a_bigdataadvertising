/**
 * 
 */
package edu.sjsu.bigdata.advertising.service.datacollector.yelp;

import java.util.List;

import edu.sjsu.bigdata.advertising.exception.dao.AdvertisngAccessException;
import edu.sjsu.bigdata.advertising.service.datacollector.yelp.aggregation.ReviewByBusinessAggregation;
import edu.sjsu.bigdata.advertising.service.datacollector.yelp.aggregation.ReviewByCategoryAggregation;
import edu.sjsu.bigdata.advertising.service.datacollector.yelp.aggregation.ReviewByDailyTimeAggregation;
import edu.sjsu.bigdata.advertising.service.datacollector.yelp.aggregation.ReviewByLocationAggregation;
import edu.sjsu.bigdata.advertising.service.datacollector.yelp.aggregation.ReviewByMonthlyTimeAggregation;


/**
 * @author yunxue
 *
 */

public interface IYelpDataService {
	
	public YelpBusiness getYelpBusinessById(String bId) throws AdvertisngAccessException;
	public void insertYelpBusiness(YelpBusiness business) throws AdvertisngAccessException;
	public List<YelpBusiness> getAllYelpBusiness() throws AdvertisngAccessException;
	public void deleteYelpBusiness(String bId) throws AdvertisngAccessException;
	
	public YelpReview getYelpReviewById(String rId) throws AdvertisngAccessException;
	public void insertYelpReview(YelpReview review) throws AdvertisngAccessException;
	public List<YelpReview> getAllYelpReview() throws AdvertisngAccessException;
	public void deleteYelpReview(String rId) throws AdvertisngAccessException;
	
	public void aggregateBusiness(String businessId) throws AdvertisngAccessException;
	
	
	//aggegation APIs
	public void insertYelpBusinessAggregation(ReviewByBusinessAggregation bAgg) throws AdvertisngAccessException;
	public void insertYelpLocationAggregation(ReviewByLocationAggregation lAgg) throws AdvertisngAccessException;
	public void insertYelpCategoryAggregation(ReviewByCategoryAggregation cAgg) throws AdvertisngAccessException;
	
	public void insertYelpDailyTimeAggregation(ReviewByDailyTimeAggregation dAgg) throws AdvertisngAccessException;
	public void insertYelpMonthlyTimeAggregation(ReviewByMonthlyTimeAggregation mAgg) throws AdvertisngAccessException;
	
	
	public List<ReviewByBusinessAggregation> getAllYelpBusinessAggregationData() throws AdvertisngAccessException;
	public List<ReviewByLocationAggregation> getAllYelpLocationAggregationData() throws AdvertisngAccessException;
	public List<ReviewByCategoryAggregation> getAllYelpCategoryAggregationData() throws AdvertisngAccessException;

	public List<ReviewByDailyTimeAggregation> getAllYelpDailyTimeAggregationData() throws AdvertisngAccessException;
	public List<ReviewByMonthlyTimeAggregation> getAllYelpMonlyTimeAggregationData() throws AdvertisngAccessException;
	
	
	
	public ReviewByCategoryAggregation findCategoryAggregationByName(String category) throws AdvertisngAccessException;
	public ReviewByLocationAggregation findLocationAggregationByName(String category, String city) throws AdvertisngAccessException;
	public ReviewByBusinessAggregation findBusinessAggregationByName(String bname) throws AdvertisngAccessException;
	//public ReviewByDailyTimeAggregation findDailyTimeAggregationByValue(String category, String date) throws AdvertisngAccessException;
	//public ReviewByMonthlyTimeAggregation findMonthlyTimeAggregationByValue(String category, String month) throws AdvertisngAccessException;
	
	public List<ReviewByLocationAggregation> findLocationAggregationByName(String city) throws AdvertisngAccessException;
	public List<ReviewByDailyTimeAggregation> findDailyTimeAggregationByValue(String startDate, String endDate) throws AdvertisngAccessException;
	public List<ReviewByMonthlyTimeAggregation> findMonthlyTimeAggregationByValue(String startMonth, String endMonth) throws AdvertisngAccessException;

	
}
