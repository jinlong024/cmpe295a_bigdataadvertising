package edu.sjsu.bigdata.advertising.service.impl.datacollector.twitter;

import jxl.write.WriteException;

import org.springframework.stereotype.Service;

import java.io.IOException;

/**
 * This is the class to use Twitter API (API 1.1) and collect twitter
 * collection. It also calls sentiment analysis module to get tweets feedback
 * result into MongoDB for further aggregation.
 * 
 * http://www.java-tutorial.ch/framework/twitter-with-java-tutorial
 */
@Service
public class TwitterServiceThread implements Runnable {

	TwitterService twitterService;

	String category;
	String manufacturer;
	String name;
	String model;

	public TwitterServiceThread(TwitterService twitterService, String category,
			String manufacturer, String name, String model) {
		this.twitterService = twitterService;
		this.category = category;
		this.manufacturer = manufacturer;
		this.name = name;
		this.model = model;
	}

	public TwitterServiceThread() {

	}

	public void run() {

		try {
			twitterService.init();
			twitterService.search(category, manufacturer, name, model);
		} catch (WriteException | IOException e) {
			System.out.println("Catch exception during twitt search:" + e);
			e.printStackTrace();
		}
	}

}