package edu.sjsu.bigdata.advertising.service.interfaces.datacollection;

import edu.sjsu.bigdata.advertising.model.feedbackAggregation.Feedback;

import java.util.List;

/**
 * This is the advertisement feedback service interface.
 * User: Lei Zhang
 * Date: August 13, 2013
 */
public interface FeedbackService {

    /**
     * This method is used to insert or update Advertisement Preference Profile and return its ID.
     *
     * @param entity: This is the advertisement preference profile to be saved.
     * @return Return null if the operation is failed;
     *         Return the profile ID if the operation is successful.
     *
     * Client Example:
     * Save: POST /recommendation/adprefprofile with the advertisement preference profile Form/JSON/XML
     * Update: PUT /recommendation/adprefprofile with the advertisement preference profile Form/JSON/XML
     */
    public Feedback save(Feedback entity);
    
    public Feedback update(Feedback entity);

    /**
     * This method is used to delete Advertisement Preference Profile by ID and return the result
     * if the it has been deleted.
     *
     * @param id: This is the advertisement preference profile ID.
     * @return Return false if the operation is failed;
     *         Return true if the operation is successful.
     *
     * Client Example:
     * Delete: DELETE /recommendation/adprefprofile/{id}
     */
    public boolean delete(String id);

    /**
     * This method is used to get Advertisement Preference Profile by ID.
     *
     * @param id: This is the advertisement preference profile ID.
     * @return Return NULL if the operation is failed;
     *         Return the advertisement preference profile if the operation is successful.
     *
     * Client Example:
     * Get: GET /recommendation/adprefprofile/{id}
     */
    public Feedback findById(String id);

    /**
     * This method is used to get list of existing Advertisement Preference Profiles.
     *
     * @return Return NULL if the operation is failed;
     *         Return the list of existing advertisement preference profiles if the operation is successful.
     *
     * Client Example:
     * Get All: GET /recommendation/adprefprofile
     */
    public List<Feedback> findAll();
}
