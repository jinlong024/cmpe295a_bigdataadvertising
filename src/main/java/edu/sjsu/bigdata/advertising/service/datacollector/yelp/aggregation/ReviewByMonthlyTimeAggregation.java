/**
 * 
 */
package edu.sjsu.bigdata.advertising.service.datacollector.yelp.aggregation;

import java.io.Serializable;
import java.util.Date;

/**
 * @author yunxue
 *
 */
public class ReviewByMonthlyTimeAggregation extends ReviewByCategoryAggregation {

	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */
	
	private String monthlyTime;
	
	public String getMonthlyTime() {
		return monthlyTime;
	}
	public void setMonthlyTime(String monthlyTime) {
		this.monthlyTime = monthlyTime;
	}

	
}
