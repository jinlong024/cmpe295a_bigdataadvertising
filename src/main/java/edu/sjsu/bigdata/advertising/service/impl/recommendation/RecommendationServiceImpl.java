package edu.sjsu.bigdata.advertising.service.impl.recommendation;

import edu.sjsu.bigdata.advertising.model.recommendation.AdCondition;
import edu.sjsu.bigdata.advertising.model.recommendation.RecommendationResult;
import edu.sjsu.bigdata.advertising.service.interfaces.recommendation.AdPrefProfileService;
import edu.sjsu.bigdata.advertising.service.interfaces.recommendation.DemographicProfileService;
import edu.sjsu.bigdata.advertising.service.interfaces.recommendation.RecommendationService;
import edu.sjsu.bigdata.advertising.service.interfaces.recommendation.RecommendationStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RecommendationServiceImpl implements RecommendationService {

	private RecommendationStrategy strategy = new SynthesisRecommendationStrategy();

    @Autowired
    private AdPrefProfileService adPrefProfileService;

    @Autowired
    private DemographicProfileService demographicProfileService;

    public RecommendationResult getRecommendation(String id) {

        System.out.println("In RecommendationServiceImpl, AdPrefProfileService is:" + adPrefProfileService);

        strategy.setAdPrefProfileService(adPrefProfileService);
        strategy.setDemographicProfileService(demographicProfileService);

		return strategy.getRecommendation(id);
	}

	public RecommendationResult getRecommendation(AdCondition condition) {

        strategy.setAdPrefProfileService(adPrefProfileService);
        strategy.setDemographicProfileService(demographicProfileService);

		return strategy.getRecommendation(condition);
	}

}
