/**
 * 
 */
package edu.sjsu.bigdata.advertising.service.datacollector.yelp;

import java.io.Serializable;

/**
 * @author yunxue
 *
 */
public class Address implements Serializable  {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String stateCode;
	public String address;
	
	public String postalCode;
	public String countryCode;
	
	public String city;
	public String displayAddress;

	public Address() {
	}
	
	
	public Address(String stateCode, String address, String postalCode, String countryCode, String city, String displayAddress) {
		this.stateCode = stateCode;
		this.address = address;
		this.postalCode = postalCode;
		this.countryCode = countryCode;
		this.city = city;
		this.displayAddress = displayAddress;
	}

	@Override
	public String toString() {
		return address;
	}
	
}
