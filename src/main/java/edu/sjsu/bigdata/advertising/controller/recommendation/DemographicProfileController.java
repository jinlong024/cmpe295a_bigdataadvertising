package edu.sjsu.bigdata.advertising.controller.recommendation;

import edu.sjsu.bigdata.advertising.model.recommendation.DemographicProfile;
import edu.sjsu.bigdata.advertising.service.interfaces.recommendation.DemographicProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * This is the demographic  profile RESFFul web service controller.
 * User: Lei Zhang
 * Date: August 13, 2013
 */
@Controller
@RequestMapping("/demographicProfiles")
public class DemographicProfileController {

    @Autowired
    private DemographicProfileService demograpphicProfileService;

    /**
     * This method is used to insert or update Advertisement Preference Profile
     * and return its ID.
     *
     * @param entity : This is the advertisement preference profile to be saved.
     * @return Return null if the operation is failed; Return the profile ID if
     *         the operation is successful.
     *         <p/>
     *         Client Example: Save: POST /recommendation/demographicProfile with the
     *         advertisement preference profile Form/JSON/XML Update: PUT
     *         /recommendation/demographicProfile with the advertisement preference
     *         profile Form/JSON/XML
     */
    @RequestMapping(method = RequestMethod.POST, consumes = {"application/json", "application/xml"})
    public void saveProfile(
            @ModelAttribute("demographicProfile") DemographicProfile entity,
            ModelMap model) {

        System.out.println("Saving: " + entity);
        if (StringUtils.hasText(entity.getId())) {
            demograpphicProfileService.updateProfile(entity);
        } else {
            demograpphicProfileService.saveProfile(entity);
        }

        System.out.println("Saved: " + entity);

    }
        

    /**
     * This method is used to delete Advertisement Preference Profile by ID and
     * return the result if the it has been deleted.
     *
     * @param id : This is the advertisement preference profile ID.
     * @return Return false if the operation is failed; Return true if the
     *         operation is successful.
     *         <p/>
     *         Client Example: Delete: DELETE /recommendation/demographicProfiles/{id}
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteProfile(@PathVariable String id, ModelMap model) {
        demograpphicProfileService.deleteProfile(id);

    }



    /**
     * This method is used to get Advertisement Preference Profile by ID.
     *
     * @param id : This is the advertisement preference profile ID.
     * @return Return NULL if the operation is failed; Return the advertisement
     *         preference profile if the operation is successful.
     *         <p/>
     *         Client Example: Get: GET /recommendation/demographicProfile/{id}
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public DemographicProfile findProfileById(@PathVariable String id) {

        System.out.println("Finding profile by ID:" + id);
        DemographicProfile profile = demograpphicProfileService.findProfileById(id);

        System.out.println("Found profile:" + profile.getAge());

        return profile;

    }





    /**
     * This method is used to get list of existing Advertisement Preference
     * Profiles.
     *
     * @return Return NULL if the operation is failed; Return the list of
     *         existing advertisement preference profiles if the operation is
     *         successful.
     *         <p/>
     *         Client Example: Get All: GET /demographicProfile
     */
    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public List<DemographicProfile> findAllProfile(ModelMap model) {
        return demograpphicProfileService.findAllProfile();

    }



}
