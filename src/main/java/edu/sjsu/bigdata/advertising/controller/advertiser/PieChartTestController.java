package edu.sjsu.bigdata.advertising.controller.advertiser;

import edu.sjsu.bigdata.advertising.model.feedbackAggregation.DailyFeedbackAggregation;
import edu.sjsu.bigdata.advertising.model.feedbackAggregation.HourlyFeedbackAggregation;
import edu.sjsu.bigdata.advertising.model.recommendation.AdPrefProfile;
import edu.sjsu.bigdata.advertising.service.interfaces.feedbackAggregation.FeedbackAggregationService;
import edu.sjsu.bigdata.advertising.service.interfaces.recommendation.AdPrefProfileService;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.view.RedirectView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: macbookpro
 * Date: 11/6/13
 * Time: 5:15 PM
 * To change this template use File | Settings | File Templates.
 */


@Controller
@RequestMapping("/Twitter")
public class PieChartTestController {

/*
    @Autowired
    private AdPrefProfileService adPrefProfileService;

    @RequestMapping(method = RequestMethod.GET)
    public String getEmptyProfile(ModelMap model) {
        //model.addAttribute("personList", personService.listPerson());
        return "displayPiechart";
    }
*/


    @Autowired
    private FeedbackAggregationService feedbackAggregationService;


    @RequestMapping(value = "/daily", method = RequestMethod.GET)
    public ModelAndView findAllDailyFeedbackAggregation(ModelMap model) throws IOException {
        List<DailyFeedbackAggregation> dailyFeedbackAggregations = feedbackAggregationService.findAllDailyFeedbackAggregation();
        model.addAttribute("feedback", dailyFeedbackAggregations);
        ModelAndView myModel = new ModelAndView("displayTwitterDailyPiechart");

        return myModel;
    }

    @RequestMapping(value = "/hourly", method = RequestMethod.GET)
    public ModelAndView findAllHourlyFeedbackAggregation(ModelMap model) throws IOException {
        List<HourlyFeedbackAggregation> hourlyFeedbackAggregations = feedbackAggregationService.findAllHourlyFeedbackAggregation();
        model.addAttribute("feedback", hourlyFeedbackAggregations);
        ModelAndView myModel = new ModelAndView("displayTwitterhourlyPiechart");

        return myModel;
    }
}