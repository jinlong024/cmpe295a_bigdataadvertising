package edu.sjsu.bigdata.advertising.controller.advertiser;

import com.sun.org.apache.xerces.internal.xs.StringList;
import edu.sjsu.bigdata.advertising.model.recommendation.AdPrefProfile;
import edu.sjsu.bigdata.advertising.model.recommendation.DemographicProfile;
import edu.sjsu.bigdata.advertising.model.recommendation.RecommendationResult;
import edu.sjsu.bigdata.advertising.service.interfaces.recommendation.AdPrefProfileService;

import edu.sjsu.bigdata.advertising.service.interfaces.recommendation.DemographicProfileService;
import edu.sjsu.bigdata.advertising.service.interfaces.recommendation.RecommendationService;
import edu.sjsu.bigdata.advertising.util.recommendation.ProfileScaleMapper;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.view.RedirectView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: macbookpro
 * Date: 10/30/13
 * Time: 1:05 PM
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping("/map")
public class MapTestController {

    @Autowired
    private AdPrefProfileService adPrefProfileService;
    @Autowired
    private DemographicProfileService demographicProfileService;
    @Autowired
    private RecommendationService recommendationService;


    @RequestMapping(method = RequestMethod.GET)
    public String getRecommend
            (@RequestParam String id, ModelMap model) throws IOException{
       RecommendationResult recommendationResult = recommendationService.getRecommendation(id);
        DemographicProfile demographicProfile = demographicProfileService.findProfileById(recommendationResult.getDemographicProfileId());
        AdPrefProfile adPrefProfile = adPrefProfileService.findProfileById(id);
        ObjectMapper mapper = new ObjectMapper();




        Integer[] integer = new Integer[]{
            ProfileScaleMapper.AGE_SCALE_MAPPER.get(adPrefProfile.getAge()),
                    ProfileScaleMapper.EDUCATION_SCALE_MAPPER.get(adPrefProfile.getEducation()),
                    ProfileScaleMapper.ETHNICITY_SCALE_MAPPER.get(adPrefProfile.getEthnicity()),
              ProfileScaleMapper.SEX_SCALE_MAPPER.get(adPrefProfile.getSex()),
               ProfileScaleMapper.HOUSING_UNIT_SCALE_MAPPER.get(adPrefProfile.getHouse()),
               ProfileScaleMapper.INCOME_SCALE_MAPPER.get(adPrefProfile.getIncome()),
               ProfileScaleMapper.POPULATION_SCALE_MAPPER.get(adPrefProfile.getPopulation()),
              ProfileScaleMapper.RACE_SCALE_MAPPER.get(adPrefProfile.getRace()),
               ProfileScaleMapper.WEATHER_SCALE_MAPPER.get(adPrefProfile.getWeather())

        };

        Integer[] integer1 = new Integer[]{
                ProfileScaleMapper.AGE_SCALE_MAPPER.get(demographicProfile.getAge()),
                ProfileScaleMapper.EDUCATION_SCALE_MAPPER.get(demographicProfile.getEducation()),
                ProfileScaleMapper.ETHNICITY_SCALE_MAPPER.get(demographicProfile.getEthnicity()),
                ProfileScaleMapper.SEX_SCALE_MAPPER.get(demographicProfile.getSex()),
                ProfileScaleMapper.HOUSING_UNIT_SCALE_MAPPER.get(demographicProfile.getHouse()),
                ProfileScaleMapper.INCOME_SCALE_MAPPER.get(demographicProfile.getIncome()),
                ProfileScaleMapper.POPULATION_SCALE_MAPPER.get(demographicProfile.getPopulation()),
                ProfileScaleMapper.RACE_SCALE_MAPPER.get(demographicProfile.getRace()),
                ProfileScaleMapper.WEATHER_SCALE_MAPPER.get(demographicProfile.getWeather()),


        };


        model.addAttribute("adPrefProfile", mapper.writeValueAsString(integer));
        model.addAttribute("demographic", mapper.writeValueAsString(integer1));
        model.addAttribute("city", demographicProfile.getLocation().getCity());
        return "displayMap";

    }
//    @RequestMapping(method = RequestMethod.GET)
//    public String getRecommend
//            (@RequestParam String id, ModelMap model) throws IOException{
//        RecommendationResult recommendationResult = recommendationService.getRecommendation(id);
//        DemographicProfile demographicProfile = demographicProfileService.findProfileById(id);
//        AdPrefProfile adPrefProfile = adPrefProfileService.findProfileById(id);
//        ObjectMapper mapper = new ObjectMapper();
//        ProfileScaleMapper mapper1 = new ProfileScaleMapper();
//        StringList stringList = new
//                int[] integer = new int[]{
//                ProfileScaleMapper.AGE_SCALE_MAPPER.get(adPrefProfile
//                        .getAge()),
//                ProfileScaleMapper.EDUCATION_SCALE_MAPPER.get(adPrefProfile.getEducation()),
//                ProfileScaleMapper.ETHNICITY_SCALE_MAPPER.get(adPrefProfile.getEthnicity())
//
//        };
//
//
//
//        model.addAttribute("adPrefProfile", mapper.writeValueAsString(adPrefProfile));
////        model.addAttribute("demographicProfile", mapper1.toString(demographicProfile));
//        model.addAttribute("de",integer );
//       /* ObjectMapper mapper = new ObjectMapper();
//
//        model.addAttribute("demographicProfile", mapper.writeValueAsString(demographicProfile));*/
//        return "displayMap";
//
//    }

 /*   @RequestMapping(method = RequestMethod.GET)
    public String displayAd
            (@RequestParam String id, ModelMap model) throws IOException {
//        RecommendationResult recommendationResult = recommendationService.getRecommendation(id);
//        DemographicProfile demographicProfile = demographicProfileService.findProfileById(id);

*//*        ObjectMapper mapper = new ObjectMapper();

        model.addAttribute("demographicProfile", mapper.writeValueAsString(demographicProfile));*//*
        AdPrefProfile adPrefProfile = adPrefProfileService.findProfileById(id);
        ObjectMapper mapper = new ObjectMapper();

        model.addAttribute("adPrefProfile", mapper.writeValueAsString(adPrefProfile));

        return "displayMap";
    }*/

/*    public String getDemographic
            (@RequestParam  ModelMap model) throws IOException {
        String id = "5289c887f4c932a3345082f7";
        DemographicProfile demographicProfile = demographicProfileService.findProfileById(id);
        return  "displayMap";
    }*/


/*    @RequestMapping(method = RequestMethod.GET)
    public String getEmptyProfile(ModelMap model) {
        //model.addAttribute("personList", personService.listPerson());
        return "displayMap";
    }*/

}

