/**
 * 
 */
package edu.sjsu.bigdata.advertising.controller.feedbackAggregation;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import edu.sjsu.bigdata.advertising.service.datacollector.yelp.IYelpDataService;
import edu.sjsu.bigdata.advertising.exception.dao.AdvertisngAccessException;
import edu.sjsu.bigdata.advertising.model.datacollector.common.SearchCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.PathVariable;

import edu.sjsu.bigdata.advertising.service.datacollector.yelp.aggregation.ReviewByCategoryAggregation;
import edu.sjsu.bigdata.advertising.service.datacollector.yelp.aggregation.ReviewByBusinessAggregation;
import edu.sjsu.bigdata.advertising.service.datacollector.yelp.aggregation.ReviewByLocationAggregation;
import edu.sjsu.bigdata.advertising.service.datacollector.yelp.aggregation.ReviewByDailyTimeAggregation;
import edu.sjsu.bigdata.advertising.service.datacollector.yelp.aggregation.ReviewByMonthlyTimeAggregation;
import java.io.IOException;


/**
 * @author yunxue
 *
 */

@Controller
@RequestMapping("/aggregation/yelp")

public class YelpFeedbackAggreationController {

//	@Autowired
	private IYelpDataService yelpAggDataService;
    
    
    @RequestMapping(value = "/category/{category}", method = RequestMethod.GET,
			produces = {"application/json", "application/xml" } )
    @ResponseBody
	public ReviewByCategoryAggregation findCategoryAggregationByName(@PathVariable String category) {
		ReviewByCategoryAggregation agg = null;
		try {
			agg = yelpAggDataService.findCategoryAggregationByName(category);
		} catch (AdvertisngAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return agg;
	}
	
	
    @RequestMapping(value = "/location/{category}/{city}", method = RequestMethod.GET,
			produces = {"application/json", "application/xml" } )
    @ResponseBody
	public ReviewByLocationAggregation findLocationAggregationByName(@PathVariable String category, @PathVariable String city) {
		ReviewByLocationAggregation agg = null;
		try {
			agg = yelpAggDataService.findLocationAggregationByName(category, city);
		} catch (AdvertisngAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return agg;
	}
	
	
    @RequestMapping(value = "/business/{bname}", method = RequestMethod.GET,
			produces = {"application/json", "application/xml" } )
    @ResponseBody
	public ReviewByBusinessAggregation findBusinessAggregationByName(@PathVariable String bname) {
		ReviewByBusinessAggregation agg = null;
		try {
			agg = yelpAggDataService.findBusinessAggregationByName(bname);
		} catch (AdvertisngAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return agg;
	}
	
	
    /*@RequestMapping(value = "/dailyTime/{category}/{date}", method = RequestMethod.GET,
			produces = {"application/json", "application/xml" } )
    @ResponseBody
	public ReviewByDailyTimeAggregation findDailyTimeAggregationByValue(@PathVariable String category, @PathVariable String  date) {
		ReviewByDailyTimeAggregation agg = null;
		try {
			agg = yelpAggDataService.findDailyTimeAggregationByValue(category, date);
		} catch (AdvertisngAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return agg;
	}
	
	
    @RequestMapping(value = "/monthlyTime/{category}/{month}", method = RequestMethod.GET,
			produces = {"application/json", "application/xml" } )
    @ResponseBody
	public ReviewByMonthlyTimeAggregation findMonthlyTimeAggregationByValue(@PathVariable String category, @PathVariable String month) {
		ReviewByMonthlyTimeAggregation agg = null;
		try {
			agg = yelpAggDataService.findMonthlyTimeAggregationByValue(category, month);
		} catch (AdvertisngAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return agg;
	}*/
	
    
    
    ///////
    @RequestMapping(value = "/location/{city}", method = RequestMethod.GET,
			produces = {"application/json", "application/xml" } )
    @ResponseBody
	public List<ReviewByLocationAggregation> findLocationAggregationByName(@PathVariable String city) {
    	List<ReviewByLocationAggregation> aggs = null;
		try {
			aggs = yelpAggDataService.findLocationAggregationByName(city);
		} catch (AdvertisngAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return aggs;
	}
    
    
    @RequestMapping(value = "/dailyTime/{startDate}-{endDate}", method = RequestMethod.GET,
			produces = {"application/json", "application/xml" } )
    @ResponseBody
	public List<ReviewByDailyTimeAggregation> findDailyTimeAggregationByValue(@PathVariable String  startDate, @PathVariable String endDate) {
    	List<ReviewByDailyTimeAggregation> aggs = null;
		try {
			aggs = yelpAggDataService.findDailyTimeAggregationByValue(startDate, endDate);
		} catch (AdvertisngAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return aggs;
	}
	
	
    @RequestMapping(value = "/monthlyTime/{startMonth}-{endMonth}", method = RequestMethod.GET,
			produces = {"application/json", "application/xml" } )
    @ResponseBody
	public List<ReviewByMonthlyTimeAggregation> findMonthlyTimeAggregationByValue(@PathVariable String startMonth, @PathVariable String endMonth) {
    	List<ReviewByMonthlyTimeAggregation> aggs = null;
		try {
			aggs = yelpAggDataService.findMonthlyTimeAggregationByValue(startMonth, endMonth);
		} catch (AdvertisngAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return aggs;
	}
}
