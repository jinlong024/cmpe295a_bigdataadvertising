package edu.sjsu.bigdata.advertising.controller.datacollection;

import edu.sjsu.bigdata.advertising.model.datacollector.common.SearchCriteria;
import edu.sjsu.bigdata.advertising.service.impl.datacollector.twitter.TwitterService;
import edu.sjsu.bigdata.advertising.service.impl.datacollector.twitter.TwitterServiceThread;
import jxl.write.WriteException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * This is the twitter data collection RESFFul web service controller. User: Lei
 * Zhang Date: November 13, 2013
 */

@Controller
@RequestMapping("/datacollections/twitters")
public class TwitterDataCollectionController {

	@Autowired
	private TwitterService twitterService;

	/**
	 * This method is used to trigger twitter query.
	 * 
	 * @param {category}/{manufacturer}/{name}/{model} : This is the twitter
	 *        search query criteria
	 * @return Return null
	 */

	@RequestMapping(value = "{category}/{manufacturer}/{name}/{model}", method = RequestMethod.GET)
	@ResponseBody
	public void collectFeedback(@PathVariable String category,
			@PathVariable String manufacturer, @PathVariable String name,
			@PathVariable String model) {

		// twitterService.init();

		System.out.println("Start to collect feedback: " + category + "/"
				+ manufacturer + "/" + name + "/" + model);

		int numThreads = 1;
		ExecutorService executor = Executors.newFixedThreadPool(numThreads);

		System.out.println("About to submit the background task");
		executor.execute(new TwitterServiceThread(twitterService, category,
				manufacturer, name, model));
		System.out.println("Submitted the background task");

		/*
		 * try { twitterService.search(); } catch (WriteException e) {
		 * System.out.println("Catch exception during twitter search " + e); }
		 * catch (IOException e) {
		 * System.out.println("Catch exception during twitter search " + e); }
		 */
		// return criteria;
	}

	/**
	 * This method is used to trigger twitter query.
	 * 
	 * @param criteria
	 *            : This is the twitter search query criteria
	 * @return Return null
	 */

	@RequestMapping(method = RequestMethod.POST, consumes = {
			"application/json", "application/xml" })
	@ResponseBody
	public void startSearch(
			@ModelAttribute("searchCriteria") SearchCriteria criteria,
			ModelMap model) {

		twitterService.init();

		System.out.println("Start to search " + criteria.getSearchContent());

		try {
			twitterService.search();
		} catch (WriteException e) {
			System.out.println("Catch exception during twitter search " + e);
		} catch (IOException e) {
			System.out.println("Catch exception during twitter search " + e);
		}
		// return criteria;
	}

}
