package edu.sjsu.bigdata.advertising.controller.usersecurity;

/**
 * User: Long Jin, Lei Zhang
 * Date: 9/10/13
 * Time: 4:15 PM
 */
import edu.sjsu.bigdata.advertising.exception.dao.AdvertisngAccessException;
import edu.sjsu.bigdata.advertising.model.security.HttpUser;
import edu.sjsu.bigdata.advertising.model.security.UserCredential;
import edu.sjsu.bigdata.advertising.service.security.IUserSecurityService;
import edu.sjsu.bigdata.advertising.service.security.UserSecurityServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.view.RedirectView;


@Controller
@RequestMapping("/login")
public class LoginController {

    @Autowired
    private IUserSecurityService securityService;

    @RequestMapping(method = RequestMethod.GET)
    public String getUserProfile(ModelMap model) {
        return "displayUserProfile";
    }

    @RequestMapping(method = RequestMethod.POST, consumes = {
            "application/x-www-form-urlencoded", "application/json", "application/xml" })
    public View authenticateUser(
            @ModelAttribute("userCredential") UserCredential userCredential , ModelMap model) throws AdvertisngAccessException {

        HttpUser httpUser =  securityService.authenticate(userCredential.getEmail(), userCredential.getPassword());

        if (httpUser.getAuthResult() == 1) {
            //verification pass, go to create adPrefProfile page
            return new RedirectView("/BigDataAdvertising/adPlanning");
        } else{
            //verification fail, go to login page again
            return new RedirectView("/BigDataAdvertising/login");
        }

    }



}