package edu.sjsu.bigdata.advertising.controller.usersecurity;

import edu.sjsu.bigdata.advertising.exception.dao.AdvertisngAccessException;
import edu.sjsu.bigdata.advertising.model.security.HttpUser;
import edu.sjsu.bigdata.advertising.model.security.UserCredential;
import edu.sjsu.bigdata.advertising.service.security.IUserSecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.support.AbstractMultipartHttpServletRequest;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.view.RedirectView;
import edu.sjsu.bigdata.advertising.model.security.User;
import edu.sjsu.bigdata.advertising.service.security.UserSecurityServiceImpl;

/**
 * Created with IntelliJ IDEA.
 * User: macbookpro
 * Date: 9/23/13
 * Time: 9:19 PM
 * To change this template use File | Settings | File Templates.
 */


@Controller

//@RequestMapping("/adminHomepage")
public class AdminHomeController {

    @Autowired
    private IUserSecurityService securityService;

    @RequestMapping(value = "/adminHomepage", method = RequestMethod.GET)
    public String getUserProfile(ModelMap model) throws AdvertisngAccessException {
        model.addAttribute("userList", securityService.getAllUsers());
        return "displayUserInfo";
    }
/*

    @RequestMapping(value = "/userHomepage/delete", method = RequestMethod.GET)
    public View deleteUser(@PathVariable("user.email") String email)  throws AdvertisngAccessException {
        securityService.deleteUser(email);
        return new RedirectView("/BigDataAdvertising/userHomepage");

    }
*/

    @RequestMapping(value = "/adminHomepage/delete", method = RequestMethod.GET)

    public View deleteUser
            (@RequestParam String email, ModelMap model)
            throws AdvertisngAccessException {
        securityService.deleteUser(email);
        return new RedirectView("/BigDataAdvertising/adminHomepage");

    }



   /* @RequestMapping(method = RequestMethod.POST, consumes = {
            "application/x-www-form-urlencoded", "application/json", "application/xml" })
    public View authenticateUser(
            @ModelAttribute("userCredential") UserCredential userCredential , ModelMap model) throws AdvertisngAccessException {

        HttpUser httpUser =  securityService.authenticate(userCredential.getEmail(), userCredential.getPassword());

        if (httpUser.getAuthResult() == 1) {
            //verification pass, go to create adPrefProfile page
            return new RedirectView("/BigDataAdvertising/adPrefProfiles");
        } else{
            //verification fail, go to login page again
            return new RedirectView("/BigDataAdvertising/login");
        }

    }

*/
   /*@RequestMapping(method = RequestMethod.POST, consumes = {
           "application/x-www-form-urlencoded", "application/json", "application/xml" })
   public View saveProfile(
           @ModelAttribute("adPrefProfiles") AdPrefProfile entity,
           ModelMap model) {

       if (StringUtils.hasText(entity.getId())) {
           adPrefProfileService.updateProfile(entity);
       } else {
           adPrefProfileService.saveProfile(entity);
       }
       return new RedirectView("/BigDataAdvertising/adPrefProfiles");
   }*/


    @RequestMapping(value = "/adminHomepage/findID", method = RequestMethod.GET)
    public View findUserByEmail(@RequestParam String email, ModelMap model)
            throws AdvertisngAccessException {
        securityService.getUserByEmail(email);
        return new RedirectView("editUserInfo");
    }


}