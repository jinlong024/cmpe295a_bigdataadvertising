package edu.sjsu.bigdata.advertising.controller.recommendation;

import edu.sjsu.bigdata.advertising.model.recommendation.AdPrefProfile;
import edu.sjsu.bigdata.advertising.service.interfaces.recommendation.AdPrefProfileService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.view.RedirectView;

/**
 * This is the advertisement preference profile RESFFul web service controller. 
 * Author: Lei Zhang 
 * Date: August 13, 2013
 */
@Controller
@RequestMapping("/adPrefProfiles")
public class AdPrefProfileController {

	@Autowired
	private AdPrefProfileService adPrefProfileService;
	
	/**
	 * This method is used to get list of existing Advertisement Preference
	 * Profiles.
	 * 
	 * @return Return NULL if the operation is failed; Return the list of
	 *         existing advertisement preference profiles if the operation is
	 *         successful.
	 * 
	 *         Client Example: Get All: GET /adPrefProfiles
	 */

    @RequestMapping(value = "/all", method = RequestMethod.GET)
	public String findAllProfiles(ModelMap model) {
		model.addAttribute("profileList", adPrefProfileService.findAllProfile());
		return "displayAdPrefProfile";
	}

    @RequestMapping(method = RequestMethod.GET)
    public String getEmptyProfile(ModelMap model) {
        //model.addAttribute("personList", personService.listPerson());
        return "displayAdPrefProfiles";
    }


    /**
	 * This method is used to insert or update Advertisement Preference Profile
	 * and return its ID.
	 * 
	 * @param entity
	 *            : This is the advertisement preference profile to be saved.
	 * @return Return null if the operation is failed; Return the profile ID if
	 *         the operation is successful.
	 * 
	 *         Client Example: Save: POST /recommendation/adprefprofile with the
	 *         advertisement preference profile Form/JSON/XML Update: PUT
	 *         /recommendation/adprefprofile with the advertisement preference
	 *         profile Form/JSON/XML
	 */
	@RequestMapping(method = RequestMethod.POST, consumes = {
			"application/x-www-form-urlencoded", "application/json", "application/xml" })
	public View saveProfile(
			@ModelAttribute("adPrefProfiles") AdPrefProfile entity,
			ModelMap model) {

		if (StringUtils.hasText(entity.getId())) {
			adPrefProfileService.updateProfile(entity);
		} else {
			adPrefProfileService.saveProfile(entity);
		}
		return new RedirectView("/BigDataAdvertising/adPrefProfiles");
	}

	/**
	 * This method is used to delete Advertisement Preference Profile by ID and
	 * return the result if the it has been deleted.
	 * 
	 * @param id
	 *            : This is the advertisement preference profile ID.
	 * @return Return false if the operation is failed; Return true if the
	 *         operation is successful.
	 * 
	 *         Client Example: Delete: DELETE /recommendation/adPrefProfile/{id}
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public View deleteProfile(@PathVariable String id, ModelMap model) {
		adPrefProfileService.deleteProfile(id);
		return new RedirectView("/BigDataAdvertising/adPrefProfiles");
	}

	/**
	 * This method is used to get Advertisement Preference Profile by ID.
	 * 
	 * @param id
	 *            : This is the advertisement preference profile ID.
	 * @return Return NULL if the operation is failed; Return the advertisement
	 *         preference profile if the operation is successful.
	 * 
	 *         Client Example: Get: GET /recommendation/adPrefProfile/{id}
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public View findProfileById(@PathVariable String id) {
		adPrefProfileService.findProfileById(id);

		return new RedirectView("/BigDataAdvertising/adPrefProfiles");
	}



}
