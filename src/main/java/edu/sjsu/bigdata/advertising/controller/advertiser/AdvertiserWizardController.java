package edu.sjsu.bigdata.advertising.controller.advertiser;

import edu.sjsu.bigdata.advertising.model.recommendation.AdPrefProfile;
import edu.sjsu.bigdata.advertising.service.interfaces.recommendation.AdPrefProfileService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.view.RedirectView;

/**
 * Created with IntelliJ IDEA.
 * User: macbookpro
 * Date: 10/3/13
 * Time: 9:41 AM
 * To change this template use File | Settings | File Templates.
 */

@Controller
@RequestMapping("/adPlanning")
public class AdvertiserWizardController {

    @Autowired
    private AdPrefProfileService adPrefProfileService;

    @RequestMapping(method = RequestMethod.GET)
    public String getEmptyProfile(ModelMap model) {
        return "adPlanning";
    }

    @RequestMapping( method = RequestMethod.POST, consumes = {
            "application/x-www-form-urlencoded", "application/json", "application/xml" })
    public View saveProfile(
            @ModelAttribute("adPrefProfiles") AdPrefProfile entity,
            ModelMap model) {

        if (StringUtils.hasText(entity.getId())) {
            adPrefProfileService.updateProfile(entity);
        } else {
            adPrefProfileService.saveProfile(entity);
        }
        return new RedirectView("/BigDataAdvertising/advHomepage");
    }





}
