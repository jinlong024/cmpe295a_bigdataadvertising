package edu.sjsu.bigdata.advertising.controller.datacollection;

import edu.sjsu.bigdata.advertising.service.impl.datacollector.twitter.TwitterService;
import edu.sjsu.bigdata.advertising.service.impl.datacollector.twitter.TwitterServiceThread;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


/**
 * Created with IntelliJ IDEA.
 * User: macbookpro
 * Date: 12/18/13
 * Time: 3:41 AM
 * To change this template use File | Settings | File Templates.
 */

@Controller
public class TwitterCollectionDisplayController {

    @Autowired
    private TwitterService twitterService;

    @RequestMapping(value = "/TwitterDataCollection", method = RequestMethod.GET)
    public String findAllProfiles(ModelMap model) {
        return "dataCollection";
    }

//TODO what if failure
    @RequestMapping(value = "/collect", method = RequestMethod.POST, consumes = {
            "application/x-www-form-urlencoded", "application/json", "application/xml"})
    public String TwitterCollection(
            @RequestParam("category") String category,
            @RequestParam("manufacturer") String manufacturer,
            @RequestParam("name") String name,
            @RequestParam("model") String model
    ) {
        int numThreads = 1;
        ExecutorService executor = Executors.newFixedThreadPool(numThreads);
        executor.execute(new TwitterServiceThread(twitterService, category,
                manufacturer, name, model));
        return "adPlanning";
    }

}
