package edu.sjsu.bigdata.advertising.controller.advertiser;

import edu.sjsu.bigdata.advertising.exception.dao.AdvertisngAccessException;
import edu.sjsu.bigdata.advertising.model.feedbackAggregation.DailyFeedbackAggregation;
import edu.sjsu.bigdata.advertising.model.feedbackAggregation.HourlyFeedbackAggregation;
import edu.sjsu.bigdata.advertising.service.datacollector.yelp.IYelpDataService;
import edu.sjsu.bigdata.advertising.service.datacollector.yelp.aggregation.ReviewByBusinessAggregation;
import edu.sjsu.bigdata.advertising.service.datacollector.yelp.aggregation.ReviewByCategoryAggregation;
import edu.sjsu.bigdata.advertising.service.datacollector.yelp.aggregation.ReviewByLocationAggregation;
import edu.sjsu.bigdata.advertising.service.interfaces.feedbackAggregation.FeedbackAggregationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.view.RedirectView;


import java.io.IOException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: macbookpro
 * Date: 12/8/13
 * Time: 12:30 PM
 * To change this template use File | Settings | File Templates.
 */


@Controller
@RequestMapping("/adEvaluation")
public class YelpController {
    private ReviewByLocationAggregation reviewByLocationAggregation;
    @Autowired
    private IYelpDataService yelpDataService;
    @Autowired
    private FeedbackAggregationService feedbackAggregationService;


/*
    @Autowired
    private IYelpDataService yelpDataService;
*/
//TODO all input is required, need to check that

    @RequestMapping(value = "yelp/category", method = RequestMethod.GET)
    public String getYelpCategory(ModelMap model) throws IOException {

        List<ReviewByCategoryAggregation> list = null;

        try {
            list = yelpDataService.getAllYelpCategoryAggregationData();
        } catch (AdvertisngAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        model.addAttribute("yelp", list);
        return "displayYelpCategory";
    }


    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView getYelpLocation(ModelMap model) throws IOException {
        List<ReviewByCategoryAggregation> list = null;

        try {
            list = yelpDataService.getAllYelpCategoryAggregationData();
        } catch (AdvertisngAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        model.addAttribute("yelp", list);

        ModelAndView myModel = new ModelAndView("selectYelpInput");

        return myModel;

    }

    @RequestMapping(value = "/yelp/location/show", method = RequestMethod.POST, consumes = {
            "application/x-www-form-urlencoded", "application/json", "application/xml"})
    public String twitterNavi(
            @RequestParam("datasource") String datasource,
            @RequestParam("timeunit") String timeunit,
            @RequestParam("dimension") String dimension,
            @RequestParam("city") String city,
            @RequestParam("timerange") String timerange,
            ModelMap model) throws IOException {


        if (timeunit.equalsIgnoreCase("Daily")) {
            List<DailyFeedbackAggregation> dailyFeedbackAggregations = feedbackAggregationService.findAllDailyFeedbackAggregation();
            model.addAttribute("feedback", dailyFeedbackAggregations);

            return "displayTwitterDailyPiechart";

        }


        if (dimension.equalsIgnoreCase("Category")) {


            List<ReviewByCategoryAggregation> list = null;

            try {
                list = yelpDataService.getAllYelpCategoryAggregationData();
            } catch (AdvertisngAccessException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            model.addAttribute("yelp", list);
            return "displayYelpCategory";
        }

        if (timeunit.equalsIgnoreCase("Hourly") && datasource.equalsIgnoreCase("Twitter")) {
            List<HourlyFeedbackAggregation> hourlyFeedbackAggregations = feedbackAggregationService.findAllHourlyFeedbackAggregation();
            model.addAttribute("feedback", hourlyFeedbackAggregations);
            return "displayTwitterhourlyPiechart";
        }


        if (dimension.equalsIgnoreCase("Location") && datasource.equalsIgnoreCase("Yelp")       )  {

            List<ReviewByLocationAggregation> agg = null;


            try {
                agg = yelpDataService.findLocationAggregationByName(city);
            } catch (AdvertisngAccessException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            model.addAttribute("yelpcity", agg);

//            return "displayYelpCity";

            return "displayYelpCity";

        }
/*
        else
            return new RedirectView("/BigDataAdvertising/Twitter/hourly");
*/
        else
            return "displayTwitterDailyPiechart";


    }

/*

    @RequestMapping(value = "yelp/location", method = RequestMethod.GET)
    public String TwitterLocation(ModelMap model) throws IOException {

*/
/*        ReviewByLocationAggregation agg = null;
        try {
            agg = yelpDataService.findLocationAggregationByName(city);
        } catch (AdvertisngAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        model.addAttribute("city", agg);
    ObjectMapper mapper = new ObjectMapper();*//*



        model.addAttribute("citysent", );


        return "displayYelpCity";
    }
*/


}


/*
<nav class="navbar navbar-custom navbar-static-top">
<div class="navbar-header">
<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a class="navbar-brand" href="#">Title</a>
</div>
<div class="collapse navbar-collapse">
<ul class="nav navbar-nav">
<li class="active"><a href="#">Home</a></li>
<li><a href="#">Link</a></li>
<li><a href="#">Link</a></li>
</ul>
<form class="navbar-form navbar-left" role="search">
<div class="form-group">
<input type="text" class="form-control" placeholder="Search">
</div>
<button type="submit" class="btn btn-default">Submit</button>
</form>
<ul class="nav navbar-nav navbar-right">

*/



