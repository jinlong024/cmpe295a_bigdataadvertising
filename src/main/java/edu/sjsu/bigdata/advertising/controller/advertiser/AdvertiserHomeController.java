package edu.sjsu.bigdata.advertising.controller.advertiser;

import edu.sjsu.bigdata.advertising.model.recommendation.AdPrefProfile;
import edu.sjsu.bigdata.advertising.service.interfaces.recommendation.AdPrefProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.view.RedirectView;

/**
 * Created with IntelliJ IDEA.
 * User: macbookpro
 * Date: 9/27/13
 * Time: 10:28 AM
 * To change this template use File | Settings | File Templates.
 */

@Controller
//@RequestMapping("/advHomepage")
public class AdvertiserHomeController {

    @Autowired
    private AdPrefProfileService adPrefProfileService;

    @RequestMapping(value = "/advHomepage", method = RequestMethod.GET)
    public String findAllProfiles(ModelMap model) {
        model.addAttribute("profileList", adPrefProfileService.findAllProfile());
        return "displayAdInfo";
    }

    @RequestMapping(value = "/advHomepage/delete", method = RequestMethod.GET)

    public View deleteProfile
            (@RequestParam String id, ModelMap model)
    {
         adPrefProfileService.deleteProfile(id);
        return new RedirectView("/BigDataAdvertising/advHomepage");
    }

    @RequestMapping(value = "/advHomepage/getRecommend", method = RequestMethod.GET)

    public View getRecommend
            (@RequestParam String id, ModelMap model)
    {
        adPrefProfileService.findProfileById(id);
        return new RedirectView("/BigDataAdvertising/map");
    }


    //TODO complete update function



    @RequestMapping(value = "/advHomepage/update", method = RequestMethod.POST, consumes = {
            "application/x-www-form-urlencoded", "application/json", "application/xml" })
    public View saveProfile(
            @ModelAttribute("adPrefProfiles") AdPrefProfile entity,
            ModelMap model) {

        if (StringUtils.hasText(entity.getId())) {
            adPrefProfileService.updateProfile(entity);
        } else {
            adPrefProfileService.saveProfile(entity);
        }
        return new RedirectView("/BigDataAdvertising/advHomepage");
    }

//    @Autowired
//    private AdPrefProfileService adPrefProfileService;
//
//    @RequestMapping(value ="/advHomepage", method = RequestMethod.GET)
//    public String getUserProfile(ModelMap model)  throws AdvertisngAccessException {
//        model.addAttribute("userList", securityService.getAllUsers());
//        return "displayUserInfo";
//    }
//
//
//
//    @RequestMapping(value = "/userHomepage/delete", method = RequestMethod.GET)
//
//    public View deleteUser
//            (@ModelAttribute String email, ModelMap model)
//            throws AdvertisngAccessException {
//        securityService.deleteUser(email);
//        return new RedirectView("/BigDataAdvertising/userHomepage");
//
//    }
//
//


}