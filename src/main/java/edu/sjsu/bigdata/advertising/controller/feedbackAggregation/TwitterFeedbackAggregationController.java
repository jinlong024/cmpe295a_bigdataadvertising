package edu.sjsu.bigdata.advertising.controller.feedbackAggregation;

import edu.sjsu.bigdata.advertising.model.feedbackAggregation.DailyFeedbackAggregation;
import edu.sjsu.bigdata.advertising.model.feedbackAggregation.HourlyFeedbackAggregation;
import edu.sjsu.bigdata.advertising.model.feedbackAggregation.MonthlyFeedbackAggregation;
import edu.sjsu.bigdata.advertising.service.interfaces.feedbackAggregation.FeedbackAggregationService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * This is the twitter feedback aggregation RESFFul web service controller.
 * It will call Twitter Feedback Aggregation Data service to get related aggregation data.
 * User: Lei Zhang
 * Date: August 13, 2013
 */
@Controller
@RequestMapping("/feedbackAggregation/twitters")
public class TwitterFeedbackAggregationController {

    @Autowired
    private FeedbackAggregationService feedbackAggregationService;
    
    
    /**
     * This is used to aggregate monthly feedback.
     */
    @RequestMapping(value = "/monthly",method = RequestMethod.GET)
    public void aggregationMonthlyFeedback(ModelMap model) {
    	
    	Date from = new Date();
    	Date to = new Date();
       feedbackAggregationService.aggregationMonthlyFeedback(from.toString(), to.toString());;

    }
    
    /**
     * This is used to aggregate daily feedback.
     */
    @RequestMapping(value = "/daily",method = RequestMethod.GET)
    public void aggregationDailyFeedback(ModelMap model) {
    	
    	Date from = new Date();
    	Date to = new Date();
       feedbackAggregationService.aggregationDailyFeedback(from.toString(), to.toString());;

    }
    
    /**
     * This is used to aggregate hourly feedback.
     */
    @RequestMapping(value = "/hourly",method = RequestMethod.GET)
    public void aggregationHourlyFeedback(ModelMap model) {
    	
    	Date from = new Date();
    	Date to = new Date();
       feedbackAggregationService.aggregationHourlyFeedback(from.toString(), to.toString());;

    }

    
    @RequestMapping(value = "/monthlyTime",method = RequestMethod.GET)
    @ResponseBody
    public List<MonthlyFeedbackAggregation> findAllMonthlyFeedbackAggregation(ModelMap model) {
        return feedbackAggregationService.findAllMonthlyFeedbackAggregation();

    }
    
    @RequestMapping(value = "/monthlyTime/{category}",method = RequestMethod.GET)
    @ResponseBody
    public List<MonthlyFeedbackAggregation> findMonthlyFeedbackAggregationByCategory(@PathVariable String category) {
        return feedbackAggregationService.findMonthlyFeedbackAggregationByCategory(category);

    }

    @RequestMapping(value = "/dailyTime",method = RequestMethod.GET)
    @ResponseBody
    public List<DailyFeedbackAggregation> findAllDailyFeedbackAggregation(ModelMap model) {
        return feedbackAggregationService.findAllDailyFeedbackAggregation();

    }
    
    @RequestMapping(value = "/dailyTime/{category}",method = RequestMethod.GET)
    @ResponseBody
    public List<DailyFeedbackAggregation> findDailyFeedbackAggregationByCategory(@PathVariable String category) {
        return feedbackAggregationService.findDailyFeedbackAggregationByCategory(category);

    }

    @RequestMapping(value = "/hourlyTime",method = RequestMethod.GET)
    @ResponseBody
    public List<HourlyFeedbackAggregation> findAllHourlyFeedbackAggregation(ModelMap model) {
        return feedbackAggregationService.findAllHourlyFeedbackAggregation();

    }
    
    @RequestMapping(value = "/hourlyTime/{category}",method = RequestMethod.GET)
    @ResponseBody
    public List<HourlyFeedbackAggregation> findHourlyFeedbackAggregationByCategory(@PathVariable String category) {
        return feedbackAggregationService.findHourlyFeedbackAggregationByCategory(category);

    }

}
