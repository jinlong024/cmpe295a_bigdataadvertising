package edu.sjsu.bigdata.advertising.controller.recommendation;

import edu.sjsu.bigdata.advertising.model.recommendation.AdCondition;
import edu.sjsu.bigdata.advertising.model.recommendation.RecommendationResult;
import edu.sjsu.bigdata.advertising.service.interfaces.recommendation.RecommendationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.view.RedirectView;


/**
 * This is the advertising recommendation RESFFul web service controller.
 * User: Lei Zhang
 * Date: Sep. 13, 2013
 */
@Controller
@RequestMapping("/adRecommendations")
public class RecommendationController {

    @Autowired
    private RecommendationService recommendationService;

    @RequestMapping(method = RequestMethod.GET)
    public String getEmptyProfile(ModelMap model) {
        return "displayRecommendation";
    }


    /**
     * This method is used to get recommendation based on the Advertisement Preference Profile ID.
     *
     * @param id: This is the advertisement preference profile ID.
     * @return Return NULL if the operation is failed;
     *         Return the recommendation result if the operation is successful.
     *         <p/>
     *         Client Example:
     *         Get: GET /recommendation/{id}
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public RecommendationResult getRecommendationById(@PathVariable String id) {

        System.out.println("Request Recommendation for profile Id: " + id);
       RecommendationResult result = recommendationService.getRecommendation(id);

        System.out.println("Recommendation Result is: " + result.getDemographicProfileId());
        return  result;
    }

    /*
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public View getRecommendationById(@RequestParam String id) {
        recommendationService.getRecommendation(id);
        return new RedirectView("/BigDataAdvertising/recommendation");
    }
    */

    /**
     * This method is used to get recommendation based on the Advertisement Preference Profile ID.
     *
     * @param condition: This is the advertisement conditions which include a list of Advertisement Preference Profile ID
     *                   and the location to display an advertisement.
     * @return Return NULL if the operation is failed;
     *         Return the recommendation result if the operation is successful.
     *         <p/>
     *         Client Example:
     *         Get: GET /recommendation/  with the advertising conditions Form/JSON/XML
     */
    @RequestMapping(value = "/adCondition", method = RequestMethod.GET)
    public View getRecommendation(@ModelAttribute("adCondition") AdCondition condition) {
        recommendationService.getRecommendation(condition);

        return new RedirectView("/BigDataAdvertising/recommendation");
    }

}
