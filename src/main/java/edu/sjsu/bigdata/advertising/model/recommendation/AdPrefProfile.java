package edu.sjsu.bigdata.advertising.model.recommendation;

import edu.sjsu.bigdata.advertising.model.common.Product;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * This is the advertisement preference profile bean.
 *
 * @author: Lei Zhang
 * Creation date: (08/12/2013)
 */
@Document
@XmlRootElement(name = "adPrefProfile")
public class AdPrefProfile extends Product{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
     * This is the unique id generated automatically by spring data.
     */
    @Id
    private String id;

    /**
     * This is the advertisement Id.
     */
    @Field("ad_id")
    private String adId;

    /**
     * This is the target age range for this advertisement.
     */
    @Field("age")
    private String age;

    /**
     * This is the target education level for this advertisement.
     */
    @Field("education")
    private String education;

    /**
     * This is the target gender type for this advertisement.
     */
    @Field("sex")
    private String sex;

    /**
     * This is the target income range for this advertisement.
     */
    @Field("income")
    private String income;

    /**
     * This is the target weather range for this advertisement.
     */
    @Field("weather")
    private String weather;

    /**
     * This is the target temperature range for this advertisement.
     */
    @Field("temperature")
    private String temperature;

    /**
     * This is the target ethnicity type for this advertisement.
     */
    @Field("ethnicity")
    private String ethnicity;

    /**
     * This is the target race type for this advertisement.
     */
    @Field("race")
    private String race;

    /**
     * This is the target race type for this advertisement.
     */
    @Field("population")
    private String population;

    /**
     * This is the target number of the house range for this advertisement.
     */
    @Field("house")
    private String house;
    
    /**
     * This is the target age range for this advertisement.
     */
    @Field("age_weight")
    private Double ageWeight = 0.0;

    /**
     * This is the target education level for this advertisement.
     */
    @Field("education_weight")
    private Double educationWeight = 0.0;

    /**
     * This is the target gender type for this advertisement.
     */
    @Field("sex_weight")
    private Double sexWeight = 0.0;

    /**
     * This is the target income range for this advertisement.
     */
    @Field("income_weight")
    private Double incomeWeight = 0.0;

    /**
     * This is the target weather range for this advertisement.
     */
    @Field("weather_weight")
    private Double weatherWeight = 0.0;

    /**
     * This is the target temperature range for this advertisement.
     */
    @Field("temperature_weight")
    private Double temperatureWeight = 0.0;

    /**
     * This is the target ethnicity type for this advertisement.
     */
    @Field("ethnicity_weight")
    private Double ethnicityWeight = 0.0;

    /**
     * This is the target race type for this advertisement.
     */
    @Field("race_weight")
    private Double raceWeight = 0.0;

    public Double getAgeWeight() {
        return ageWeight;
    }

    public void setAgeWeight(Double ageWeight) {
        this.ageWeight = ageWeight;
    }

    public Double getEducationWeight() {
        return educationWeight;
    }

    public void setEducationWeight(Double educationWeight) {
        this.educationWeight = educationWeight;
    }

    public Double getSexWeight() {
        return sexWeight;
    }

    public void setSexWeight(Double sexWeight) {
        this.sexWeight = sexWeight;
    }

    public Double getIncomeWeight() {
        return incomeWeight;
    }

    public void setIncomeWeight(Double incomeWeight) {
        this.incomeWeight = incomeWeight;
    }

    public Double getWeatherWeight() {
        return weatherWeight;
    }

    public void setWeatherWeight(Double weatherWeight) {
        this.weatherWeight = weatherWeight;
    }

    public Double getTemperatureWeight() {
        return temperatureWeight;
    }

    public void setTemperatureWeight(Double temperatureWeight) {
        this.temperatureWeight = temperatureWeight;
    }

    public Double getEthnicityWeight() {
        return ethnicityWeight;
    }

    public void setEthnicityWeight(Double ethnicityWeight) {
        this.ethnicityWeight = ethnicityWeight;
    }

    public Double getRaceWeight() {
        return raceWeight;
    }

    public void setRaceWeight(Double raceWeight) {
        this.raceWeight = raceWeight;
    }

    public Double getPopulationWeight() {
        return populationWeight;
    }

    public void setPopulationWeight(Double populationWeight) {
        this.populationWeight = populationWeight;
    }

    public Double getHouseWeight() {
        return houseWeight;
    }

    public void setHouseWeight(Double houseWeight) {
        this.houseWeight = houseWeight;
    }

    /**

     * This is the target race type for this advertisement.
     */
    @Field("population_weight")
    private Double populationWeight = 0.0;

    /**
     * This is the target number of the house range for this advertisement.
     */
    @Field("house_weight")
    private Double houseWeight = 0.0;


	public String getId() {
        return id;
    }

    public String getAdId() {
        return adId;
    }

    public String getAge() {
        return age;
    }

    public String getEducation() {
        return education;
    }


    public void setId(String id) {
        this.id = id;
    }

    public void setAdId(String adId) {
        this.adId = adId;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getIncome() {
        return income;
    }

    public void setIncome(String income) {
        this.income = income;
    }

    public String getWeather() {
        return weather;
    }

    public void setWeather(String weather) {
        this.weather = weather;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getEthnicity() {
        return ethnicity;
    }

    public void setEthnicity(String ethnicity) {
        this.ethnicity = ethnicity;
    }

    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }

    public String getPopulation() {
        return population;
    }

    public void setPopulation(String population) {
        this.population = population;
    }

    public String getHouse() {
        return house;
    }

    public void setHouse(String house) {
        this.house = house;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public void setEducation(String education) {
        this.education = education;
    }

}
