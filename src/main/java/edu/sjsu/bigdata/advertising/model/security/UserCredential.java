/**
 * 
 */
package edu.sjsu.bigdata.advertising.model.security;

import javax.xml.bind.annotation.XmlElement;



/**
 * @author Lei Zhang
 *
 */
public class UserCredential  {
	
	private String email;
	private String password;
	
	@XmlElement
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	@XmlElement
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
}
