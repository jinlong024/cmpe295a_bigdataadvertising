package edu.sjsu.bigdata.advertising.model.recommendation;

/**
 * This is the advertisement recommendation result bean.
 *
 * @author: Lei Zhang
 * Creation date: (08/12/2013)
 */
public class RecommendationResult {
	
	/**
     * This is a advertisement Id to recommend in one location.
     */
	String adId; 
	
	/**
     * This is the recommended demographic profile Id for an advertisement.
     */
	String demographicProfileId;
	
    public String getAdId() {
		return adId;
	}

	public void setAdId(String adId) {
		this.adId = adId;
	}

	public String getDemographicProfileId() {
		return demographicProfileId;
	}

	public void setDemographicProfileId(String demographicProfileId) {
		this.demographicProfileId = demographicProfileId;
	}


	
}
