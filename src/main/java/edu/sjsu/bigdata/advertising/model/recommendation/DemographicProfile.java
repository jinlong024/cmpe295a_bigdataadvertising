package edu.sjsu.bigdata.advertising.model.recommendation;

import edu.sjsu.bigdata.advertising.model.common.Location;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * This is the demographic profile bean.
 * 
 * @author: Lei Zhang Creation date: (08/12/2013)
 */
@Document
@XmlRootElement(name = "demographicProfile")
public class DemographicProfile implements Serializable {

	/**
	 * This is automated generated serial version UID.
	 */
	private static final long serialVersionUID = -4362172785450010243L;

	/**
	 * This is the unique id generated automatically by spring data.
	 */
	@Id
	private String id;

	/**
	 * This is the demographic country.
	 */
	@Field("location")
	private Location location;

	/**
	 * This is the demographic age range in the location.
	 */
	@Field("age")
	private String age;

	/**
	 * This is the demographic education level in the location.
	 */
	@Field("education")
	private String education;

	/**
	 * This is the demographic gender type in the location.
	 */
	@Field("gender")
	private String sex;

	/**
	 * This is the demographic income range in the location.
	 */
	@Field("income")
	private String income;

	/**
	 * This is the demographic weather range in the location.
	 */
	@Field("weather")
	private String weather;

	/**
	 * This is the demographic temperature range in the location.
	 */
	@Field("temperature")
	private String temperature;

	/**
	 * This is the demographic ethnicity type in the location.
	 */
	@Field("ethnicity")
	private String ethnicity;

	/**
	 * This is the demographic race type in the location.
	 */
	@Field("race")
	private String race;

	/**
	 * This is the demographic race type in the location.
	 */
	@Field("population")
	private String population;

	/**
	 * This is the demographic number of the house range in the location.
	 */
	@Field("house")
	private String house;

	public String getId() {
		return id;
	}

	public String getAge() {
		return age;
	}

	public String getEducation() {
		return education;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getIncome() {
		return income;
	}

	public void setIncome(String income) {
		this.income = income;
	}

	public String getWeather() {
		return weather;
	}

	public void setWeather(String weather) {
		this.weather = weather;
	}

	public String getTemperature() {
		return temperature;
	}

	public void setTemperature(String temperature) {
		this.temperature = temperature;
	}

	public String getEthnicity() {
		return ethnicity;
	}

	public void setEthnicity(String ethnicity) {
		this.ethnicity = ethnicity;
	}

	public String getRace() {
		return race;
	}

	public void setRace(String race) {
		this.race = race;
	}

	public String getPopulation() {
		return population;
	}

	public void setPopulation(String population) {
		this.population = population;
	}

	public String getHouse() {
		return house;
	}

	public void setHouse(String house) {
		this.house = house;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public void setEducation(String education) {
		this.education = education;
	}

}
