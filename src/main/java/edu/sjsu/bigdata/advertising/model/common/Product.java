package edu.sjsu.bigdata.advertising.model.common;


import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;


/**
 * This is the advertising product bean.
 *
 * @author: Lei Zhang
 * Creation date: (08/12/2013)
 */
@Document
@XmlRootElement(name = "product")
public class Product implements Serializable {

    /**
     * This is automated generated serial version UID.
     */
    protected static final long serialVersionUID = -4362172785450010243L;

    @Field("product_category")
    public String productCategory;

    @Field("product_manufacturer")
    public String productManufacturer;

    @Field("product_name")
    public String productName;

    @Field("product_model")
    public String productModel;



    public String getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(String productCategory) {
        this.productCategory = productCategory;
    }

    public String getProductManufacturer() {
        return productManufacturer;
    }

    public void setProductManufacturer(String productManufacturer) {
        this.productManufacturer = productManufacturer;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductModel() {
        return productModel;
    }

    public void setProductModel(String productModel) {
        this.productModel = productModel;
    }

}
