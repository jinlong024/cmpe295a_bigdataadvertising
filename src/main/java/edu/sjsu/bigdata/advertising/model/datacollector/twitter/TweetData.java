package edu.sjsu.bigdata.advertising.model.datacollector.twitter;

/**
 * This is the collected tweet information bean.
 * 
 * @author: Lei Zhang Creation date: (08/28/2013)
 */
public class TweetData {

	public String createTime;

	public String screenName;

	public String text;

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getScreenName() {
		return screenName;
	}

	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

}