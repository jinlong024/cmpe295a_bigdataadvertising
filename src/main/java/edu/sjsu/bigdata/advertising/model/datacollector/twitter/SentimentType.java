package edu.sjsu.bigdata.advertising.model.datacollector.twitter;


/**
 * This is the sentiment type bean.
 * 
 * @author: Lei Zhang
 * @Creation date: (08/28/2013)
 */
public enum SentimentType {
	
	Neutral("Neutral"), Positive("Positive"), Negative("Negative");

	private String status;

	/**
	 * @param status
	 */
	private SentimentType(String status) {
		this.status = status;
	}

	public String getStatus() {
		return status;
	}

	public static boolean contains(String newStatus) {

		for (SentimentType myStatus : SentimentType.values()) {
			if (myStatus.name().equalsIgnoreCase(newStatus)) {
				return true;
			}
		}

		return false;
	}

	public  void setStatus(String newStatus) {

		if (contains(newStatus)) {
			this.status = newStatus;
		} else {
			this.status = null;
		}
	}

	public String toString() {
		return status;
	}
}
