package edu.sjsu.bigdata.advertising.model.feedbackAggregation;

import edu.sjsu.bigdata.advertising.model.common.Location;
import edu.sjsu.bigdata.advertising.model.common.Product;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * This is the advertising feedback bean.
 * User: Lei Zhang
 * Date: 12-1-2013
 */
@Document
@XmlRootElement(name = "adFeedback")
public class Feedback extends Product  {


    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
     * This is the unique id generated automatically by mongoDB.
     */
    @Id
    private String id;

    @Field("location")
    public Location location;

    @Field("user_name")
    public String userName;

    @Field("time_stamp")
    public String timeStamp;

    @Field("feedback_level")
    public String feedbackLevel;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getFeedbackLevel() {
        return feedbackLevel;
    }

    public void setFeedbackLevel(String feedbackLevel) {
        this.feedbackLevel = feedbackLevel;
    }



}
