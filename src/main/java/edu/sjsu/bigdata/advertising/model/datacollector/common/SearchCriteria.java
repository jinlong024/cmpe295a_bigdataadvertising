package edu.sjsu.bigdata.advertising.model.datacollector.common;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * This is the advertising feedback search criteria bean.
 * User: lei zhang
 * Date: 11-28-2013
 */
@XmlRootElement(name = "searchCriteria")
public class SearchCriteria implements Serializable {


    /**
     * This is automated generated serial version UID.
     */
    private static final long serialVersionUID = -4362172785450010243L;

    private String searchContent;

    public String getSearchContent() {
        return searchContent;
    }

    public void setSearchContent(String searchContent) {
        this.searchContent = searchContent;
    }
}
