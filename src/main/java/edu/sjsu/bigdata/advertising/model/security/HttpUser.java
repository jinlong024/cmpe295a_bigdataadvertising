/**
 * 
 */
package edu.sjsu.bigdata.advertising.model.security;

import java.io.Serializable;

/**
 * @author yuxue
 *
 */
public class HttpUser implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private User user;
	private Integer authResult;
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}


	public Integer getAuthResult() {
		return authResult;
	}

	public void setAuthResult(Integer authResult) {
		this.authResult = authResult;
	}
	
	
}
