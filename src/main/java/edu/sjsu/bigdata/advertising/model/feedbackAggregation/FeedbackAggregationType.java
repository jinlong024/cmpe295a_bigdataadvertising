package edu.sjsu.bigdata.advertising.model.feedbackAggregation;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * This is the product feedback aggregation type bean.
 * User: Lei Zhang
 * Date: 12-1-2013
 */
@Document
@XmlRootElement(name = "feedback_aggregation_type")
public class FeedbackAggregationType implements Serializable {


    /**
     * This is automated generated serial version UID.
     */
    private static final long serialVersionUID = -4362172785450010243L;

    /**
     * This is the unique id generated automatically by mongoDB.
     */
    @Id
    private String id;

    @Field("aggregation_type")
    public String aggregationType;

    @Field("aggregation_type_description")
    public String aggregationTypeDescription;


}
