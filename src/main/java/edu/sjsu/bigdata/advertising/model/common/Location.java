package edu.sjsu.bigdata.advertising.model.common;


import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.xml.bind.annotation.XmlRootElement;


/**
 * This is the location bean.
 *
 * @author: Lei Zhang
 * Creation date: (08/12/2013)
 */
@Document
@XmlRootElement(name = "location")
public class Location {
	
    /**
     * This is the country.
     */
    @Field("country")
    private String country;
    
    /**
     * This is the  state.
     */
    @Field("state")
    private String state;
    
    /**
     * This is the  city.
     */
    @Field("city")
    private String city;
    
    public String getCountry() {
  		return country;
  	}


  	public void setCountry(String country) {
  		this.country = country;
  	}


  	public String getState() {
  		return state;
  	}


  	public void setState(String state) {
  		this.state = state;
  	}


  	public String getCity() {
  		return city;
  	}


  	public void setCity(String city) {
  		this.city = city;
  	}

}
