package edu.sjsu.bigdata.advertising.model.feedbackAggregation;

import edu.sjsu.bigdata.advertising.model.common.Product;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * This is the product feedback aggregation base bean.
 * User: Lei Zhang
 * Date: 12-1-2013
 */
@Document
@XmlRootElement(name = "feedbackAggregationBase")
public class FeedbackAggregationBase extends Product {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
     * This is the unique id generated automatically by mongoDB.
     */
    @Id
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Field("user_count")
    public Integer userCount;

    @Field("feedbackLevelAggregation")
    public FeedbackLevelDistribution feedbackLevel;

    @Field("time_stamp")
    public String timeStamp;

    public Integer getUserCount() {
        return userCount;
    }

    public void setUserCount(Integer userCount) {
        this.userCount = userCount;
    }

    public FeedbackLevelDistribution getFeedbackLevel() {
        return feedbackLevel;
    }

    public void setFeedbackLevel(FeedbackLevelDistribution feedbackLevel) {
        this.feedbackLevel = feedbackLevel;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }
}
