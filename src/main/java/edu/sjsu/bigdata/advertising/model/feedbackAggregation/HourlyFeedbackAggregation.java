package edu.sjsu.bigdata.advertising.model.feedbackAggregation;

import org.springframework.data.mongodb.core.mapping.Document;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * This is the hourly feedback aggregation bean.
 * User: Lei Zhang
 * Date: 12-1-2013
 */
@Document
@XmlRootElement(name = "hourlyFeedbackAggregation")
public class HourlyFeedbackAggregation extends FeedbackAggregationBase {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


}
