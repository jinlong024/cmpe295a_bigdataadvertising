package edu.sjsu.bigdata.advertising.model.recommendation;

import edu.sjsu.bigdata.advertising.model.common.Location;

import java.util.List;

/**
 * This is the advertisement recommendation condition bean.
 *
 * @author: Lei Zhang
 * Creation date: (08/12/2013)
 */
public class AdCondition {
	
    /**
     * This is a list of advertisement Id to identify in one location.
     */
	List<String> adIds; 
	
	/**
     * This is the location to recommend from a list of advertisement.
     */
	Location location;
	
	public List<String> getAdIds() {
		return adIds;
	}

	public void setAdIds(List<String> adIds) {
		this.adIds = adIds;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}



}
