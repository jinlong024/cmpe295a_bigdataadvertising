/**
 * 
 */
package edu.sjsu.bigdata.advertising.model.security;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;



@XmlRootElement(name = "role")
/**
 * @author yunxue
 *
 */

@Document(collection = "roles")
public class Role implements Serializable {
	
	private static final long serialVersionUID = 1L;
	@Id
	private String roleId;
	
	private String role;
	private String description;
	
	
	@XmlElement
	public String getRoleId() {
		return roleId;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	
	
	@XmlElement
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	
	
	@XmlElement
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	

}
