package edu.sjsu.bigdata.advertising.model.feedbackAggregation;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * This is the feedback level distribution bean.
 * User: Lei Zhang
 * Date: 12-1-2013
 */
@Document
@XmlRootElement(name = "feedbackLevel")
public class FeedbackLevelDistribution   {

    @Field("STRONGLY_DISLIKE")
    public Integer stronglyDislikeCount;

    @Field("DISLIKE")
    public Integer dislikeCount;

    @Field("NEUTRAL")
    public Integer neutralCount;

    @Field("LIKE")
    public Integer likeCount;

    @Field("STRONGLY_LIKE")
    public Integer stronglyLikeCount;

    public Integer getStronglyDislikeCount() {
        return stronglyDislikeCount;
    }

    public void setStronglyDislikeCount(Integer stronglyDislikeCount) {
        this.stronglyDislikeCount = stronglyDislikeCount;
    }

    public Integer getDislikeCount() {
        return dislikeCount;
    }

    public void setDislikeCount(Integer dislikeCount) {
        this.dislikeCount = dislikeCount;
    }

    public Integer getNeutralCount() {
        return neutralCount;
    }

    public void setNeutralCount(Integer neutralCount) {
        this.neutralCount = neutralCount;
    }

    public Integer getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(Integer likeCount) {
        this.likeCount = likeCount;
    }

    public Integer getStronglyLikeCount() {
        return stronglyLikeCount;
    }

    public void setStronglyLikeCount(Integer stronglyLikeCount) {
        this.stronglyLikeCount = stronglyLikeCount;
    }
}
