package edu.sjsu.bigdata.advertising.webservice.recommendation;

import edu.sjsu.bigdata.advertising.model.recommendation.AdPrefProfile;
import edu.sjsu.bigdata.advertising.service.interfaces.recommendation.AdPrefProfileService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * This is the advertisement preference profile RESFFul web service.
 * User: Lei Zhang
 * Date: August 13, 2013
 */
@Controller
@RequestMapping("ws/adPrefProfile")
public class AdPrefProfileWS {

	@Autowired
    private AdPrefProfileService adPrefProfileService;

    /**
     * This method is used to insert or update Advertisement Preference Profile and return its ID.
     *
     * @param entity: This is the advertisement preference profile to be saved.
     * @return Return null if the operation is failed;
     *         Return the profile ID if the operation is successful.
     *
     * Client Example:
     * Save: POST /recommendation/adprefprofile with the advertisement preference profile Form/JSON/XML
     * Update: PUT /recommendation/adprefprofile with the advertisement preference profile Form/JSON/XML
     */
    @RequestMapping(method = RequestMethod.POST, consumes = {"application/json", "application/xml"})
    @ResponseBody
    public AdPrefProfile saveProfile(@ModelAttribute("adPrefProfile") AdPrefProfile entity){
        return adPrefProfileService.saveProfile(entity);
    }

    /**
     * This method is used to delete Advertisement Preference Profile by ID and return the result
     * if the it has been deleted.
     *
     * @param id: This is the advertisement preference profile ID.
     * @return Return false if the operation is failed;
     *         Return true if the operation is successful.
     *
     * Client Example:
     * Delete: DELETE /recommendation/adprefprofile/{id}
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public boolean deleteProfile(@PathVariable String id){
        return adPrefProfileService.deleteProfile(id);
    }

    /**
     * This method is used to get Advertisement Preference Profile by ID.
     *
     * @param id: This is the advertisement preference profile ID.
     * @return Return NULL if the operation is failed;
     *         Return the advertisement preference profile if the operation is successful.
     *
     * Client Example:
     * Get: GET /recommendation/adprefprofile/{id}
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public AdPrefProfile findProfileById(@PathVariable String id){
        return adPrefProfileService.findProfileById(id);
    }

    /**
     * This method is used to get list of existing Advertisement Preference Profiles.
     *
     * @return Return NULL if the operation is failed;
     *         Return the list of existing advertisement preference profiles if the operation is successful.
     *
     * Client Example:
     * Get All: GET /recommendation/adprefprofile
     */
    @RequestMapping(method = RequestMethod.GET)
    public List<AdPrefProfile> findAllProfile(){
        return adPrefProfileService.findAllProfile();
    }
}
