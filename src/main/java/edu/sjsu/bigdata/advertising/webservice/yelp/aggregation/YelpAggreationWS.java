package edu.sjsu.bigdata.advertising.webservice.yelp.aggregation;

import edu.sjsu.bigdata.advertising.exception.dao.AdvertisngAccessException;
import edu.sjsu.bigdata.advertising.service.datacollector.yelp.IYelpDataService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

import edu.sjsu.bigdata.advertising.service.datacollector.yelp.aggregation.ReviewByDailyTimeAggregation;
import edu.sjsu.bigdata.advertising.service.datacollector.yelp.aggregation.ReviewByMonthlyTimeAggregation;
import edu.sjsu.bigdata.advertising.service.datacollector.yelp.aggregation.ReviewByBusinessAggregation;
import edu.sjsu.bigdata.advertising.service.datacollector.yelp.aggregation.ReviewByCategoryAggregation;
import edu.sjsu.bigdata.advertising.service.datacollector.yelp.aggregation.ReviewByLocationAggregation;


@Controller
@RequestMapping("/ws/yelp/aggregation")
public class YelpAggreationWS {

	/*	
	@Autowired
	private IYelpDataService yelpAggDataService;
	

    
    @RequestMapping(value = "/category", method = RequestMethod.GET,
    				produces = {"application/json", "application/xml" } )
    @ResponseBody
    public List<ReviewByCategoryAggregation> getAllYelpCategoryAggregation() {
    	List<ReviewByCategoryAggregation>  list = null;
    	
        try {
        	list = yelpAggDataService.getAllYelpCategoryAggregationData();
		} 
        catch (AdvertisngAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        return list;
    }

    
    
    @RequestMapping(value = "/location", method = RequestMethod.GET,
    				produces = {"application/json", "application/xml" } )
    @ResponseBody
    public List<ReviewByLocationAggregation> getAllYelpLocationAggregation() {
    	List<ReviewByLocationAggregation>  list = null;
    	
        try {
        	list = yelpAggDataService.getAllYelpLocationAggregationData();
		} 
        catch (AdvertisngAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        return list;
    }
    

    
    @RequestMapping(value = "/business", method = RequestMethod.GET,
    				produces = {"application/json", "application/xml" } )
    @ResponseBody
    public List<ReviewByBusinessAggregation> getAllYelpBusinessAggregation() {
    	List<ReviewByBusinessAggregation>  list = null;
    	
        try {
        	list = yelpAggDataService.getAllYelpBusinessAggregationData();
		} 
        catch (AdvertisngAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        return list;
    }

    
    
    @RequestMapping(value = "/dailyTime", method = RequestMethod.GET,
    				produces = {"application/json", "application/xml" } )
    @ResponseBody
    public List<ReviewByDailyTimeAggregation> getAllYelpDailyTimeAggregation() {
    	List<ReviewByDailyTimeAggregation>  list = null;
    	
        try {
        	list = yelpAggDataService.getAllYelpDailyTimeAggregationData();
		} 
        catch (AdvertisngAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        return list;
    }

    
    
    @RequestMapping(value = "/monthlyTime", method = RequestMethod.GET,
    				produces = {"application/json", "application/xml" } )
    @ResponseBody
    public List<ReviewByMonthlyTimeAggregation> getAllYelpMonlyTimeAggregation() {
    	List<ReviewByMonthlyTimeAggregation>  list = null;
    	
        try {
        	list = yelpAggDataService.getAllYelpMonlyTimeAggregationData();
		} 
        catch (AdvertisngAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        return list;
    }
    
    
    
    @RequestMapping(value = "/category/{category}", method = RequestMethod.GET,
			produces = {"application/json", "application/xml" } )
    @ResponseBody
	public ReviewByCategoryAggregation findCategoryAggregationByName(@PathVariable String category) {
		ReviewByCategoryAggregation agg = null;
		try {
			agg = yelpAggDataService.findCategoryAggregationByName(category);
		} catch (AdvertisngAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return agg;
	}
	
	
    @RequestMapping(value = "/location/{category}/{city}", method = RequestMethod.GET,
			produces = {"application/json", "application/xml" } )
    @ResponseBody
	public ReviewByLocationAggregation findLocationAggregationByName(@PathVariable String category, @PathVariable String city) {
		ReviewByLocationAggregation agg = null;
		try {
			agg = yelpAggDataService.findLocationAggregationByName(category, city);
		} catch (AdvertisngAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return agg;
	}
	
	
    @RequestMapping(value = "/business/{bname}", method = RequestMethod.GET,
			produces = {"application/json", "application/xml" } )
    @ResponseBody
	public ReviewByBusinessAggregation findBusinessAggregationByName(@PathVariable String bname) {
		ReviewByBusinessAggregation agg = null;
		try {
			agg = yelpAggDataService.findBusinessAggregationByName(bname);
		} catch (AdvertisngAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return agg;
	}
	
	
    @RequestMapping(value = "/dailyTime/{category}/{date}", method = RequestMethod.GET,
			produces = {"application/json", "application/xml" } )
    @ResponseBody
	public ReviewByDailyTimeAggregation findDailyTimeAggregationByValue(@PathVariable String category, @PathVariable String  date) {
		ReviewByDailyTimeAggregation agg = null;
		try {
			agg = yelpAggDataService.findDailyTimeAggregationByValue(category, date);
		} catch (AdvertisngAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return agg;
	}
	
	
    @RequestMapping(value = "/monthlyTime/{category}/{month}", method = RequestMethod.GET,
			produces = {"application/json", "application/xml" } )
    @ResponseBody
	public ReviewByMonthlyTimeAggregation findMonthlyTimeAggregationByValue(@PathVariable String category, @PathVariable String month) {
		ReviewByMonthlyTimeAggregation agg = null;
		try {
			agg = yelpAggDataService.findMonthlyTimeAggregationByValue(category, month);
		} catch (AdvertisngAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return agg;
	}
	*/
	
}
