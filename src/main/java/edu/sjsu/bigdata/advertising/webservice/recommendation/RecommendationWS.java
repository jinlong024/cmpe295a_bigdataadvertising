package edu.sjsu.bigdata.advertising.webservice.recommendation;

import edu.sjsu.bigdata.advertising.model.recommendation.AdCondition;
import edu.sjsu.bigdata.advertising.model.recommendation.RecommendationResult;
import edu.sjsu.bigdata.advertising.service.impl.recommendation.RecommendationServiceImpl;
import edu.sjsu.bigdata.advertising.service.interfaces.recommendation.RecommendationService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/ws/recommendation")
public class RecommendationWS {

     private RecommendationService recommendationService = new RecommendationServiceImpl();


    /**
     * This method is used to get recommendation based on the Advertisement Preference Profile ID.
     *
     * @param id: This is the advertisement preference profile ID.
     * @return Return NULL if the operation is failed;
     *         Return the recommendation result if the operation is successful.
     *
     * Client Example:
     * Get: GET /recommendation/{id}
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public RecommendationResult getRecommendation(String id){
        return recommendationService.getRecommendation(id);
    }

    /**
     * This method is used to get recommendation based on the Advertisement Preference Profile ID.
     *
     * @param condition: This is the advertisement conditions which include a list of Advertisement Preference Profile ID
     *                      and the location to display an advertisement.
     * @return Return NULL if the operation is failed;
     *         Return the recommendation result if the operation is successful.
     *
     * Client Example:
     * Get: GET /recommendation/  with the advertising conditions Form/JSON/XML
     */
    @RequestMapping(method = RequestMethod.GET)
    public RecommendationResult getRecommendation(@ModelAttribute("adCondition") AdCondition condition){
        return recommendationService.getRecommendation(condition);
    }

}
