package edu.sjsu.bigdata.advertising.machinelearning.statistic;


import edu.sjsu.bigdata.advertising.model.datacollector.twitter.SentimentType;


/**
 * This is the word with weight point class.
 * User: Lei Zhang
 * Date: October 29, 2013
 */
public class WordWithWeight {

	public String text;

	public double sentimentValue = 0d;

	public SentimentType type = SentimentType.Neutral;

	public WordWithWeight(String text, double sentimentValue) {
		this.text = text;
		this.sentimentValue = sentimentValue;

		if (sentimentValue == 0) {
			type = SentimentType.Neutral;
		} else if (sentimentValue > 0) {
			type = SentimentType.Positive;
		} else {
			type = SentimentType.Negative;
		}
	}

}
