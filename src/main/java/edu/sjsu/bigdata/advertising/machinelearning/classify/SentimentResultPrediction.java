package edu.sjsu.bigdata.advertising.machinelearning.classify;

import weka.core.*;
import weka.classifiers.meta.FilteredClassifier;

import java.util.List;
import java.util.ArrayList;
import java.io.*;

/**
 * This is the class to predict sentence sentiment result based on trained
 * model.
 * 
 * @author: Jing Ma@SJSU Date: 11-8-2013
 */
public class SentimentResultPrediction {

	Instances rawInstances;
	FilteredClassifier fClassifier;

	public static void main(String[] args) {
		SentimentResultPrediction self = new SentimentResultPrediction();
		SentimentResultPrediction classifier;
		if (args.length < 2) {
			System.out
					.println("Usage: java MyClassifier <fileData> <fileModel>"
							+ "//para:" + args.length);
		} else {
			classifier = new SentimentResultPrediction();
			String predictionPath = args[3];
			classifier.loadModel(args[2]);
			classifier.loadInstances(args[1]);
			classifier.performClassification(predictionPath);

		}
	}

	public int predict(String featureFile, String rawDataFile,
			String modelFile, String preditionOutPutPath) {

		loadModel(modelFile);
		loadInstances(rawDataFile);
		int result = performClassification(preditionOutPutPath);
		
		return result;

	}

	/**
	 * Jing@: Load well-trained models here
	 * 
	 * @param modelPath
	 */
	public void loadModel(String modelPath) {
		try {
			ObjectInputStream in = new ObjectInputStream(new FileInputStream(
					modelPath));

			fClassifier = (FilteredClassifier) in.readObject();

			in.close();
			System.out.println("===== Loaded model: " + modelPath + " =====");
		} catch (Exception e) {
			// Given the cast, a ClassNotFoundException must be caught along
			// with the IOException
			System.out.println("Problem found when reading: " + modelPath);
			e.printStackTrace();
		}
	}

	public void loadInstances(String rawFilePath) {
		// Create the attributes, class and text
		FastVector fvNominalVal = new FastVector(2);

		// Five level
		fvNominalVal.addElement("-1");
		fvNominalVal.addElement("-2");
		fvNominalVal.addElement("0");
		fvNominalVal.addElement("1");
		fvNominalVal.addElement("2");

		// raw data file as input
		Attribute attribute1 = new Attribute("class", fvNominalVal);
		Attribute attribute2 = new Attribute("text", (FastVector) null);

		// Create list of instances with one element
		FastVector fvWekaAttributes = new FastVector(2);
		fvWekaAttributes.addElement(attribute1);
		fvWekaAttributes.addElement(attribute2);
		rawInstances = new Instances("Test relation", fvWekaAttributes, 1);

		// multiple instances, set index
		rawInstances.setClassIndex(0);

		// multiple instances, read instances
		try {

			BufferedReader br = new BufferedReader(new FileReader(rawFilePath));
			String line;
			while ((line = br.readLine()) != null) {

				// Create and add the instance
				Instance instance = new Instance(2);
				instance.setValue(attribute2, line);
				rawInstances.add(instance);

			}
			br.close();
		} catch (Exception e) {
			System.out.println("Prediction: Read data file Error!");
			e.printStackTrace();

		}

		System.out
				.println("===== Instance created with reference dataset =====");
		System.out.println(rawInstances);
	}

	/**
	 * This method performs the classification of the instance. Output is done
	 * at the command-line.
	 */
	public int performClassification(String predictionPath) {
		
		String result = "0";
		try {
			System.out.println("===== Classified instance =====");
			File file = new File(predictionPath);
			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write("JM@Predictions:");

			for (int instanceCurrent = 0; instanceCurrent < rawInstances
					.numInstances(); instanceCurrent++) {

				if (instanceCurrent <= 4) {

					String predictionOutPut;
					predictionOutPut = "JM@Information: "
							+ rawInstances.instance(instanceCurrent).toString();
					System.out.println(predictionOutPut);

					bw.newLine();
					bw.write(predictionOutPut);
					bw.flush();

				} else {
					double pred = fClassifier.classifyInstance(rawInstances
							.instance(instanceCurrent));
					String predictionOutPut;
					predictionOutPut = "Current instance:"
							+ rawInstances.instance(instanceCurrent)
							+ "; Class predicted: "
							+ rawInstances.classAttribute().value((int) pred);
					
					result = rawInstances.classAttribute().value((int) pred);
					System.out.println(predictionOutPut);

					bw.newLine();
					bw.write(predictionOutPut);
					bw.flush();
				}

			}

		} catch (Exception e) {
			System.out.println("Problem found when classifying the text");
			e.printStackTrace();
		}
		
		return Integer.parseInt(result);
	}

}