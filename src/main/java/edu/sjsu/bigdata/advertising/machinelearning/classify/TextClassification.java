package edu.sjsu.bigdata.advertising.machinelearning.classify;


import weka.classifiers.Classifier;
import weka.classifiers.bayes.NaiveBayes;
import weka.classifiers.functions.SMO;
import weka.classifiers.trees.j48.*;
import weka.classifiers.Evaluation;
import weka.core.Instances;
import weka.core.OptionHandler;
import weka.core.SerializationHelper;
import weka.core.Utils;
import weka.core.converters.ArffLoader.ArffReader;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.StringToWordVector;
import weka.classifiers.meta.FilteredClassifier;
import weka.classifiers.meta.Vote;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Random;
import java.util.Vector;
import java.util.ArrayList;




/**
 * This is the class to train classifier model and conduct cross evaluation. 
 * 
 * filtered classifier training process
 * including cross evaluation
 * @author: Jing Ma@SJSU 
 * Date: 11-8-2013
 */
public class TextClassification{
	public static void main(String[] args,String rawFilePath,String modelPath, StringToWordVector filter){
		
		TextClassification selfOwn = new TextClassification();
		
		//Jing@: get classifier string and dataset path
		CommandInput parameter = new CommandInput("","");
		if(args.length<4) {
			System.out.println(TextClassification.usage());
		      System.exit(1);
			
		} else{
			parameter = selfOwn.inputParse(args);			
		}
		
		if (parameter.getClassifier().equals("") || parameter.getDataset().equals("")){
			 System.out.println("Not all parameters provided!");
		      System.out.println(TextClassification.usage());
		      System.exit(2);			
		}
		
				
		//Jing@: set and run classifier
		FilteredClassifier fc = new FilteredClassifier();
		//Classifier classifier; //TODO: why I can't new a classifier here?
		
		
		
		try{
			//set classifier, dataset and filter
			System.out.println("=======================Training process begins, classifier:"+parameter.getClassifier()+"======================");
			Classifier classifier = Classifier.forName(parameter.getClassifier(), null);
			fc.setClassifier(classifier);
			
			fc.setFilter(filter);
			
			Instances dataset = new ArffReader(new BufferedReader(new FileReader(parameter.getDataset()))).getData();
			System.out.println("===== Loaded dataset =====");
			
			
			dataset.setClassIndex(1);
			
			
			
			//Jing@: cross evaluation first
			System.out.println("=======================Cross evaluation process begins, classifier:"+parameter.getClassifier()+"======================");			
			selfOwn.crossEvaluation(3, dataset,filter, classifier);
			System.out.println("=======================Cross evaluation process ends, classifier:"+parameter.getClassifier()+"======================");
			
			
			
			//run learner
			fc.buildClassifier(dataset);
			
			//TODO: print models here
			
			System.out.println(fc.toString());
			System.out.println("=======================Training process ends, classifier:"+parameter.getClassifier()+"======================");
			
			
			//Jing@:output models;
			selfOwn.modelSave(fc,modelPath,parameter.getClassifier());
				System.out.println("=======================Trained filtered classifier: "+parameter.getClassifier()+" is saved.======================");
			
			
			
			
		} catch (Exception e){
			
			System.out.println("Setting classifier error!");
			e.printStackTrace();
			
		}
		
		
		
		
	}
	
	public static String usage() {
	    	return
	        "\nusage:\n  " + TextClassification.class.getName() 
		        + "  CLASSIFIER <classname> [options] \n"
		        + "  FILTER <classname> [options]\n"
		        + "  DATASET <trainingfile>\n\n"
		        + "e.g., \n"
		        + "    CLASSIFIER weka.classifiers.trees.J48 -U \n"
		        + "    FILTER weka.filters.unsupervised.instance.Randomize \n"
		        + "    DATASET iris.arff\n";
		  }  
	
	

	/**
	 * @author Jing
	 * Cross evaluation should input 
	 */
	public void crossEvaluation(int folderNum, Instances dataset,StringToWordVector filter, Classifier classifier){
		
		//Instances dataset = new Instances(new BufferedReader(new FileReader(parameter.getDataset())));
		dataset.setClassIndex(1);
		FilteredClassifier fClassifer = new FilteredClassifier();
		StringToWordVector ftest = new StringToWordVector();
		ftest.setAttributeIndices("first");
		fClassifer.setFilter(ftest);
		fClassifer.setClassifier(classifier);		
				
		
		System.out.println("classindex number:" +dataset.classIndex());
		
		try{
			Evaluation eval = new Evaluation(dataset);
			eval.crossValidateModel(fClassifer, dataset, folderNum, new Random(1));
			
			System.out.println(eval.toSummaryString());
			System.out.println(eval.toMatrixString());
			System.out.println(eval.toClassDetailsString());
			
			
		} catch (Exception e){
			e.printStackTrace();
		}	
		
	}	

	
	public boolean modelSave(FilteredClassifier model, String modelPath,String classifierName) throws Exception{
		boolean rs = false;
		System.out.println("test"+modelPath+model.getClassifier().toString());
		weka.core.SerializationHelper.write(modelPath+classifierName+".model", model);
		
		return rs;
		
	}
	
	public CommandInput inputParse(String[] commandInput){
		//the program is not accept ops input;
		int i=0;
		String classifierString = "";
		String datasetString = "";		
		
		do{			
			if(commandInput[i].equals("CLASSIFIER")){
				classifierString = commandInput[i+1];				
				i=i+2;
			} else if(commandInput[i].equals("DATASET")){
				datasetString = commandInput[i+1];	
				i=i+2;
			}			
		} while (i<commandInput.length);
		
		return new CommandInput(classifierString,datasetString);	
		
	}
	
	
}
class CommandInput {
	private String classifierString;
	private String datasetString;	
	
	public CommandInput(String classifier,String dataset){
		classifierString = classifier;
		datasetString = dataset;		
	}
	
    public void setClassifier(String classifier){
    	classifierString = classifier;    	
    }
    
    public void setDataset(String dataset){
    	datasetString = dataset;    	
    }
    
    public String getClassifier(){    	
    	return classifierString;
    }
    
    public String getDataset(){    	
    	return datasetString;
    }
}
