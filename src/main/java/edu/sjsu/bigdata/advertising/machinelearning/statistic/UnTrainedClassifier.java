package edu.sjsu.bigdata.advertising.machinelearning.statistic;

import java.io.*;
import java.util.*;
import java.util.regex.*;

import com.google.gson.*;

import edu.sjsu.bigdata.advertising.model.datacollector.twitter.SentimentType;
import edu.sjsu.bigdata.advertising.util.recommendation.IOUtil;

/**
 * This is the untrained classifier class. 
 * Untrained sentiment analysis approach utilizes different set of key words to decide whether a sentiment is positive, negative or neutral. 
 * User: Lei Zhang
 * Date: October 29, 2013
 */
public class UnTrainedClassifier {

	Gson gson = new Gson();
	Set<String> stopWordSet = new TreeSet<String>(String.CASE_INSENSITIVE_ORDER);
	Set<String> negationWordSet = new TreeSet<String>(
			String.CASE_INSENSITIVE_ORDER);


	HashMap<String, Double> sentimentWordMap = new HashMap<String, Double>();
	HashSet<String> negationSet = new HashSet<String>();

	Pattern wordPattern = Pattern
			.compile("[#@]?\\w+(\'\\w+)?|:\\w(?:\\s)|([^\\w\\s@#])+");

	public UnTrainedClassifier(String sentimentWordsFileName,
			String negationsFileName, String stopWordFilename)
			throws IOException {

		FileInputStream sentimentWordsStream = new FileInputStream(
				sentimentWordsFileName);

		sentimentWordMap.clear();
		sentimentWordMap = gson.fromJson(new InputStreamReader(
				sentimentWordsStream), (new HashMap<String, Integer>())
				.getClass());

		//System.out.println("The sentiment word set is:");
		//IOUtil.printMap(sentimentWordMap);

		stopWordSet.clear();
		negationWordSet.clear();

		ArrayList<String> negationWordList = IOUtil
				.readWordList(negationsFileName);

		for (String negationWord : negationWordList) {
			negationWordSet.add(negationWord);
		}

		//System.out.println("The negation word set is:");
		//IOUtil.printCollection(negationWordSet);

		ArrayList<String> stopWordList = IOUtil.readWordList(stopWordFilename);

		for (String stopWord : stopWordList) {
			stopWordSet.add(stopWord);
		}

		//System.out.println("The stop word set is:");
		//IOUtil.printCollection(stopWordSet);

	}

	public String classify(String text) throws IOException {
		ArrayList<WordWithWeight> positiveWordList = new ArrayList<WordWithWeight>();
		ArrayList<WordWithWeight> negativeWordList = new ArrayList<WordWithWeight>();
		Double totalSentimentValue = 0d;

		Matcher matcher = wordPattern.matcher(text);
		List<String> wordsFromInput = new ArrayList<String>(
				matcher.groupCount());

		int i = 0;

		while (matcher.find()) {
			String matchedWord = matcher.group();

			System.out.println("word " + i++ + " is " + matchedWord);

			if (stopWordSet.contains(matchedWord)) {
				continue;
			} else {
				wordsFromInput.add(matchedWord);
			}
		}

		System.out.println("After stop word processing: " + wordsFromInput);

		int gameChanger = 1;
		for (String wordInput : wordsFromInput) {
			if (negationWordSet.contains(wordInput)) {
				gameChanger = -1;
			} else {
				Double weight = 0d;

				System.out.println("word: " + wordInput);
				if (sentimentWordMap.containsKey(wordInput.toLowerCase())) {
					System.out.println("Sentiment word list include word: " + wordInput);
					weight = sentimentWordMap.get(wordInput.toLowerCase());
					System.out.println("Word: " + wordInput + " weight is " + weight);
				}

				totalSentimentValue += (gameChanger * weight);
				gameChanger = 1;

			}
		}

		System.out.println("After processing, postive word list: "
				+ positiveWordList.size());
		System.out.println("After processing, negative word list: "
				+ negativeWordList);
		System.out.println("After processing, sentiment value: "
				+ totalSentimentValue);

		String type;
		if (totalSentimentValue == 0) {
			type = SentimentType.Neutral.toString();
		} else if (totalSentimentValue > 0) {
			type = SentimentType.Positive.toString();
		} else {
			type = SentimentType.Negative.toString();
		}

		return type;
	}

	public static void main(String[] args) {

		UnTrainedClassifier classifier;
		try {
			classifier = new UnTrainedClassifier("afinn_111.json",
					"negations.json", "stop_words.txt");

			String result = classifier.classify("I will not be scared again.");
			System.out.println(result);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
