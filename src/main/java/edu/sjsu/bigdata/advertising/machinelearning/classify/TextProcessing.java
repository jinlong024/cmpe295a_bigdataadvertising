package edu.sjsu.bigdata.advertising.machinelearning.classify;
 
 import weka.core.Instances;
 import weka.core.converters.ArffLoader.ArffReader;
 import weka.core.tokenizers.NGramTokenizer;
 import weka.filters.Filter;
 import weka.filters.unsupervised.attribute.StringToWordVector;
 import java.io.*;
 
 
 /**
  * This is the class to handle WEKA text indexing and customize NLP filters:
 *   implement: 
 *     keywords pool size;
 *     stop words control;
 *     TF-iDF;(normalized);
 *     Lower-case control;
 *     N-grain control;(=1)   
 *     
 *     ALL INPUT/OUTPUT files are all ARFF format, 
 *     please refer sample ARFF file format(WEKA's requirement)
  * @author: Jing Ma@SJSU
  * Date: 11-8-2013
  */
 public class TextProcessing {
    
   Instances input, output;  
   
   public static StringToWordVector indexer(String rawTextPath,String featureFilePath) {
	   
	     //static method only can use static attribute, so need a object as itself for using non-static ones
	     TextProcessing indexer = new TextProcessing();	     	     	    
		 
	     //load raw text file
	     indexer.loadARFF(rawTextPath);
	     
	     long beginTime, endTime;
	     beginTime = System.currentTimeMillis();
	     System.out.println("Pre-process started at: " + beginTime);
	     
	     //set-up filter and transfer input to output(feature file) by filtering
	     StringToWordVector filterRS = indexer.index();
	     
	     endTime = System.currentTimeMillis();
	     System.out.println("Pre-process finished at: " + endTime);
	     System.out.println("Pre-process total time: " + (endTime-beginTime));
	     
	     //save feature file to featureFilePath
	     indexer.saveARFF(featureFilePath);
	     return filterRS;
	     
	   }
     
   /**
    * Loads an ARFF file into an instances object.
    * 
    */
   public void loadARFF(String fileName) {
   
     try {
       BufferedReader reader = new BufferedReader(new FileReader(fileName));
       ArffReader arff = new ArffReader(reader);
       input = arff.getData();
       System.out.println("===== Loaded dataset: "  +fileName+  " =====");
       reader.close();
     } catch (IOException e) {
       System.out.println("exception:" + e);
       System.out.println("Problem found when reading: " + fileName);
     }
     
   }
   
   /**
    * Index the inputInstances string features using the StringToWordVector filter.
    */
   public StringToWordVector index() {
     // outputInstances = inputInstances;
     try {
       
       // Set the tokenizer
       NGramTokenizer tokenizer = new NGramTokenizer();
       tokenizer.setNGramMinSize(1);
       tokenizer.setNGramMaxSize(1);
       tokenizer.setDelimiters("\\W");       
       
       //Set the filter        
       StringToWordVector filter = new StringToWordVector();          
       filter.setTokenizer(tokenizer);
       filter.setTFTransform(true);
       filter.setIDFTransform(true); 
       ///no bias
       String[] normalization ={"-N 1"};
       filter.setOptions(normalization);       
       ///Stop words
       String stopWordPath;	     
       stopWordPath = "E:/SJSU/BigData_LeiNFlora/Clean_data/" +"stop_words.txt";
       File value = new File(stopWordPath);
       filter.setStopwords(value);       
       ///other parameters
       filter.setInputFormat(input);  ////input instances as raw file records
       filter.setWordsToKeep(80);
       filter.setDoNotOperateOnPerClassBasis(true);
       filter.setLowerCaseTokens(true);
       
       // Filter: the input instances into the output ones(feature file)
       output = Filter.useFilter(input,filter);
       
       System.out.println("===== Filtering dataset done =====");
       return filter;
       
     }
     catch (Exception e) {
       System.out.println("Problem found when training");
       e.printStackTrace();
       return null;
     }
   }
   
   /**
    * Save an output instances object into an ARFF file.   
    */  
   public void saveARFF(String fileName) {
   
     try {
       PrintWriter writer = new PrintWriter(new FileWriter(fileName));
       writer.print(output);
       System.out.println("===== Saved dataset: " + fileName + " =====");
       writer.close();
     }
     catch (IOException e) {
       System.out.println("Problem found when writing: " +fileName);
     }
   } 
   
 }