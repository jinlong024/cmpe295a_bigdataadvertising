package edu.sjsu.bigdata.advertising.machinelearning.classify;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

import weka.filters.unsupervised.attribute.StringToWordVector;

/**
 * This is the main class to conduct Natural Language Processing(NLP), feature
 * creation, train models, cross evaluation and generate prediction result.
 * author: Jing Ma@SJSU include: NLP pre-process Features file creation
 * 
 * Classification: Naive Bayes/SVM/J48 Well-trained Models Classification result
 * output
 * 
 * Firstly, it is required to use training data with pre-defined classifier to
 * train related classification model.
 * 
 * Then, use trained model to predict classification result for raw with without
 * classifier.
 * 
 */
public class ClassificationService {

	/**
	 * This is used to train a classification model. The following is an example
	 * parameters: String trainDataFile = "data/yelpTrainingData.arff"; String
	 * featureFile = "data/yelpFeatureData.arff"; String modelsPath = "model/";
	 */
	public boolean trainClassificationModel(String trainDataFile,
			String featureFile, String modelsPath) {

		StringToWordVector filter = TextProcessing.indexer(trainDataFile,
				featureFile);

		// Train ML models
		// TODO: if you are training a new model, please use following code:
		// machineLearningModelTrainer(modelsPath, filter, trainDataFile);

		String commandStringNB = "CLASSIFIER weka.classifiers.bayes.NaiveBayes ";
		String commandStringJ48 = "CLASSIFIER weka.classifiers.trees.J48 ";
		String commandStringSVM = "CLASSIFIER weka.classifiers.functions.SMO ";

		String rawDatasetTraining = "DATASET " + trainDataFile;

		String[] commandArgsNB = (commandStringNB + rawDatasetTraining)
				.split(" ");
		String[] commandArgsJ48 = (commandStringJ48 + rawDatasetTraining)
				.split(" ");
		String[] commandArgsSVM = (commandStringSVM + rawDatasetTraining)
				.split(" ");
		boolean result = false;
		// classification model training

		try {
			// for training and validation
			double currentTime = System.currentTimeMillis();

			TextClassification.main(commandArgsNB, rawDatasetTraining,
					modelsPath, filter);
			TextClassification.main(commandArgsJ48, rawDatasetTraining,
					modelsPath, filter);
			TextClassification.main(commandArgsSVM, rawDatasetTraining,
					modelsPath, filter);

			result = true;

			double timeGap = System.currentTimeMillis() - currentTime;

			System.out.println("Models training time(s): " + timeGap / 1000);

		} catch (Exception e) {

			System.out.println("Catch excpetion when training a model:" + e);
		}
		return result;
	}

	/**
	 * This is used to train a classification model. The following is an example
	 * parameters: String rawDataFile = "data/yelpRawData.arff"; String
	 * featureFile = "data/yelpFeatureData.arff"; String modelsPath = "model/";
	 */
	public int predictSentimentResult(String rawDataFile, String featureFile,
			String modelsPath) {

		// Jing@Predictions:
		String modelJ48 = modelsPath + "weka.classifiers.trees.J48.model";
		String modelSVM = modelsPath + "weka.classifiers.functions.SMO.model";
		String modelNB = modelsPath + "weka.classifiers.bayes.NaiveBayes.model";
		String preditionOutPutPath = modelsPath + "PredictionResult.txt";
		
		// NB, J48 or SVM model can be chosen here. Use NB at this point
		String[] fileNModel = { featureFile, rawDataFile, modelNB,
				preditionOutPutPath };
		System.out.println("file: " + fileNModel);
		//SentimentResultPrediction.main(fileNModel);
		
		SentimentResultPrediction classifier = new SentimentResultPrediction();
		return classifier.predict(featureFile, rawDataFile, modelNB, preditionOutPutPath);

	}

	public void generateRawDataArffFile(String rawText) {

		PrintWriter writer;
		try {
			writer = new PrintWriter("data/rawText.arff", "UTF-8");
			writer.println("@relation user_review_level");
			writer.println("");
			writer.println("@attribute  Review string");
			writer.println("");
			writer.println("@data");
			writer.println(rawText);
			writer.close();
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			System.out
					.println("Catch exception when generating raw data arff file:"
							+ e);
		}

	}

	public static boolean machineLearningModelTrainer(String modelsPath,
			StringToWordVector filterOringin, String rawFile) {
		String commandStringNB = "CLASSIFIER weka.classifiers.bayes.NaiveBayes ";
		String commandStringJ48 = "CLASSIFIER weka.classifiers.trees.J48 ";
		String commandStringSVM = "CLASSIFIER weka.classifiers.functions.SMO ";

		String rawDatasetTraining = "DATASET " + rawFile;

		String[] commandArgsNB = (commandStringNB + rawDatasetTraining)
				.split(" ");
		String[] commandArgsJ48 = (commandStringJ48 + rawDatasetTraining)
				.split(" ");
		String[] commandArgsSVM = (commandStringSVM + rawDatasetTraining)
				.split(" ");
		boolean rs = false;
		// classification model training

		try {
			// for training and validation
			double currentTime = System.currentTimeMillis();
			TextClassification.main(commandArgsNB, rawDatasetTraining,
					modelsPath, filterOringin);
			TextClassification.main(commandArgsJ48, rawDatasetTraining,
					modelsPath, filterOringin);
			TextClassification.main(commandArgsSVM, rawDatasetTraining,
					modelsPath, filterOringin);
			rs = true;
			double timeGap = System.currentTimeMillis() - currentTime;
			System.out.println("Models training time(s): " + timeGap / 1000);

		} catch (Exception e) {

			e.printStackTrace();
		}
		return rs;
	}
	
	public int predict(String rawText) {

		String rawDataFile = "data/rawText.arff";
		String featureFile = "data/featureData.arff";
		String modelsPath = "model/";

		ClassificationService service = new ClassificationService();
		service.generateRawDataArffFile(rawText);

		int result = service.predictSentimentResult(rawDataFile, featureFile, modelsPath);
		
		System.out.println("Classifiction Result is:" + result);
		
		return result;



	}

	public static void main(String[] args) {

		// String rawDataFile = "data/yelpRawData.arff";
		String rawDataFile = "data/rawText.arff";
		String trainingDataFile = "data/yelpTrainingData.arff";
		String featureFile = "data/featureData.arff";
		String modelsPath = "model/";

		ClassificationService service = new ClassificationService();
		String rawText = "Every time my dad and I go to berkeley we ALWAYS go to this restaurant.  And I m sure that Berkeley has many delicious restaurants  but this place is awesomeness on a stick.  Or  in a bowl  I guess.    The Veggie Delight salad is amazing.  It doesn t even matter what dressing you get--they re all good.  And you can probably just get the salad and share it with a friend.  Seriously.  One veggie delight salad could feed Peru for several months! (although  guinea pig is not on the menu)  And the sammiches are delicious!  the fresh bread is  as every other reviewer has said  thick and tasty.  A bit sweet  but in a good way.    The egg salad is super good  the tofu sammich is alright.  A little sweet.  the turkey is good  standard turkey sammich.  It s just that salad.  Its SERIOUS salad.  Here s the missing star (I almost rated it 5  then thought about it a bit more).... They are often out of egg salad and/or sprouts.  And  I love both egg salad and sprouts.  What can I say  I m a california girl.  Tables can be scarce--best to figure out what you re ordering before you start standing in line  and come with a friend who can grab the table and hold it for you.  And that s about it.  I don t care  about the tables that much  but it s really really disappointing when all you want is a 1/2 and 1/2 veggie delight and egg salad sammich only to find that neither has sprouts and the sammich can t be egg salad.  Especially when you re coming from Santa Cruz.  Or Sacramento.  Or San Diego.  Then it s a bitter cup of sproutless eggsaladlessness.  P.S. Its super cheap  everything about $7 (with $2 beers  incidentally)  but bring cash!! Cash only.";

		int result = service.predict(rawText);
		
		System.out.println("Classifiction Result is:" + result);

		// service.trainClassificationModel(trainingDataFile, featureFile,
		// modelsPath);

		// StringToWordVector filter = TextProcessing.indexer(rawTextPath,
		// featureFilePath);

		// Train ML models
		// machineLearningModelTrainer(modelsPath, filter, rawTextPath);

	}

}
