package edu.sjsu.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import edu.sjsu.bigdata.advertising.model.recommendation.AdPrefProfile;
import edu.sjsu.bigdata.advertising.service.interfaces.recommendation.AdPrefProfileService;


/**
 * This is the advertisement preference profile RESFFul web service.
 * User: Lei Zhang
 * Date: August 13, 2013
 * 1. to enable TEST for Spring\n" +
 * 2. tell our test class what configs to use for spring
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:*/webapp/WEB-INF/applicationContext.xml")
public class AdPrefProfileServiceTest {

   @Autowired
   private AdPrefProfileService adPrefProfileService;

    @Test
    public void insertAdPrefProfileData() {

        AdPrefProfile profile = new AdPrefProfile();
        profile.setAge("10");
        adPrefProfileService.saveProfile(profile) ;

    }

}
