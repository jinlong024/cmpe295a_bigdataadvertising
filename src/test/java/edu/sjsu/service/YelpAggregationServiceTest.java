/**
 * 
 */
package edu.sjsu.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import edu.sjsu.bigdata.advertising.exception.dao.AdvertisngAccessException;
import edu.sjsu.bigdata.advertising.service.datacollector.yelp.IYelpDataService;
import edu.sjsu.bigdata.advertising.service.datacollector.yelp.aggregation.ReviewByCategoryAggregation;

/**
 * @author yunxue
 *
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:*/webapp/WEB-INF/applicationContext.xml")
public class YelpAggregationServiceTest {

    @Autowired
    private IYelpDataService yelpDataService;

    @Test
    public ReviewByCategoryAggregation findYelpCategoryAggregation() {
    	String category = "Food";
    	ReviewByCategoryAggregation  agg = null;
    	try {
    		agg = yelpDataService.findCategoryAggregationByName(category);
		} 
    	catch (AdvertisngAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
		return agg;
    }

}

