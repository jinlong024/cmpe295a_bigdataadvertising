package edu.sjsu.service;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;


import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.view.RedirectView;


import edu.sjsu.bigdata.advertising.model.recommendation.AdPrefProfile;
import edu.sjsu.bigdata.advertising.model.recommendation.DemographicProfile;
import edu.sjsu.bigdata.advertising.util.recommendation.ProfileScaleMapper;
   
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:/webapp/WEB-INF/dispatcher-servlet.xml")
public class RecommendationAlgorithmTest {  
   
	
	@Test
	public void calculateNoWeightDistance() {  
     
		
		AdPrefProfile adPrefProfile = new AdPrefProfile();
		DemographicProfile demographicProfile = new  DemographicProfile();
		
		adPrefProfile.setAge("Child (2-11)");
		demographicProfile.setAge("Youth (12-17)");
		
		adPrefProfile.setEducation("Less than high school");
		demographicProfile.setEducation("High school graduate");
		
		adPrefProfile.setRace("White");
		demographicProfile.setRace("Asian");
		
		adPrefProfile.setEthnicity("Hispanic");
		demographicProfile.setEthnicity("Non-Hispanic");
		
		adPrefProfile.setSex("Male");
		demographicProfile.setSex("Female");
		
		adPrefProfile.setIncome("Less than $10,000");
		demographicProfile.setIncome("$10,000 to $49,999");
		
		adPrefProfile.setPopulation("Less than 10,000");
		demographicProfile.setPopulation("10,000 to 99,999");
		
		adPrefProfile.setHouse("Less than 10,000");
		demographicProfile.setHouse("10,000 to 99,999");
		
		adPrefProfile.setWeather("Coastal Weather");
		demographicProfile.setWeather("Hot Weather");
		
		Integer distance = 0;

		distance = distance + Math.abs(ProfileScaleMapper.AGE_SCALE_MAPPER.get(adPrefProfile
				.getAge())
				- ProfileScaleMapper.AGE_SCALE_MAPPER.get(demographicProfile
						.getAge()));
		
		distance = distance + Math.abs(ProfileScaleMapper.EDUCATION_SCALE_MAPPER.get(adPrefProfile
				.getEducation())
				- ProfileScaleMapper.EDUCATION_SCALE_MAPPER.get(demographicProfile
						.getEducation()));
		
		distance = distance + Math.abs(ProfileScaleMapper.INCOME_SCALE_MAPPER.get(adPrefProfile
				.getIncome())
				- ProfileScaleMapper.INCOME_SCALE_MAPPER.get(demographicProfile
						.getIncome()));
		
		distance = distance + Math.abs(ProfileScaleMapper.SEX_SCALE_MAPPER.get(adPrefProfile
				.getSex())
				- ProfileScaleMapper.SEX_SCALE_MAPPER.get(demographicProfile
						.getSex()));
		
		distance = distance + Math.abs(ProfileScaleMapper.ETHNICITY_SCALE_MAPPER.get(adPrefProfile
				.getEthnicity())
				- ProfileScaleMapper.ETHNICITY_SCALE_MAPPER.get(demographicProfile
						.getEthnicity()));
		
		distance = distance + Math.abs(ProfileScaleMapper.RACE_SCALE_MAPPER.get(adPrefProfile
				.getRace())
				- ProfileScaleMapper.RACE_SCALE_MAPPER.get(demographicProfile
						.getRace()));
		
		distance = distance + Math.abs(ProfileScaleMapper.POPULATION_SCALE_MAPPER.get(adPrefProfile
				.getPopulation())
				- ProfileScaleMapper.POPULATION_SCALE_MAPPER.get(demographicProfile
						.getPopulation()));
		
		distance = distance + Math.abs(ProfileScaleMapper.HOUSING_UNIT_SCALE_MAPPER.get(adPrefProfile
				.getHouse())
				- ProfileScaleMapper.HOUSING_UNIT_SCALE_MAPPER.get(demographicProfile
						.getHouse()));
		
		distance = distance + Math.abs(ProfileScaleMapper.WEATHER_SCALE_MAPPER.get(adPrefProfile
				.getWeather())
				- ProfileScaleMapper.WEATHER_SCALE_MAPPER.get(demographicProfile
						.getWeather()));
        
		System.out.println("Distance is " +  distance/9 );

    }  
    
	
	@Test
	public void calculateWeightDistance() {  
     
		AdPrefProfile adPrefProfile = new AdPrefProfile();
		DemographicProfile demographicProfile = new  DemographicProfile();
		
		adPrefProfile.setAge("Child (2-11)");
		demographicProfile.setAge("Youth (12-17)");
		
		adPrefProfile.setEducation("Less than high school");
		demographicProfile.setEducation("High school graduate");
		
		adPrefProfile.setRace("White");
		demographicProfile.setRace("Asian");
		
		adPrefProfile.setEthnicity("Hispanic");
		demographicProfile.setEthnicity("Non-Hispanic");
		
		adPrefProfile.setSex("Male");
		demographicProfile.setSex("Female");
		
		adPrefProfile.setIncome("Less than $10,000");
		demographicProfile.setIncome("$10,000 to $49,999");
		
		adPrefProfile.setPopulation("Less than 10,000");
		demographicProfile.setPopulation("10,000 to 99,999");
		
		adPrefProfile.setHouse("Less than 10,000");
		demographicProfile.setHouse("10,000 to 99,999");
		
		adPrefProfile.setWeather("Coastal Weather");
		adPrefProfile.setWeatherWeight(1.0);
		demographicProfile.setWeather("Hot Weather");
		
		Double distance = 0.0;

		distance = distance + Math.abs(adPrefProfile.getAgeWeight() * (ProfileScaleMapper.AGE_SCALE_MAPPER.get(adPrefProfile
				.getAge()) 
				- ProfileScaleMapper.AGE_SCALE_MAPPER.get(demographicProfile
						.getAge())));
		
		distance = distance + Math.abs(adPrefProfile.getEducationWeight() * (ProfileScaleMapper.EDUCATION_SCALE_MAPPER.get(adPrefProfile
				.getEducation())
				- ProfileScaleMapper.EDUCATION_SCALE_MAPPER.get(demographicProfile
						.getEducation())));
		
		distance = distance + Math.abs(adPrefProfile.getIncomeWeight() * (ProfileScaleMapper.INCOME_SCALE_MAPPER.get(adPrefProfile
				.getIncome())
				- ProfileScaleMapper.INCOME_SCALE_MAPPER.get(demographicProfile
						.getIncome())));
		
		distance = distance + Math.abs(adPrefProfile.getSexWeight() * (ProfileScaleMapper.SEX_SCALE_MAPPER.get(adPrefProfile
				.getSex())
				- ProfileScaleMapper.SEX_SCALE_MAPPER.get(demographicProfile
						.getSex())));
		
		distance = distance + Math.abs(adPrefProfile.getEthnicityWeight() * (ProfileScaleMapper.ETHNICITY_SCALE_MAPPER.get(adPrefProfile
				.getEthnicity())
				- ProfileScaleMapper.ETHNICITY_SCALE_MAPPER.get(demographicProfile
						.getEthnicity())));
		
		distance = distance + Math.abs(adPrefProfile.getRaceWeight() * (ProfileScaleMapper.RACE_SCALE_MAPPER.get(adPrefProfile
				.getRace())
				- ProfileScaleMapper.RACE_SCALE_MAPPER.get(demographicProfile
						.getRace())));
		
		distance = distance + Math.abs(adPrefProfile.getPopulationWeight() * (ProfileScaleMapper.POPULATION_SCALE_MAPPER.get(adPrefProfile
				.getPopulation())
				- ProfileScaleMapper.POPULATION_SCALE_MAPPER.get(demographicProfile
						.getPopulation())));
		
		distance = distance + Math.abs(adPrefProfile.getHouseWeight() * (ProfileScaleMapper.HOUSING_UNIT_SCALE_MAPPER.get(adPrefProfile
				.getHouse())
				- ProfileScaleMapper.HOUSING_UNIT_SCALE_MAPPER.get(demographicProfile
						.getHouse())));
		
		distance = distance + Math.abs(adPrefProfile.getWeatherWeight() * (ProfileScaleMapper.WEATHER_SCALE_MAPPER.get(adPrefProfile
				.getWeather())
				- ProfileScaleMapper.WEATHER_SCALE_MAPPER.get(demographicProfile
						.getWeather())));
        
		System.out.println("Distance is " +  distance/9 );

    }  
   
}

