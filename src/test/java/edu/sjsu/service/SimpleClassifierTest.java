package edu.sjsu.service;



import java.io.*;

import org.junit.*;

import com.google.gson.Gson;

import edu.sjsu.bigdata.advertising.machinelearning.statistic.UnTrainedClassifier;
import edu.sjsu.bigdata.advertising.model.datacollector.twitter.SentimentType;



public class SimpleClassifierTest {

	
	UnTrainedClassifier classifier;


	@Before
	public void setUp() throws Exception {
	
			classifier = new UnTrainedClassifier("afinn_111.json",
					"negations.json", "stop_words.txt");

		
	}

	@Test
	public void positve2() throws IOException {
		String result = classifier.classify("Apple iphone cases 4/4s,iphone5 cases the BEst iphone 4/5 cases,iphone 4 cover ,iphone 4 cases gree- http://t.co/ViNgF9Gtvl #Etsy #Iphone");
		Assert.assertEquals(SentimentType.Positive.toString(), result);
	}

	@Test
	public void positive1() throws IOException {
		String result = classifier.classify("I love the whole world.");
		Assert.assertEquals(SentimentType.Positive.toString(), result);
	}

	@Test
	public void negative1() throws IOException {
		String result = classifier.classify("I messed up my midterm. I will be in the hell in the rest of the semester.");
		Assert.assertEquals(SentimentType.Negative.toString(), result);
	}

	@Test
	public void negative2() throws IOException {
		String result = classifier.classify("Go hell, faggot!");
		Assert.assertEquals(SentimentType.Negative.toString(), result);
	}

	@Test
	public void negative3() throws IOException {
		String result = classifier.classify("shit");
		Assert.assertEquals(SentimentType.Negative.toString(), result);
	}

	@Test
	public void doubleNegative() throws IOException {
		String result = classifier.classify("I will not be scared again.");
		Assert.assertEquals(SentimentType.Positive.toString(), result);
	}

	@Test
	public void relativeContext() throws IOException {
		String result = classifier.classify("The book is freaking good. I'd love to read it again.");
		Assert.assertEquals(SentimentType.Positive.toString(), result);
	}
}
